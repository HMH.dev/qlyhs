

package BusinessLogicLayers;

import DataAccessLayers.GiaoVienDAL;
import DataAccessLayers.HocKiDAL;
import DataAccessLayers.LopDAL;
import DataAccessLayers.NamHocDAL;
import DataAccessLayers.PhanCongDAL;
import PresentationLayers.FrmMain;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;


public class PhanCongBLL {
    private FrmMain frmmain;
    private ArrayList<PhanCongDAL> ArrPC=new ArrayList<PhanCongDAL>();
    private JTable table;
    private NavigationBLL nav;
    
    private JLabel reset;
    private JLabel delete;
    private JLabel save;
    private JLabel add;
    private JLabel sumrow;
    private JLabel rowcurrent;
    private JLabel thongbao;
    
    private JComboBox cbbnamhoc;
    private JComboBox cbbhocki;
    private JComboBox cbblop;
    
    private boolean ThemMoi=false;
    private int soluongthem=0;

    public PhanCongBLL(FrmMain frmmain) {
        this.frmmain = frmmain;
        
        getData();
    }
    
    private void getData()
    {
        try {
            ResultSet rss=frmmain.connect.ExecuteSPSelect("SelectPhanCong");
            while(rss.next())
            {
                PhanCongDAL pc=new PhanCongDAL();
                
                pc.setMaNH(frmmain.namhoc.SearchTenNamHoc(rss.getInt(1)));
                pc.setMaHK(frmmain.hocki.SearchTenHK(rss.getInt(2)));
                pc.setMaLop(frmmain.lop.SearchLop(rss.getInt(3)));
                pc.setGiaoVien(frmmain.giaovien.SearchTenGiaoVien(rss.getInt(4)));
                
                ArrPC.add(pc);
            }
            rss.close();
        } catch (Exception e) {
        }
    }
    public void setComBoxBox(JComboBox cbbnamhoc,JComboBox cbbhocki,JComboBox cbblop)
    {
        this.cbbnamhoc=cbbnamhoc;
        this.cbbhocki=cbbhocki;
        this.cbblop=cbblop;
    }
    public void AddComBoBoxNamHoc()
    {
        cbbnamhoc.removeAllItems();
        frmmain.namhoc.AddComboBox(cbbnamhoc);
    }
    public void AddComBoBoxHocKi()
    {
        cbbhocki.removeAllItems();
        frmmain.hocki.AddComboBox(cbbhocki);
    }
    public void AddComBoBoxLop()
    {
        cbblop.removeAllItems();
        frmmain.lop.AddComboBoxLop(cbblop, ((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa());
    }
    public void setTable(JTable table,JPanel pnl)
    {
        nav=new NavigationBLL(table, pnl, reset, reset, reset, reset, rowcurrent, sumrow);
        this.table=table;
    }
    public void InsertTable()
    {
        InsertDataTable();
        
        EditColumnTable();
    }
    private void InsertDataTable()
    {
        table.setModel(new DefaultTableModel(getDataTable(), getColumnTable()));
        nav.selectrowtable(0);
        nav.getSumRow();
    }
    private void EditColumnTable()
    {
        JComboBox cbb=new JComboBox();
        frmmain.giaovien.AddComboBox(cbb);
        TableColumn Column = table.getColumnModel().getColumn(1);
        Column.setCellEditor(new DefaultCellEditor(cbb));
    }
    private Vector getColumnTable()
    {
        Vector column=new Vector();
        
        column.add("STT");
        column.add("Tên Giáo Viên");
        column.add("Dạy Môn");
        
        return column;
    }
    private Vector getDataTable()
    {
        Vector row=new Vector();
        Vector dt;
        
        int MaLop=((LopDAL)(cbblop.getSelectedItem())).getMaLop();
        int MaHK=((HocKiDAL)(cbbhocki.getSelectedItem())).getMa();
        int count=1;
        for(PhanCongDAL pc :ArrPC)
        {
            if(pc.getMaLop().getMaLop()==MaLop && pc.getMaHK().getMa()==MaHK)  
            {
                dt=new Vector();

                dt.add(count++);
                dt.add(pc.getGiaoVien());
                dt.add(pc.getGiaoVien().getMaMonHoc().getTen());

                row.add(dt);
            }
        }
        return row;
    }
    public void setNav(JLabel thongbao)
    {
        this.thongbao=thongbao;
    }
    public void thanhnav()
    {
        reset=nav.create(228, 3,20,22, "Làm Mới Danh Sách");
        delete=nav.create(250, 3,20,22, "Xóa");
        save=nav.create(272, 3,21,22, "Lưu");
        add=nav.create(206, 5,17,19, "Thêm Mới Học Sinh");
        eventlabel();
    }
    private void InsertPhanCong()
    {
        int row=table.getRowCount();
        for(int i=row-soluongthem;i<row;i++)
        {
             try {
                 if(KiemTra(i))
                 {
                    frmmain.connect.ExecuteSPAction("InsertPhanCong", getValue(i));
                    soluongthem--;
                    thongbao.setText("Thêm Thành Công");
                 }
                 else
                     break;
            } catch (Exception e) {
                thongbao.setText("Có Lỗi "+e.getMessage());
                break;
            }
        }
        if(soluongthem==0)
            ThemMoi=false;
    }
    private void EditPhanCong()
    {
        for(int i=0;i<table.getRowCount();i++)
        {
            try {
                if(KiemTra(i)) 
                {
                    frmmain.connect.ExecuteSPAction("EditPhanCong", getValue(i));
                    thongbao.setText("Sửa Thành Công");
                }
            } catch (Exception e) {
                thongbao.setText("Có Lỗi "+e.getMessage());
                break;
            }
        }
    }
    private boolean KiemTra(int row)
    {
        int MaGV=((GiaoVienDAL)(table.getValueAt(row, 1))).getMaGV();
        int MaNH=((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa();
        int MaHocKi=((HocKiDAL)(cbbhocki.getSelectedItem())).getMa();
        int MaLop =((LopDAL)(cbblop.getSelectedItem())).getMaLop();
        for(PhanCongDAL pc : ArrPC)
        {
            if(pc.getGiaoVien().getMaGV()==MaGV && pc.getMaLop().getMaLop()==MaLop && pc.getMaNH().getMa()==MaNH && pc.getMaHK().getMa()==MaHocKi)
            {
                JOptionPane.showMessageDialog(null, "Giáo Viên "+pc.getGiaoVien().getHoTen()+
                        " Đã Có Trong Lich Phân Công Của Lớp "+cbblop.getSelectedItem().toString()+".");
                return false;
            }
        }
        return true;
    }
    private String[] getValue(int row)
    {
        String[] value=new String[4];
        
        value[0]=String.valueOf(((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa());
        value[1]=String.valueOf(((HocKiDAL)(cbbhocki.getSelectedItem())).getMa());
        value[2]=String.valueOf(((LopDAL)(cbblop.getSelectedItem())).getMaLop());
        value[3]=String.valueOf(((GiaoVienDAL)(table.getValueAt(row, 1))).getMaGV());
        
        return value;
    }
    private void DeleteRowTable()
    {
        int rowselect=table.getSelectedRow();
            int result = JOptionPane.showConfirmDialog(null, "Bạn Có Chắc?", "Thông Báo", JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION)
            {
                    try
                    {
                        if(soluongthem>0 && rowselect>=((table.getRowCount()+1)-soluongthem))
                        {
                            soluongthem--;
                            if(soluongthem==0)
                                ThemMoi=false;
                        }
                        else
                        {
                            String[] value={String.valueOf(((LopDAL)(cbblop.getSelectedItem())).getMaLop()),String.valueOf(((GiaoVienDAL)(table.getValueAt(rowselect, 1))).getMaGV())};
                            frmmain.connect.ExecuteSPAction("DeletePhanCong", value);
 
                        }
                        ArrPC.clear();
                        getData();
                        InsertTable();
                    }
                    catch(Exception ex)
                    {
                    }
           }
    }
    private void eventlabel()
    {
        reset.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ArrPC.clear();
                    getData();
                    InsertTable();
                }
                catch(Exception ex)
                {
                    
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                reset.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                reset.setBorder(null);
            }
        });
        delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    DeleteRowTable();
                }
                catch(Exception ex)
                {
                    
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                delete.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                delete.setBorder(null);
            }
        });
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if(ThemMoi) 
                    InsertPhanCong();
                else
                    EditPhanCong();
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                save.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                save.setBorder(null);
            }
        });
        add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ThemMoi=true;
                    soluongthem++;
                    AddNewRowTable();
                }
                catch(Exception ex)
                {
                    //JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                add.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                add.setBorder(null);
            }
        });
    }
    private void AddNewRowTable()
    {
        DefaultTableModel Model = (DefaultTableModel)table.getModel(); 
        
        int row=table.getRowCount()-1;
        int MaMH=0;
        try
        {
            MaMH=Integer.parseInt(table.getValueAt(row, 0).toString());
        }
        catch(Exception ex)
        {
            
        }
        Model.addRow(new Object[]{MaMH+1,"","Tự Động"});
        nav.selectrowtable(table.getRowCount()-1);
        sumrow.setText("Của "+String.valueOf(table.getRowCount()));
    }
    
    public boolean KiemTraXoaGV(int MaGV){
        for(PhanCongDAL pc : ArrPC){
            if(pc.getGiaoVien().getMaGV()==MaGV){
                return false;
            } 
        }
        return true;
    }
}
