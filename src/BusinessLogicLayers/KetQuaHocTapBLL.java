

package BusinessLogicLayers;

import DataAccessLayers.DiemDAL;
import DataAccessLayers.HocKiDAL;
import DataAccessLayers.HocSinh_LopDAL;
import DataAccessLayers.KetQuaHocTapDAL;
import DataAccessLayers.LopDAL;
import DataAccessLayers.MonHocDAL;
import DataAccessLayers.NamHocDAL;
import PresentationLayers.FrmMain;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;


public class KetQuaHocTapBLL {
    
    private FrmMain frmmain;
    private JComboBox cbbnamhoc;
    private JComboBox cbbhocki;
    private JComboBox cbblop;
    private JTable tablekq;
    private NavigationBLL nav;
    
    private JLabel reset;
    private JLabel save;
    private JLabel thongbao;
    
    private ArrayList<KetQuaHocTapDAL> ArrKQHT=new ArrayList<KetQuaHocTapDAL>();
    
    public KetQuaHocTapBLL(FrmMain frmmain)
    {
        this.frmmain=frmmain;
        
        getData();
    }
    private void getData()
    {
        try {
            ResultSet rss=frmmain.connect.ExecuteSPSelect("SelectKQHT");
            while(rss.next())
            {
                KetQuaHocTapDAL kqht=new KetQuaHocTapDAL();
                
                kqht.setHocSinh(frmmain.hocsinh.SearchHocSinh(rss.getString(1)));
                kqht.setHocKi(frmmain.hocki.SearchTenHK(rss.getInt(2)));
                kqht.setNamHoc(frmmain.namhoc.SearchTenNamHoc(rss.getInt(3)));
                kqht.setHocLuc(rss.getString(4));
                kqht.setHanhKiem(rss.getString(5));
                
                ArrKQHT.add(kqht);
            }
            rss.close();
        } catch (Exception e) {
        }
    }
    public void setComBoBox(JComboBox cbbnamhoc,JComboBox cbbhocki,JComboBox cbblop)
    {
        this.cbbnamhoc=cbbnamhoc;
        this.cbbhocki=cbbhocki;
        this.cbblop=cbblop;
    }
    public void AddComBoBoxNamHoc()
    {
        cbbnamhoc.removeAllItems();
        frmmain.namhoc.AddComboBox(cbbnamhoc);
    }
    public void AddComBoBoxHocKi()
    {
        cbbhocki.removeAllItems();
        frmmain.hocki.AddCombobox(cbbhocki);
    }
    public void AddComBoBoxLop()
    {
        cbblop.removeAllItems();
        frmmain.lop.AddComboBoxLop(cbblop, ((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa());
    }
    public void setTable(JTable tablekq,NavigationBLL nav)
    {
        this.tablekq=tablekq;
        this.nav=nav;
    }
    public void insertTable()
    {
        DefaultTableModel tableModel = new DefaultTableModel(getDataTable(), getColumnTable()) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return column !=0;
            }
        };
        tablekq.setModel(tableModel);
        nav.selectrowtable(0);
        nav.getSumRow();
        EditTable();
    }
    private void EditTable()
    {
        String[] str = {"", "Giỏi","Khá","Trung Bình","Yếu","Kém"};
        JComboBox cbb = new JComboBox(str);
        TableColumn Column = tablekq.getColumnModel().getColumn(3);
        Column.setCellEditor(new DefaultCellEditor(cbb));
        
        String[] str2 = {"", "Tốt","Khá","Trung Bình","Yếu"};
        cbb = new JComboBox(str2);
        Column = tablekq.getColumnModel().getColumn(4);
        Column.setCellEditor(new DefaultCellEditor(cbb));
    }
    private Vector getColumnTable()
    {
        Vector column=new Vector();
        column.add("Mã Học Sinh");
        column.add("Tên Học Sinh");
        column.add("Trung Bình");
        column.add("Học Lực");
        column.add("Hạnh Kiểm");
        return column;
    }
    private Vector getDataTable()
    {
        int MaHK=((HocKiDAL)(cbbhocki.getSelectedItem())).getMa();
        int MaLop=((LopDAL)(cbblop.getSelectedItem())).getMaLop();
        int MaNH=((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa();
        double DiemTB;
        Vector row=new Vector();
        Vector dt;
        DecimalFormat df=new DecimalFormat("#.#");
        
        for(HocSinh_LopDAL hsl : frmmain.hocsinhtronglop.getArrHSL(MaLop))
        {
            dt=new Vector();
            
            dt.add(hsl.getHocSinh().getMaHS());
            dt.add(hsl.getHocSinh().getHoTen());
            DiemTB=getDiemTB(hsl.getHocSinh().getMaHS(), MaHK, MaNH);
            dt.add(df.format(DiemTB));
            dt.add(getHocLuc(DiemTB));
            for(KetQuaHocTapDAL kq : ArrKQHT)
            {
                 if(kq.getHocSinh().getMaHS().equals(hsl.getHocSinh().getMaHS()) && 
                         kq.getHocKi().getMa()==MaHK && kq.getNamHoc().getMa()==MaNH)
                 {
                    
                    dt.add(kq.getHanhKiem());
                 }
            }
            
            row.add(dt);
        }
        return row;
    }
    private double getDiemTB(String MaHS,int MaHK,int MaNH)
    {
        double diem=0.0;
        double tong=0.0;
        for(MonHocDAL mh : frmmain.monhoc.getArrayMH())
        {
             diem+=(getDiemTB(mh.getMa(), MaHS, MaHK, MaNH)*mh.getHeSo());
             tong+=mh.getHeSo();
        }
        return diem/tong;
    }
    private double getDiemTB(int MaMH,String MaHS,int MaHK,int MaNH)
    {
        String phut15="";
        String phut45="";
        String Thi="";
        String Mieng="";
        for(DiemDAL d : frmmain.diem.getArrayDiem())
        {
            if(d.getHocSinh().getHocSinh().getMaHS().equals(MaHS)
                        && d.getMonHoc().getMa()==MaMH && d.getHocKi().getMa()==MaHK && d.getNamHoc().getMa()==MaNH) 
                {
                    switch(d.getLoaiDiem().getMaLoai())
                    {
                        case 1:
                            Mieng=d.getDiem();
                            break;
                        case 2:
                            phut15=d.getDiem();
                            break;
                        case 3:
                            phut45=d.getDiem();
                            break;
                        case 4:
                            Thi=d.getDiem();
                            break;
                    }
                }
        }
        return DiemTB(Mieng, phut15, phut45, Thi);
    }
    private double DiemTB(String Mieng,String phut15,String phut45,String Thi)
    {
        if(Mieng!="" && phut15!="" && phut45!="" && Thi!="")
        {
            return (TachDiem(Mieng)+TachDiem(phut15)+(TachDiem(phut45)*2)+(TachDiem(Thi)*3))/7;
        }
        else
            return 0.0;
    }
    private double TachDiem(String diem){
        String[] str=diem.split(";");
        if(str.length==1)
            return Double.parseDouble(diem);
        else
        {
            float tong=0;
            for(int i=0;i<str.length;i++){
                tong+=Double.parseDouble(str[i].trim());
            }
            return tong/str.length;
        }
    }
    private String getHocLuc(double diemtb)
    {
        if(diemtb>=8)
            return "Giỏi";
        if(diemtb>=6.5)
            return "Khá";
        if(diemtb>=5)
            return "Trung Bình";
        if(diemtb>=3.5)
            return "Yếu";
        return "Kém";
    }
    public void thanhnav(JLabel thongbao)
    {
        this.thongbao=thongbao;
        this.reset=nav.create(206, 2,20,22, "Làm Mới Danh Sách");
        this.save=nav.create(228, 2,20,22, "Lưu");
        eventlabel();
    }
    private void eventlabel()
    {
        reset.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ArrKQHT.clear();
                    getData();
                    insertTable();
                }
                catch(Exception ex)
                {
                    
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                reset.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                reset.setBorder(null);
            }
        });
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    LuuLai();
                }
                catch(Exception ex)
                {
                    
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                save.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                save.setBorder(null);
            }
        });
    }
    private void LuuLai()
    {
        for(int i=0;i<tablekq.getRowCount();i++)
        {
            if(KTKQ(i, 3) || KTKQ(i, 4))
                frmmain.connect.ExecuteSPAction("InsertKQHT", getValue(i));
        }
        thongbao.setText("Cập Nhật Thành Công");
    }
    private boolean KTKQ(int row,int column)
    {
        if(tablekq.getValueAt(row, column).toString().trim()!="")
            return true;
        return false;
    }
    private String[] getValue(int row)
    {
        String[] value=new String[5];
        value[0]=tablekq.getValueAt(row, 0).toString().trim();
        value[1]=String.valueOf(((HocKiDAL)(cbbhocki.getSelectedItem())).getMa());
        value[2]=String.valueOf(((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa());
        value[3]=tablekq.getValueAt(row, 3).toString();
        value[4]=tablekq.getValueAt(row, 4).toString();
        return value;
    }
    public String getHanhKiem(String MaHS)
    {
        for(KetQuaHocTapDAL kqht : ArrKQHT)
        {
            if(kqht.getHocSinh().getMaHS().equals(MaHS))
                return kqht.getHanhKiem();
        }
        return "";
    }
}
