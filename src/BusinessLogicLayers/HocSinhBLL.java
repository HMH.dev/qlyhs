

package BusinessLogicLayers;
import DataAccessLayers.DanTocDAL;
import DataAccessLayers.HocSinhDAL;
import DataAccessLayers.TonGiaoDAL;
import PresentationLayers.FrmCalendar;
import PresentationLayers.FrmChiTietHocSinh;
import PresentationLayers.FrmDanToc;
import PresentationLayers.FrmMain;
import PresentationLayers.FrmTonGiao;
import java.awt.event.MouseEvent;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;


public class HocSinhBLL {
    
    //icon reset của thanh navigation
    private JLabel reset;
    //icon delete của thanh navigation
    private JLabel delete;
    //icon save của thanh navigation
    private JLabel save;
    //icon add của thanh navigation
    private JLabel add;
    //icon sumrow của thanh navigation
    private JLabel sumrow;
    //icon rowcurrent của thanh navigation
    private JLabel rowcurrent;
    //icon thongbao của thanh navigation
    private JLabel thongbao;
    
    //dùng để liên kết với  các đối tượng khác
    private final FrmMain main;
    
    //hiển thị danh sách học sinh cho người dùng xem
    private JTable tablehocsinh;
    
    //thanh navigation dùng để thêm sửa xóa,.. dữ liệu trên table
    private NavigationBLL nav;
    
    /*  dùng để lưu dữ danh sách học sinh, để có thể sử dụng khi cần.
        dữ liệu được lấy trong sql server.
        dùng ArrayList để giúp cho chương trình không phải liên tục lấy dữ liệu trong csdl.
        mà chỉ cần lấy 1 lần có thể sử dụng nhiều lần*/
    private ArrayList<HocSinhDAL> ArrHS;
    
    
    //hiện thị danh sách dân tộc
    private JComboBox cbbDanToc;
    //hiện thị danh sách tôn giáo
    private JComboBox cbbTonGiao;
    
    //các control của khung trái
    private JTextField MaHs;
    private JTextField TenHs;
    private JTextField NgaySinh;
    private JTextField NoiSinh;
    private JTextField HoTenCha;
    private JTextField HoTenMe;
    private JComboBox GioiTinh;
    private JComboBox dt;
    private JComboBox tg;
    private JTextField textngaysinh;
    
    //để kiểm tra xem người dùng muốn thêm mới học sinh hay là sửa học sinh
    //nếu ThemMoi=false thì tức là sửa, còn ngược lại là Thêm mới
    private boolean ThemMoi=false;
    //Số lượng học sinh muốn thêm
    private int soluongthem=0;
    
    public HocSinhBLL(FrmMain main)
    {
        //Khởi tạo list học sinh
        ArrHS=new ArrayList<HocSinhDAL>();

        this.main=main;
        
        /*  lấy dữ liệu trong csdl
            mục đích của việc đặt hàm getData() trong hàm khởi tạo là để
            khi chương trình vừa chạy sẽ lấy toàn bộ dữ liệu trong csdl lên.
            để có thể sử dụng khi cần.
            - Lợi ích của cách này là giúp chương trình không phải liên tục lấy
            dữ liệu từ csdl mà chỉ cần lấy 1 lần sử dụng nhiều lần, như vậy sẽ giúp 
            chương trình chạy nhanh hơn trong 'khi sử dụng'.
            - Bất lợi: chương trình sẽ 'khởi động' chậm hơn.
        */
        getData();
    }
    //Lấy dữ liệu từ csdl sql server sau đó add vào ArrayList Để sử dụng khi cần
    private void getData()
    {
        try {
            //rss dùng để lấy dữ liệu trả về khi thực thi thành công câu lệnh truy vần sql
            //SelectAllHocSinh là Store procedure trong csdl sql server
             ResultSet rss = main.connect.ExecuteSPSelect("SelectAllHocSinh");
             //nếu lấy dữ liệu không bị lỗi   
             if (rss != null) {
                 //lấy toàn bộ dữ liệu trong csdl rồi add vào Danh sách
                    while (rss.next()) {
                        //tạo 1 học sinh mới
                        HocSinhDAL hs=new HocSinhDAL();
                        
                        hs.setMaHS(rss.getString(1));
                        hs.setHoTen(rss.getString(2).trim());
                        hs.setGioiTinh(rss.getBoolean(3));
                        hs.setNgaySinh(rss.getString(4));
                        hs.setNoiSinh(rss.getString(5));
                        hs.setDanToc(main.DanToc.SearchDanToc(rss.getInt(6)));
                        hs.setTonGiao(main.TonGiao.SearchTonGiao(rss.getInt(7)));
                        hs.setHoTenCha(rss.getString(8));
                        hs.setHoTenMe(rss.getString(9));
                        
                        //add thông tin học sinh vừa tạo vào danh sách
                        ArrHS.add(hs);
                    }
                    //giải phóng tài nguyên
                rss.close();
            }
        } catch (Exception ex) {
            //JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }
   
    //thêm học sinh mới vào csdl
    private void InsertStudent()
    {
            //duyệt toàn bộ table để lấy dữ liệu trên table insert vào csdl
          int row=tablehocsinh.getRowCount();
          for(int i=row-soluongthem;i<row;i++)
           {
                //Insert Vào CSDL
                try {
                    String[] value=getValue(i);
                    main.connect.ExecuteSPAction("InsertHocSinh", value);
                    soluongthem--;
                    thongbao.setText("Thêm Thành Công");
                    
                    //add học sinh mới thêm vào danh sách
                    HocSinhDAL hs=new HocSinhDAL();
                    hs.setMaHS(value[0]);
                    hs.setHoTen(value[1]);
                    hs.setGioiTinh((value[2].equals("true"))?true:false);
                    hs.setNgaySinh(value[4]);
                    hs.setNoiSinh(value[5]);
                    hs.setDanToc(((DanTocDAL)(tablehocsinh.getValueAt(i, 5))));
                    hs.setTonGiao(((TonGiaoDAL)(tablehocsinh.getValueAt(i, 6))));
                    hs.setHoTenCha(value[7]);
                    hs.setHoTenMe(value[8]);
                    ArrHS.add(hs);
                    
                } catch (Exception e) {
                    thongbao.setText("Có lỗi "+e.getMessage());
                    nav.selectrowtable(i);
                    break;
                }
            }
          //nếu tất cả học sinh đã thêm vào thành công thì cho themmoi=false;
        if(soluongthem==0) 
            ThemMoi=false;
    }
    //sửa học sinh
    private void EditAllStudent()
    {
        //duyệt toàn bộ table để lấy danh sách học sinh
        for(int i=0;i<tablehocsinh.getRowCount();i++)
        {
            try
            {
                main.connect.ExecuteSPAction("EditHocSinh", getValue(i));
                thongbao.setText("Sửa Thành Công "+(i+1)+" Học Sinh");
            }
            catch(Exception ex)
            {
                thongbao.setText("Có Lỗi Ở HS "+tablehocsinh.getValueAt(i, 1).toString());
                nav.selectrowtable(i);
                break;
            }
        }
    }
    
    //set control của table
    public void setTable(JTable tablehocsinh,JPanel pnl)
    {
        nav=new NavigationBLL(tablehocsinh, pnl, reset, reset, reset, reset, rowcurrent, sumrow,0);
        this.tablehocsinh=tablehocsinh;
        this.sumrow=nav.getJSumRow();
        this.rowcurrent=nav.getCurrentRow();
        this.nav.set(this);
    }
    
    //đổ danh sách học sinh lên table
    public void insertTable()
    {
        InsertDataTable();
        
        EditColumnTable();
    }
    private void InsertDataTable()
    {
        //Đổ dữ liệu lên jtable
        tablehocsinh.setModel(new DefaultTableModel(getDatatable(),getColumnTable()));
        //Select vào row 0
        nav.selectrowtable(0);
        //lấy số lượng row
        nav.getSumRow();
    }
    private void EditColumnTable()
    {
        //Edit column giới tính thành combobox
        String[] gioitinh = {"Nam", "Nữ"};
        JComboBox gioitinhCombo = new JComboBox(gioitinh);
        TableColumn Column = tablehocsinh.getColumnModel().getColumn(2);
        Column.setCellEditor(new DefaultCellEditor(gioitinhCombo));
        
        //Edit column dân tộc thành combobox
        gioitinhCombo = new JComboBox();
        main.DanToc.AddComboBox(gioitinhCombo);
        Column = tablehocsinh.getColumnModel().getColumn(5);
        Column.setCellEditor(new DefaultCellEditor(gioitinhCombo));
        
        
        //Edit column Tôn giáo thành combobox
        gioitinhCombo = new JComboBox();
        main.TonGiao.AddComboBox(gioitinhCombo);
        Column = tablehocsinh.getColumnModel().getColumn(6);
        Column.setCellEditor(new DefaultCellEditor(gioitinhCombo));
        
        //Edit comlumn mã học sinh thành textfield sau đó set Enabled bằng false
        //để không cho thay đổi mã hs
        JTextField txt=new JTextField();
        txt.setEnabled(false);
        Column = tablehocsinh.getColumnModel().getColumn(0);
        Column.setCellEditor(new DefaultCellEditor(txt));
        
        textngaysinh=new JTextField();
        textngaysinh.setEnabled(false);
        textngaysinh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                showCalendar(textngaysinh,150);
            }
        });
        Column = tablehocsinh.getColumnModel().getColumn(3);
        Column.setCellEditor(new DefaultCellEditor(textngaysinh));
    }
    //tìm kiếm học sinh theo mã học sinh
    public HocSinhDAL SearchHocSinh(String MaHS)
    {
        //duyệt toàn bộ danh sách để tìm học sinh. nếu tìm được thì trả về hs tìm được
        //ko tìm được thì trả về null
        for(HocSinhDAL hs : ArrHS)
        {
            if(hs.getMaHS().equals(MaHS))
                return hs;
        }
        return null;
    }
    //set control của khung bên trái
    public void setKhungTrai(JTextField MaHs,JTextField TenHs,JTextField NgaySinh,JTextField NoiSinh,
            JComboBox GioiTinh,JComboBox dt,JComboBox tg,JTextField HoTenCha,JTextField HoTenMe)
    {
        this.MaHs=MaHs;
        this.TenHs=TenHs;
        this.NgaySinh=NgaySinh;
        this.NoiSinh=NoiSinh;
        this.GioiTinh=GioiTinh;
        this.dt=dt;
        this.tg=tg;
        this.HoTenCha=HoTenCha;
        this.HoTenMe=HoTenMe;
    }
    /*
        - Hàm Đổ Dữ Liệu Học Sinh Lên khung bên trái form.
    */
    public void HienThiChiTiet()
    {
        try
        {
            int row=tablehocsinh.getSelectedRow();
            MaHs.setText(tablehocsinh.getValueAt(row, 0).toString());
            TenHs.setText(tablehocsinh.getValueAt(row, 1).toString());
            NgaySinh.setText(tablehocsinh.getValueAt(row, 3).toString());
            NoiSinh.setText(tablehocsinh.getValueAt(row, 4).toString());
            GioiTinh.setSelectedItem(tablehocsinh.getValueAt(row, 2).toString());
            dt.setSelectedItem((DanTocDAL)tablehocsinh.getValueAt(row, 5));
            tg.setSelectedItem((TonGiaoDAL)tablehocsinh.getValueAt(row, 6));
            HoTenCha.setText(tablehocsinh.getValueAt(row, 7).toString());
            HoTenMe.setText(tablehocsinh.getValueAt(row, 8).toString());
        }
        catch(Exception ex)
        {
            
        }
    }
    /*
        - Hàm set Control Navigation.
        - Gồm các control reset dữ liệu, lưu dữ liệu, thêm dữ liệu, thông báo khi thực hiện
    */
    public void setNav(JLabel thongbao)
    {
        this.thongbao=thongbao;
        thanhnav();
    }
    /*
        - Hàm tạo thanh navigation.
    */
    private void thanhnav()
    {
        reset=nav.create(228, 3,20,22, "Làm Mới Danh Sách");
        delete=nav.create(250, 3,20,22, "Xóa");
        save=nav.create(272, 3,21,22, "Lưu");
        add=nav.create(206, 5,17,19, "Thêm Mới Học Sinh");
        eventlabel();
    }
    
    /*
        - Hàm xóa 1 hàng trên table
        - Dùng để xóa học sinh
    */
    private void DeleteRowTable()
    {
        //lấy row cần xóa
        int rowselect=tablehocsinh.getSelectedRow();
        /*kiểm tra có thể xóa được học sinh này không
            học sinh chỉ xóa được khi học sinh đó chưa có lớp và chưa có điểm
        */
        if(KiemTraXoaHocSinh(rowselect))
        {
            //hiện thị hộp thoại thông báo, chống click nhầm
            int result = JOptionPane.showConfirmDialog(null, "Bạn Có Chắc?", "Thông Báo", JOptionPane.YES_NO_OPTION);
            //nếu thật sự muốn xóa thì xóa học sinh đó ra khỏi danh sách
            if (result == JOptionPane.YES_OPTION)
            {
                    try
                    {
                        if(soluongthem>0 && rowselect>=((tablehocsinh.getRowCount()+1)-soluongthem))
                        {
                            soluongthem--;
                            if(soluongthem==0)
                                ThemMoi=false;
                        }
                        else
                        {
                            String[] value={tablehocsinh.getValueAt(rowselect, 0).toString().trim()};
                            main.connect.ExecuteSPAction("DeleteHocSinh", value);
                            RemoveHS(value[0]);
                            main.phanlop.UpdateData();
                        }
                        ((DefaultTableModel)tablehocsinh.getModel()).removeRow(rowselect);
                        nav.selectrowtable((rowselect==tablehocsinh.getRowCount())?rowselect-1:rowselect);
                        sumrow.setText("Của "+String.valueOf(tablehocsinh.getRowCount()));
                    }
                    catch(Exception ex)
                    {
                        
                    }
           }
        }
    }
    //kiểm tra có học sinh đang muốn xóa có thể xóa được không
    private boolean KiemTraXoaHocSinh(int row)
    {
        String MaHS=tablehocsinh.getValueAt(row, 0).toString().trim();
        if(main.hocsinhtronglop.SearchHocSinh(MaHS)!=null)
        {
            JOptionPane.showMessageDialog(main, "Học Sinh "+tablehocsinh.getValueAt(row, 1).toString()+
                    "\nĐã Có Trong Danh Sách Lớp Học.\nKhông Thể Xóa");
            return false;
        }
        return true;
    }
    //xóa học sinh ra khỏi danh sach
    private void RemoveHS(String MaHS)
    {
        for(HocSinhDAL hs : ArrHS)
        {
            if(hs.getMaHS().equals(MaHS))
            {
                ArrHS.remove(hs);
                break;
            }
        }
    }
    //reset lại table
    private void ResetTable()
    {
        ArrHS.clear();
        getData();
        InsertDataTable();
        setCurrentRow();
        HienThiChiTiet();
    }
    
    //Hiển Thị Row Hiện Tại Lên Nav
    public void setCurrentRow()
    {
        try
        {
            //lấy row đang select
            rowcurrent.setText(String.valueOf(tablehocsinh.getSelectedRow()+1));
        }
        catch(Exception ex)
        {
        }
    }
    //Đổ Dữ Liệu Vào ComboBox
    public void AddCombobox(JComboBox cbbDanToc,JComboBox cbbTonGiao)
    {
        main.DanToc.AddComboBox(cbbDanToc);
        main.TonGiao.AddComboBox(cbbTonGiao);
        this.cbbDanToc=cbbDanToc;
        this.cbbTonGiao=cbbTonGiao;
    }
    //đổ dữ liệu vào combobox dân tộc
    public void addComboBoxDT()
    {
        //xóa toàn bộ dan tộc trước đó.
        cbbDanToc.removeAllItems();
        main.DanToc.AddComboBox(cbbDanToc);
        EditColumnTable();
    }
    //đổ dữ liệu vào combobox tôn giáo
    public void addComboBoxTG()
    {
        cbbTonGiao.removeAllItems();
        main.TonGiao.AddComboBox(cbbTonGiao);
        EditColumnTable();
    }
    //lưu dữ liệu vào danh sách trên khung bên trái
    public void LuuVaoDanhSach()
    {
        String[] value=new String[9];
        value[0]=MaHs.getText();
        value[1]=TenHs.getText();
        value[2]=(GioiTinh.getSelectedItem().toString().equals("Nam"))?"true":"false";
        value[3]=NgaySinh.getText().trim();
        value[4]=NoiSinh.getText().trim();
        value[5]=String.valueOf(((DanTocDAL)(cbbDanToc.getSelectedItem())).getMa());
        value[6]=String.valueOf(((TonGiaoDAL)(cbbTonGiao.getSelectedItem())).getMa());
        value[7]=HoTenCha.getText().trim();
        value[8]=HoTenMe.getText().trim();
        try
            {
                if(SearchHocSinh(value[0])!=null) 
                {
                    main.connect.ExecuteSPAction("EditHocSinh", value);
                    thongbao.setText("Sửa Thành Công");
                }
                else
                {
                    main.connect.ExecuteSPAction("InsertHocSinh", value);
                    thongbao.setText("Thêm Thành Công");
                    HocSinhDAL hs=new HocSinhDAL();
                    hs.setMaHS(value[0]);
                    hs.setNgaySinh(value[1]);
                    hs.setNoiSinh(value[2]);
                    hs.setDanToc((DanTocDAL)cbbDanToc.getSelectedItem());
                    hs.setTonGiao((TonGiaoDAL)cbbTonGiao.getSelectedItem());
                    hs.setHoTenCha(value[7]);
                    hs.setHoTenMe(value[8]);
                    ArrHS.add(hs);
                    soluongthem--;
                    main.phanlop.UpdateData(value[0], value[1]);
                }
                CapNhatLenTable(value);
            }
            catch(Exception ex)
            {
                thongbao.setText("Có Lỗi");
            }
    }
    private void CapNhatLenTable(String[] value)
    {
        int row=tablehocsinh.getSelectedRow();
        tablehocsinh.setValueAt(value[1], row, 1);
        tablehocsinh.setValueAt((value[2].equals("true"))?"Nam":"Nữ", row, 2);
        tablehocsinh.setValueAt(value[3], row, 3);
        tablehocsinh.setValueAt(value[4], row, 4);
        tablehocsinh.setValueAt(cbbDanToc.getSelectedItem().toString(), row, 5);
        tablehocsinh.setValueAt(cbbTonGiao.getSelectedItem().toString(), row, 6);
    }
    
    //Tạo Comlumn table
    private Vector getColumnTable()
    {
        Vector Comlumn=new Vector();
        Comlumn.add("Mã Học Sinh");
        Comlumn.add("Tên Học Sinh");
        Comlumn.add("Giới Tính");
        Comlumn.add("Ngày Sinh");
        Comlumn.add("Nơi Sinh");
        Comlumn.add("Dân Tộc");
        Comlumn.add("Tôn Giáo");
        Comlumn.add("Họ Tên Cha");
        Comlumn.add("Họ Tên Mẹ");
        return Comlumn;
    }
    //Lấy data của table
    private Vector getDatatable()
    {
        Vector row=new Vector();
        Vector dr;
        
        
        for(HocSinhDAL hs : ArrHS)
        {
            dr=new Vector();
            dr.add(hs.getMaHS());
            dr.add(hs.getHoTen());
            dr.add((hs.isGioiTinh())?"Nam":"Nữ");
            dr.add(hs.getNgaySinh());
            dr.add(hs.getNoiSinh());
            dr.add(hs.getDanToc());
            dr.add(hs.getTonGiao());
            dr.add(hs.getHoTenCha());
            dr.add(hs.getHoTenMe());
            row.add(dr);
        }
        
        return row;
    }
    //Lấy data của table Search
    private Vector getDatatable(String MaHS,String TenHS)
    {
        Vector row=new Vector();
        Vector dr;
        
        
        for(HocSinhDAL hs : ArrHS)
        {
            if((MaHS!="" && hs.getMaHS().indexOf(MaHS.toUpperCase())!=-1) || (TenHS!="" && hs.getHoTen().toLowerCase().indexOf(TenHS)!=-1))
            {
                dr=new Vector();
                dr.add(hs.getMaHS());
                dr.add(hs.getHoTen());
                dr.add((hs.isGioiTinh())?"Nam":"Nữ");
                dr.add(hs.getNgaySinh());
                dr.add(hs.getNoiSinh());
                dr.add(hs.getDanToc());
                dr.add(hs.getTonGiao());
                dr.add(hs.getHoTenCha());
                dr.add(hs.getHoTenMe());
                row.add(dr);
            }
        }
        
        return row;
    }
    
    //tạo sự kiện cho control navigation
    private void eventlabel()
    {
        reset.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ResetTable();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                reset.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                reset.setBorder(null);
            }
        });
        delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    DeleteRowTable();
                }
                catch(Exception ex)
                {
                    
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                delete.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                delete.setBorder(null);
            }
        });
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if(ThemMoi)
                    InsertStudent();
                else
                    EditAllStudent();
                try {
                    main.phanlop.UpdateData();
                } catch (Exception e) {
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                save.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                save.setBorder(null);
            }
        });
        add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ThemMoi=true;
                    soluongthem++;
                    AddNewRowTable();
                    HienThiChiTiet();
                }
                catch(Exception ex)
                {
                    //JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                add.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                add.setBorder(null);
            }
        });
    }
    //Thếm 1 Row Mới Vào Table
    private void AddNewRowTable()
    {
        DefaultTableModel Model = (DefaultTableModel)tablehocsinh.getModel(); 
        
        int size=ArrHS.size();
        String MaHS="HS0001";
        if(size>0)
        {
            if(!taochua) 
                MaHS=ArrHS.get(size-1).getMaHS();
            else
                MaHS=tablehocsinh.getValueAt(tablehocsinh.getRowCount()-1, 0).toString();
            MaHS=MaHS.substring(2);
            int m=Integer.parseInt(MaHS.trim());
            m++;
            if(m<10)
                MaHS="HS000"+m;
            else
                if(m<100)
                    MaHS="HS00"+m;
            else
                    if(m<1000)
                        MaHS="HS0"+m;
            else
                        MaHS="HS"+m;
        }
        Model.addRow(new Object[]{MaHS,"","Nam","","","","","",""});
        nav.selectrowtable(tablehocsinh.getRowCount()-1);
        sumrow.setText("Của "+String.valueOf(tablehocsinh.getRowCount()));
        
    }
    //lấy giá trị trên table xuống để thêm sửa.
    private String[] getValue(int row)
    {
        String[] value=new String[9];
        value[0]=tablehocsinh.getValueAt(row, 0).toString().trim();
        value[1]=FormatName(tablehocsinh.getValueAt(row, 1).toString());
        value[2]=(tablehocsinh.getValueAt(row, 2).toString().equals("Nam"))?"true":"false";
        value[3]=tablehocsinh.getValueAt(row, 3).toString();
        value[4]=FormatName(tablehocsinh.getValueAt(row, 4).toString());
        value[5]=String.valueOf(((DanTocDAL)(tablehocsinh.getValueAt(row, 5))).getMa());
        value[6]=String.valueOf(((TonGiaoDAL)(tablehocsinh.getValueAt(row, 6))).getMa());
        value[7]=tablehocsinh.getValueAt(row, 7).toString();
        value[8]=tablehocsinh.getValueAt(row, 8).toString();
        return value;
    }
    //Hàm Định Dạng Lại Tên
    //VD:
    //Input:  nguyen van a
    //Output: Nguyen Van A
    private String FormatName(String Name)
    {
        //Chuyển Đổi Về Chữ Thường
        Name=Name.trim();
        String str="";
        char c=Name.charAt(0);
        if(c>='a' && c<='z') 
            c-=32;
        str+=c;
        for(int i=1;i<Name.length();i++)
        {
            //Nếu Kí Tự Là Khoảng Trắng Thị Kí Tự Tiếp Theo Sẽ Được Chuyển Về Hoa
            if(Name.charAt(i)==' ')
            {
                //Lấy Kí Tự Tiếp Theo
                c=Name.charAt(i+1);
                //Kiểm Tra Xem Kí Tự Đó Có Phải là Khoảng Trắng.
                //Nếu Không Phải Là Khoảng trắng thì chuyển đổi về hoa con không thì thôi.
                if(c!=' ')
                {
                    if(c>='a' && c<='z') 
                        c-=32;
                    str+=" "+c;
                    i++;
                }
            }
            else
                str+=Name.charAt(i);
        }
        return str;
    }
    //form search học sinh
    private void SearchHocSinh()
    {
        int loaisearch=cbbnoisearch.getSelectedIndex();
        tablehocsinh.setModel(new DefaultTableModel(getDatatable((loaisearch==0)?MaHs.getText():"",(loaisearch==0)?"":MaHs.getText().toLowerCase()),getColumnTable()));
        EditColumnTable();
        nav.selectrowtable(0);
    }
    private boolean taochua=true;
    private JComboBox cbbnoisearch=new JComboBox();
    private JLabel lbsearchh=new JLabel(new ImageIcon(getClass().getResource("/PresentationLayers/Images/buttonsearch.png")));
    public void frmsearch(JPanel pnlthongtinhocsinnh,JTextField txttenhs,JLabel jLabel5)
    {
        if(taochua)
        {
            cbbnoisearch.setSize(169,25);
            cbbnoisearch.setLocation(txttenhs.getX(),txttenhs.getY());
            cbbnoisearch.addItem("Mã Học Sinh");
            cbbnoisearch.addItem("Tên Học Sinh");
            pnlthongtinhocsinnh.add(cbbnoisearch);
            lbsearchh.setSize(172,25);
            lbsearchh.setLocation(jLabel5.getX(),jLabel5.getY());
            pnlthongtinhocsinnh.add(lbsearchh);
            lbsearchh.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
            taochua=false;
            lbsearchh.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    SearchHocSinh();
                }
            });
        }
        else
        {
            cbbnoisearch.setVisible(true);
            lbsearchh.setVisible(true);
        }
               
    }
    public void frmchitiet(JTextField txtmahs)
    {
        cbbnoisearch.setVisible(false);
         lbsearchh.setVisible(false);
         txtmahs.setText(tablehocsinh.getValueAt(tablehocsinh.getSelectedRow(), 0).toString());
    }
    public boolean getTaoChua()
    {
        return this.taochua;
    }
    public void showCalendar(JTextField txtngays)
    {
        JDialog dialog=new JDialog(main,"Calendar",true);
        dialog.setSize(158, 170);
        dialog.setLocation(txtngays.getX(), txtngays.getY()+dialog.getHeight()+45);
        dialog.add(new FrmCalendar(txtngays,dialog));
        dialog.setVisible(true);
    }   
    public void showCalendar(JTextField txtngays,int x)
    {
        JDialog dialog=new JDialog(main,"Calendar",true);
        dialog.setSize(158, 170);
        dialog.setLocation(txtngays.getX()+x, txtngays.getY()+dialog.getHeight()+45);
        dialog.add(new FrmCalendar(txtngays,dialog));
        dialog.setVisible(true);
    }   
    public void showformdantoc()
    {
        main.createTab("Dân Tộc", new FrmDanToc(main.DanToc), 11);
    }
    public void showformtongiao()
    {
        main.createTab("Tôn Giáo", new FrmTonGiao(main.TonGiao), 12);
    }
    public boolean SearchDanToc(int MaDT)
    {
        for(HocSinhDAL hs : ArrHS)
        {
            if(hs.getDanToc().getMa()==MaDT)
            {
                return true; 
            }
        }
        return false;
    }
    public boolean SearchTonGiao(int MaTG)
    {
        for(HocSinhDAL hs : ArrHS)
        {
            if(hs.getTonGiao().getMa()==MaTG)
            {
                return true; 
            }
        }
        return false;
    }
    public ArrayList<HocSinhDAL> getArrayHS()
    {
        return this.ArrHS;
    }
}
