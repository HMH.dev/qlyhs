
package BusinessLogicLayers;

import DataAccessLayers.HocKiDAL;
import DataAccessLayers.HocSinh_LopDAL;
import DataAccessLayers.LopDAL;
import DataAccessLayers.NamHocDAL;
import PresentationLayers.FrmMain;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class HocSinh_LopBLL {
    
    //icon reset của thanh navigation
    private JLabel reset;
    //icon sumrow của thanh navigation
    private JLabel sumrow;
    //icon rowcurrent của thanh navigation
    private JLabel rowcurrent;
    
     //dùng để liên kết với  các đối tượng khác
    private final FrmMain frmmain;
    
    //hiện thị danh sách năm học
    private JComboBox cbbnamhoc;
    //hiện thị danh sách lớp
    private JComboBox cbblop;
    //hiện thị danh sách học kì
    private JComboBox cbbhocki;
    
    /*  dùng để lưu dữ danh sách học sinh trong lớp, để có thể sử dụng khi cần.
        dữ liệu được lấy trong sql server.
        dùng ArrayList để giúp cho chương trình không phải liên tục lấy dữ liệu trong csdl.
        mà chỉ cần lấy 1 lần có thể sử dụng nhiều lần*/
    private ArrayList<HocSinh_LopDAL> ArrHSLop;
    
    //hiện thi danh sách lớp nên cho người dùng xem
    private JTable tablehocsinhlop;
    
    //thanh navigation dùng để thêm sửa xóa,.. dữ liệu trên table
    private NavigationBLL nav;
    
    public HocSinh_LopBLL(FrmMain frmmain)
    {
        //gán frmmain của class này với frmmain truyền vào từ FrmMain
        this.frmmain=frmmain;
        //tạo mới danh sách lớp
        ArrHSLop=new ArrayList();
        /*  lấy dữ liệu trong csdl
            mục đích của việc đặt hàm getData() trong hàm khởi tạo là để
            khi chương trình vừa chạy sẽ lấy toàn bộ dữ liệu trong csdl lên.
            để có thể sử dụng khi cần.
            - Lợi ích của cách này là giúp chương trình không phải liên tục lấy
            dữ liệu từ csdl mà chỉ cần lấy 1 lần sử dụng nhiều lần, như vậy sẽ giúp 
            chương trình chạy nhanh hơn trong 'khi sử dụng'.
            - Bất lợi: chương trình sẽ 'khởi động' chậm hơn.
        */
        getData();
    }
    //Lấy dữ liệu từ csdl sql server sau đó add vào ArrayList Để sử dụng khi cần
    private void getData()
    {
        try {
             //rss dùng để lấy dữ liệu trả về khi thực thi thành công câu lệnh truy vần sql
            //SelectAllHocSinhLop là Store procedure trong csdl sql server
            ResultSet rss=frmmain.connect.ExecuteSPSelect("SelectAllHocSinhLop");
            //duyệt hết dữ liệu trong csdl
            while(rss.next())
            {
                //tạo mới 1 học sinh trong lớp
                HocSinh_LopDAL hsl=new HocSinh_LopDAL();
                
                //set thông tin cho học sinh vừa tạo ở trên
                hsl.setHocSinh(frmmain.hocsinh.SearchHocSinh(rss.getString(1)));
                hsl.setLop(frmmain.lop.SearchLop(rss.getInt(2)));
                hsl.setHocKi(frmmain.hocki.SearchTenHK(rss.getInt(3)));
                hsl.setNamHoc(frmmain.namhoc.SearchTenNamHoc(rss.getInt(4)));
                
                //thêm học sinh đó vào array lớp
                ArrHSLop.add(hsl);
            }
            //giải phóng tài nguyên
            rss.close();
            
        } catch (Exception e) {
        }
    }
    //tìm kiếm học sinh trong lớp
    public HocSinh_LopDAL SearchHocSinh(String MaHS)
    {
        //duyệt hết danh sách nếu tìm được học sinh cần tìm thì trả về học sinh vừa tìm thấy.
        //Nếu không tìm thấy thì trả về null
        for(HocSinh_LopDAL hsl : ArrHSLop)
        {
            //equals trả về true khi chuỗi A bằng chuỗi B
            if(hsl.getHocSinh().getMaHS().equals(MaHS))
                return hsl;
        }
        return null;
    }
    //lấy danh sách học sinh trong 1 lớp
    public ArrayList<HocSinh_LopDAL> getArrHSL(int MaLop)
    {
        ArrayList<HocSinh_LopDAL> ahsl=new ArrayList<HocSinh_LopDAL>();
        //duyệt toàn bộ danh sách để lọc ra tất cả học sinh trong lớp cần lấy
        for(HocSinh_LopDAL hsl : ArrHSLop)
        {
            if(hsl.getLop().getMaLop()==MaLop)
               ahsl.add(hsl);
        }
        //trả về danh sách vừa lấy được
        return ahsl;
    }
    //set control của table
    public void setTable(JTable tablehocsinh,JPanel pnl)
    {
        nav=new NavigationBLL(tablehocsinh, pnl, reset, reset, reset, reset, rowcurrent, sumrow);
        this.tablehocsinhlop=tablehocsinh;
        this.sumrow=nav.getJSumRow();
        this.rowcurrent=nav.getCurrentRow();
    }
    //đổ dữ liệu lên table
    public void insertTable()
    {
        InsertDataTable();
    }
    private void InsertDataTable()
    {
        //Đổ dữ liệu lên jtable
        tablehocsinhlop.setModel(new DefaultTableModel(getDatatable(),getColumnTable()));
        //Select vào row 0
        nav.selectrowtable(0);
        nav.getSumRow();
    }
    //update lại dữ liệu khi thêm 1 học sinh mới
    public void UpdateData(HocSinh_LopDAL hsl)
    {
        ArrHSLop.add(hsl);
        try {
             if(cbblop.getSelectedItem().toString().equals(hsl.getLop().getTenLop().trim()))
            {
                DefaultTableModel Model = (DefaultTableModel)tablehocsinhlop.getModel();
                Model.addRow(new Object[]{hsl.getHocSinh().getMaHS(),hsl.getHocSinh().getHoTen(),
                hsl.getLop().getTenLop(),hsl.getHocKi().getTen(),hsl.getNamHoc().getTen()});
            }
            nav.getSumRow();
        } catch (Exception e) {
        }
       
    }
    //xóa học sinh khỏi lớp
    public void UpdateData(String MaHS)
    {
        for(HocSinh_LopDAL hsl : ArrHSLop)
        {
            if(hsl.getHocSinh().getMaHS().equals(MaHS))
            {
                ArrHSLop.remove(hsl);
                break;
            }
        }
        InsertDataTable();
        nav.getSumRow();
    }
    //Tạo Comlumn table
    private Vector getColumnTable()
    {
        Vector Comlumn=new Vector();
        Comlumn.add("Mã Học Sinh");
        Comlumn.add("Tên Học Sinh");
        Comlumn.add("Lớp");
        Comlumn.add("Năm Học");
        Comlumn.add("Học Kì");
        return Comlumn;
    }
    //Lấy data của table
    private Vector getDatatable()
    {
        Vector row=new Vector();
        Vector dr;
        
        //lấy thông tìm người dùng cần xem
        int MaNH=((NamHocDAL)cbbnamhoc.getSelectedItem()).getMa();
        int MaLop=((LopDAL)cbblop.getSelectedItem()).getMaLop();
        int MaHK=0;
        if(cbbhocki.getSelectedIndex()!=0) 
            MaHK=((HocKiDAL)cbbhocki.getSelectedItem()).getMa();
        
        /*
            ---------------
           | dr | dr | dr | row 1
            --------------
           | dr | dr | dr | row 2
            --------------
        */
        for(HocSinh_LopDAL hsl : ArrHSLop)
        {
            if(hsl.getLop().getMaLop()==MaLop && hsl.getNamHoc().getMa()==MaNH && (hsl.getHocKi().getMa()==MaHK || MaHK==0))
            {
                dr=new Vector();
                dr.add(hsl.getHocSinh().getMaHS());
                dr.add(hsl.getHocSinh().getHoTen());
                dr.add(hsl.getLop().getTenLop());
                dr.add(hsl.getNamHoc().getTen());
                dr.add(hsl.getHocKi().getTen());
                row.add(dr);
            }
        }
        
        return row;
    }
    //Lấy data của table thêo lớp
    public Vector getDatatable(int MaLop)
    {
        Vector row=new Vector();
        Vector dr;
        
        for(HocSinh_LopDAL hsl : ArrHSLop)
        {
            if(hsl.getLop().getMaLop()==MaLop)
            {
                dr=new Vector();
                dr.add(hsl.getHocSinh().getMaHS());
                dr.add(hsl.getHocSinh().getHoTen());
                row.add(dr);
            }
        }
        
        return row;
    }
    //đổ dữ liệu vào combobox trên khung bên trái
    public void AddComboBox(JComboBox cbbnamhoc,JComboBox cbblop,JComboBox cbbhocki)
    {
        this.cbbnamhoc=cbbnamhoc;
        this.cbblop=cbblop;
        this.cbbhocki=cbbhocki;
        AddComBoxBoxNamHoc();
        AddComBoBoxHocKi();
    }
    //đổ dữ liệu vào combobox học kì
    public void AddComBoBoxHocKi()
    {
        //xóa toàn bộ học kì cũ
        cbbhocki.removeAllItems();
        cbbhocki.addItem("Tất Cả");
        frmmain.hocki.AddComboBox(cbbhocki);
    }
    //đổ dữ liệu vào combobox năm học
    public void AddComBoxBoxNamHoc()
    {
        //xóa toàn bộ năm học cũ
        cbbnamhoc.removeAllItems();
        frmmain.namhoc.AddComboBox(cbbnamhoc);
    }
    //đổ dữ liệu vào combobox lớp
    public void AddComboBoxLop()
    {
        //lấy mã năm học cần xem
        int manh=((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa();
        frmmain.lop.AddComboBoxLop(cbblop,manh);
    }
    //
    public void HienThiDanhSach()
    {
        InsertDataTable();
    }
    //set control của thanh navigation
    public void setNav()
    {
        thanhnav();
    }
    private void thanhnav()
    {
        reset=nav.create(205, 3,20,22, "Làm Mới Danh Sách");
        eventlabel();
    }
    private void eventlabel()
    {
        reset.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ArrHSLop.clear();
                    getData();
                    InsertDataTable();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                reset.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                reset.setBorder(null);
            }
        });
        /*int result = JOptionPane.showConfirmDialog(null, "Bạn Có Chắc?", "Thông Báo", JOptionPane.YES_NO_OPTION);
                    if (result == JOptionPane.YES_OPTION)*/
    }
    //thực hiện khi có thêm mới 1 lớp
    public void updatecbblop()
    {
        cbblop.removeAllItems();
        AddComboBoxLop();
    }
    //kiểm tra xem có thể xóa được lớp không
    //lớp chỉ xóa được khi ko có học sinh trong lớp đó
    public boolean KiemTraXoaLop(int malop)
    {
        for(HocSinh_LopDAL hsl : ArrHSLop)
        {
            if(hsl.getLop().getMaLop()==malop)
                return true;
        }
        return false;
    }
    //kiểm tra xóa năm học
    public boolean KiemTraXoaNamHoc(int manh)
    {
        for(HocSinh_LopDAL hsl : ArrHSLop)
        {
            if(hsl.getNamHoc().getMa()==manh)
                return true;
        }
        return false;
    }
    //kiểm tra xóa học kì
    public boolean KiemTraXoaHocKi(int mahk)
    {
        for(HocSinh_LopDAL hsl : ArrHSLop)
        {
            if(hsl.getHocKi().getMa()==mahk)
                return true;
        }
        return false;
    }
    //kiểm tra xóa học sinh
    public int KiemTraXoaHS(String MaHS,int MaNH)
    {
        int count=0;
        for(HocSinh_LopDAL hsl : ArrHSLop)
        {
            if(hsl.getHocSinh().getMaHS().equals(MaHS) && hsl.getNamHoc().getMa()!=MaNH)
                count++;
        }
        return count;
    }
    //lấy toàn bộ danh sách học sinh trong lớp
    public ArrayList<HocSinh_LopDAL> getArrayHSL()
    {
        return this.ArrHSLop;
    }
}
