

package BusinessLogicLayers;

import DataAccessLayers.NamHocDAL;
import PresentationLayers.FrmMain;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class NamHocBLL {
    
    private FrmMain frmmain;
    
    private ArrayList<NamHocDAL> ArrNamHoc;
    
    private JTable tablenamhoc;
    
    private NavigationBLL nav;
    
    private JLabel reset;
    private JLabel delete;
    private JLabel save;
    private JLabel add;
    private JLabel sumrow;
    private JLabel rowcurrent;
    private JLabel thongbao;
    
    private boolean ThemMoi=false;
    private int soluongthem=0;
    private JTextField txtmanh;
    private JTextField txttennh;
    
    public NamHocBLL(FrmMain frmmain) {
        this.frmmain=frmmain;
        ArrNamHoc=new ArrayList<NamHocDAL>();
        
        getData();
    }
    
    private void getData()
    {
        try {
             ResultSet rss = frmmain.connect.ExecuteSPSelect("SelectAllNamHoc");
                if (rss != null) {
                    while (rss.next()) {
                        NamHocDAL nh=new NamHocDAL();
                        
                        nh.setMa(rss.getInt(1));
                        nh.setTen(rss.getString(2));
                        
                        ArrNamHoc.add(nh);
                    }
                rss.close();
            }
        } catch (Exception ex) {
            
        }
    }
    public NamHocDAL SearchTenNamHoc(int manh)
    {
        for(NamHocDAL nh : ArrNamHoc)
        {
            if(nh.getMa()==manh)
                return nh;
        }
        return null;
    }
    public void AddComboBox(JComboBox cbb)
    {
        for(NamHocDAL nh : ArrNamHoc)
        {
            cbb.addItem(nh);
        }
    }
    public void insertTable()
    {
        DefaultTableModel tableModel = new DefaultTableModel(getDatatable(),getColumnTable()) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return column == 1;
            }
        };
        tablenamhoc.setModel(tableModel);
        //Select vào row 0
        nav.selectrowtable(0);
        nav.getSumRow();
    }
    //Tạo Comlumn table
    private Vector getColumnTable()
    {
        Vector Comlumn=new Vector();
        Comlumn.add("Mã Năm Học");
        Comlumn.add("Tên Năm Học");
        return Comlumn;
    }
    //Lấy data của table
    private Vector getDatatable()
    {
        Vector row=new Vector();
        Vector dr;
        
        
        for(NamHocDAL nh : ArrNamHoc)
        {
            dr=new Vector();
            dr.add(nh.getMa());
            dr.add(nh.getTen());
            row.add(dr);
        }
        
        return row;
    }
    public void setTable(JTable tablenamhoc,JPanel pnl)
    {
        nav=new NavigationBLL(tablenamhoc, pnl, reset, reset, reset, reset, rowcurrent, sumrow,5);
        this.tablenamhoc=tablenamhoc;
        this.sumrow=nav.getJSumRow();
        this.rowcurrent=nav.getCurrentRow();
        this.nav.set(this);
    }
    public void setKhungTrai(JTextField txtmanh,JTextField txttennh)
    {
        this.txtmanh=txtmanh;
        this.txttennh=txttennh;
    }
    //Hiển Thị Row Hiện Tại Lên Nav
    public void setCurrentRow()
    {
        try
        {
            rowcurrent.setText(String.valueOf(tablenamhoc.getSelectedRow()+1));
        }
        catch(Exception ex)
        {
        }
    }
    public void HienThiChiTiet()
    {
        try
        {
            int row=tablenamhoc.getSelectedRow();
            txtmanh.setText(tablenamhoc.getValueAt(row, 0).toString());
            txttennh.setText(tablenamhoc.getValueAt(row, 1).toString());
        }catch(Exception ex)
        {
            
        }
    }
    public void setNav(JLabel thongbao)
    {
        this.thongbao=thongbao;
        thanhnav();
    }
    private void AddNewRowTable()
    {
        DefaultTableModel Model = (DefaultTableModel)tablenamhoc.getModel(); 
        
        int row=tablenamhoc.getRowCount()-1;
        int MaMH=0;
        MaMH=Integer.parseInt(tablenamhoc.getValueAt(row, 0).toString())+1;
        Model.addRow(new Object[]{MaMH,""});
        nav.selectrowtable(tablenamhoc.getRowCount()-1);
        sumrow.setText("Của "+String.valueOf(tablenamhoc.getRowCount()));
    }
    private void InsertNamHoc()
    {
        int row=tablenamhoc.getRowCount();
        for(int i=row-soluongthem;i<row;i++)
        {
            try
            {
                frmmain.connect.ExecuteSPAction("InsertNamHoc", getValue(i));
                thongbao.setText("Thêm Thành Công");
                soluongthem--;
            }
            catch(Exception ex)
            {
                thongbao.setText("Có Lỗi");
                nav.selectrowtable(i);
                break;
            }
        }
        if(soluongthem==0)
            ThemMoi=false;
    }
    private void EditNamHoc()
    {
        for(int i=0;i<tablenamhoc.getRowCount();i++)
        {
            try {
                frmmain.connect.ExecuteSPAction("EditNamHoc", getValue2(i));
                thongbao.setText("Sửa Thành Công "+i+" Năm Học");
            } catch (Exception e) {
                thongbao.setText("Có Lỗi");
                nav.selectrowtable(i);
                break;
            }
        }
    }
    private String[] getValue(int row)
    {
        String[] value=new String[1];
        value[0]=tablenamhoc.getValueAt(row, 1).toString();
        return value;
    }
    private String[] getValue2(int row)
    {
        String[] value=new String[2];
        value[0]=tablenamhoc.getValueAt(row, 0).toString();
        value[1]=tablenamhoc.getValueAt(row, 1).toString();
        return value;
    }
    private void DeleteRowTable()
    {
        int rowselect=tablenamhoc.getSelectedRow();
        if(KiemTraXoaNamHoc(rowselect))
        {
            int result = JOptionPane.showConfirmDialog(null, "Bạn Có Chắc?", "Thông Báo", JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION)
            {
                    try
                    {
                        if(soluongthem>0 && rowselect>=((tablenamhoc.getRowCount()+1)-soluongthem))
                        {
                            soluongthem--;
                            if(soluongthem==0)
                                ThemMoi=false;
                        }
                        else
                        {
                            String[] value={tablenamhoc.getValueAt(rowselect, 0).toString().trim()};
                            frmmain.connect.ExecuteSPAction("DeleteNamHoc", value);
                            RemoveNH(Integer.parseInt(value[0]));
                            CapNhat();
                        }
                        ((DefaultTableModel)tablenamhoc.getModel()).removeRow(rowselect);
                        nav.selectrowtable((rowselect==tablenamhoc.getRowCount())?rowselect-1:rowselect);
                        sumrow.setText("Của "+String.valueOf(tablenamhoc.getRowCount()));
                    }
                    catch(Exception ex)
                    {
                        
                    }
           }
        }
    }
    private boolean KiemTraXoaNamHoc(int row)
    {
        int MaNH=Integer.parseInt(tablenamhoc.getValueAt(row, 0).toString().trim());
        if(frmmain.hocsinhtronglop.KiemTraXoaNamHoc(MaNH) || frmmain.lop.KiemTraXoaNamHoc(MaNH))
        {
            JOptionPane.showMessageDialog(null, "Đã Có Đối Tượng Sử Dụng Năm Học "+tablenamhoc.getValueAt(row, 1).toString()+
                    "\nBạn Không Thê Xóa");
            return false;
        }
        return true;
    }
    private void RemoveNH(int MaNH)
    {
        for(NamHocDAL nh : ArrNamHoc)
        {
            if(nh.getMa()==MaNH)
            {
                ArrNamHoc.remove(nh);
                break;
            }
        }
    }
    private void CapNhat()
    {
        try {
            frmmain.hocsinhtronglop.AddComBoxBoxNamHoc();
            frmmain.kqht.AddComBoBoxNamHoc();
            frmmain.diem.AddComBoBoNamHoc();
            frmmain.lop.AdddComBoxBoxNamHoc();
            frmmain.phanlop.AddComBoxBoxNamHoc();
            frmmain.chuyenlop.AddComBoxBoxNamHoc();
            frmmain.phancong.AddComBoBoxNamHoc();
        } catch (Exception e) {
        }
    }
    //tạo sự kiện cho control navigation
    private void eventlabel()
    {
        reset.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ArrNamHoc.clear();
                    getData();
                    insertTable();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                reset.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                reset.setBorder(null);
            }
        });
        delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    DeleteRowTable();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                delete.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                delete.setBorder(null);
            }
        });
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if(ThemMoi) 
                    InsertNamHoc();
                else
                    EditNamHoc();
                ArrNamHoc.clear();
                getData();
                CapNhat();
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                save.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                save.setBorder(null);
            }
        });
        add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ThemMoi=true;
                    soluongthem++;
                    AddNewRowTable();
                    HienThiChiTiet();
                }
                catch(Exception ex)
                {
                    //JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                add.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                add.setBorder(null);
            }
        });
    }
    /*
        - Hàm tạo thanh navigation.
    */
    private void thanhnav()
    {
        reset=nav.create(228, 3,20,22, "Làm Mới Danh Sách");
        delete=nav.create(250, 3,20,22, "Xóa");
        save=nav.create(272, 3,21,22, "Lưu");
        add=nav.create(206, 5,17,19, "Thêm Mới Năm Học");
        eventlabel();
    }
    public void LuuVaoDanhSach()
    {
        try {
            if(SearchTenNamHoc(Integer.parseInt(txtmanh.getText()))!=null)
            {
                String[] value={txtmanh.getText().trim(),txttennh.getText()};
                frmmain.connect.ExecuteSPAction("EditNamHoc", value);
                thongbao.setText("Sửa Thành Công");
                Sua(Integer.parseInt(value[0]),value[1]);
            }
            else
            {
                String[] value={txttennh.getText()};
                frmmain.connect.ExecuteSPAction("InsertNamHoc", value);
                thongbao.setText("Thêm Thành Công");
                ArrNamHoc.add(new NamHocDAL(Integer.parseInt(txtmanh.getText().trim()), txttennh.getText()));
            }
            CapNhatLenTable(txttennh.getText());
            CapNhat();
        } catch (Exception e) {
        }
    }
    private void Sua(int MaHK,String TenHK)
    {
        for(NamHocDAL nh : ArrNamHoc)
        {
            if(nh.getMa()==MaHK)
            {
                nh.setMa(MaHK);
                nh.setTen(TenHK);
                break;
            }
        }
    }
    private void CapNhatLenTable(String TenHK)
    {
        tablenamhoc.setValueAt(TenHK, tablenamhoc.getSelectedRow(), 1);
    }
}
