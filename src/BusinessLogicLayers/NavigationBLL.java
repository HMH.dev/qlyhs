

package BusinessLogicLayers;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;


public class NavigationBLL {
    private JTable table;
    private JPanel pnlthanhnav;
    private JLabel first;
    private JLabel last;
    private JLabel previous;
    private JLabel next;
    private JLabel RowCurrent;
    private JLabel SumRow;
    private int n;
    
    private HocSinhBLL hocsinh;
    private GiaoVienBLL giaovien;
    private MonHocBLL monhoc;
    private DanTocBLL dantoc;
    private TonGiaoBLL tongiao;
    private NamHocBLL namhoc;
    private HocKiBLL hocki;

    public NavigationBLL(JTable table, JPanel pnlthanhnav, JLabel first, JLabel last, JLabel previous, JLabel next,JLabel RowCurrent,JLabel SumRow) {
        this.table = table;
        this.pnlthanhnav = pnlthanhnav;
        this.first = first;
        this.last = last;
        this.previous = previous;
        this.next = next;
        this.RowCurrent=RowCurrent;
        this.SumRow=SumRow;
        this.n=100;
        AddNavigation();
    }
    public NavigationBLL(JTable table, JPanel pnlthanhnav, JLabel first, JLabel last, JLabel previous, JLabel next,JLabel RowCurrent,JLabel SumRow,int n) {
        this.table = table;
        this.pnlthanhnav = pnlthanhnav;
        this.first = first;
        this.last = last;
        this.previous = previous;
        this.next = next;
        this.RowCurrent=RowCurrent;
        this.SumRow=SumRow;
        this.n=n;
        AddNavigation();
    }
    public void set(HocSinhBLL hocsinh)
    {
        this.hocsinh=hocsinh;
    }
    public void set(GiaoVienBLL giaovien)
    {
        this.giaovien=giaovien;
    }
    public void set(MonHocBLL monhoc)
    {
        this.monhoc=monhoc;
    }
    public void set(DanTocBLL dantoc)
    {
        this.dantoc=dantoc;
    }
    public void set(TonGiaoBLL tongiao)
    {
        this.tongiao=tongiao;
    }
    public void set(NamHocBLL namhoc)
    {
        this.namhoc=namhoc;
    }
    public void set(HocKiBLL hocki)
    {
        this.hocki=hocki;
    }
    public void AddNavigation()
    {
        first=create(3, 5,15,19,"Đến Đầu Danh Sách");
        previous=create(28, 5,15,19, "Trở Lại Dòng Trước");
        next=create(153, 5,15,19, "Tới Dòng Kế Tiếp");
        last=create(178, 5,15,19, "Đến Cuối Danh Sách");
        SumRow=create(100, 5, 70, 17, "Tổng Số");
        RowCurrent=create(60, 5, 20, 17, "");
        RowCurrent.setText("1");
        AddEvent();
    }
    public void getSumRow()
    {
        SumRow.setText("Của "+String.valueOf(table.getRowCount()));
    }
    private void hienthikhungtrai()
    {
        switch(n)
        {
            case 0:
                hocsinh.HienThiChiTiet();
                break;
            case 1:
                giaovien.HienThiChiTiet();
                break;
            case 2:
                monhoc.HienThiChiTiet();
                break;
            case 3:
                dantoc.HienThiChiTiet();
                break;
            case 4:
                tongiao.HienThiChiTiet();
                break;
            case 5:
                namhoc.HienThiChiTiet();
                break;
            case 6:
                hocki.HienThiChiTiet();
                break;
        }
    }
    private void AddEvent()
    {
        first.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    selectrowtable(0);
                    RowCurrent.setText("1");
                    hienthikhungtrai();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                first.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                first.setBorder(null);
            }
        });
        previous.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    selectrowtable(((table.getSelectedRow())==0)?table.getRowCount()-1:table.getSelectedRow()-1);
                    RowCurrent.setText(String.valueOf(table.getSelectedRow()+1));
                    hienthikhungtrai();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                previous.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                previous.setBorder(null);
            }
        });
        next.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    selectrowtable(((table.getSelectedRow())>table.getRowCount()-2)?0:table.getSelectedRow()+1);
                    RowCurrent.setText(String.valueOf(table.getSelectedRow()+1));
                    hienthikhungtrai();
                }
                catch(Exception ex)
                {
                    
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                next.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                next.setBorder(null);
            }
        });
        last.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                selectrowtable(table.getRowCount()-1);
                RowCurrent.setText(String.valueOf(table.getRowCount()));
                hienthikhungtrai();
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                last.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                last.setBorder(null);
            }
        });

    }
    public JLabel create(int x,int y,int w,int h,String tooltip)
    {
        JLabel lb=new JLabel();
        lb.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lb.setSize(w,h);
        lb.setLocation(x, y);
        lb.setToolTipText(tooltip);
        pnlthanhnav.add(lb);
        return lb;
    }
    public void selectrowtable(int row)
    {
        try
        {
            table.changeSelection(table.getSelectedRow(),0,true,false);
            table.changeSelection(row,0,true,false);
        }
        catch(Exception ex)
        {
            
        }
    }
    public JLabel getCurrentRow()
    {
        return this.RowCurrent;
    }
    public JLabel getJSumRow()
    {
        return this.SumRow;
    }
}
