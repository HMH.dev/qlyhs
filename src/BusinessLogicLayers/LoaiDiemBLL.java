
package BusinessLogicLayers;

import DataAccessLayers.LoaiDiemDAL;
import PresentationLayers.FrmMain;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;


public class LoaiDiemBLL {
    
    private FrmMain frmmain;
    
    private ArrayList<LoaiDiemDAL> ArrLD=new ArrayList<LoaiDiemDAL>();
    
    public LoaiDiemBLL(FrmMain frmmain)
    {
        this.frmmain=frmmain;
        
        getData();
    }
    private void getData()
    {
        try {
            ResultSet rss=frmmain.connect.ExecuteSPSelect("SelectLoaiDiem");
            
            while(rss.next())
            {
                LoaiDiemDAL ld=new LoaiDiemDAL(rss.getInt(1),rss.getString(2),rss.getInt(3));
                
                ArrLD.add(ld);
            }
            rss.close();
        } catch (Exception e) {
        }
    }
    public LoaiDiemDAL SearchLoaiDiem(int MaLD)
    {
        for(LoaiDiemDAL ld : ArrLD)
        {
            if(ld.getMaLoai()==MaLD)
                return ld;
        }
        return null;
    }
    public Vector ThemVaoComlumn(boolean b)
    {
        Vector column=new Vector();
        column.add("Mã Học Sinh");
        column.add("Tên Học Sinh");
        if(b){ 
            for(LoaiDiemDAL ld : ArrLD)
            {
                column.add(ld.getTenLoai());
            }
        }
        else
            column.add("Trung Bình Môn");
        return column;
    }
}
