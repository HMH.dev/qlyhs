

package BusinessLogicLayers;
import DataAccessLayers.DiemDAL;
import DataAccessLayers.HocKiDAL;
import DataAccessLayers.HocSinh_LopDAL;
import DataAccessLayers.LopDAL;
import DataAccessLayers.MonHocDAL;
import DataAccessLayers.NamHocDAL;
import PresentationLayers.FrmMain;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class DiemBLL {
    //dùng để liên kết với  các đối tượng khác
    private final FrmMain frmmain;
    //hiển thị danh sách năm học
    private JComboBox cbbnamhoc;
    //hiển thị danh sách học kì
    private JComboBox cbbhocki;
    //hiển thi danh sách lớp
    private JComboBox cbblop;
    //hiển thị danh sách môn học
    private JComboBox cbbmonnhoc;
    
    //hiển thị điểm lên cho người dùng xem
    private JTable tablediem;
    //thanh navigation dùng để thêm sửa xóa,.. dữ liệu trên table
    private NavigationBLL nav;
    
    /*  dùng để lưu dữ điểm, để có thể sử dụng khi cần.
        dữ liệu được lấy trong sql server.
        dùng ArrayList để giúp cho chương trình không phải liên tục lấy dữ liệu trong csdl.
        mà chỉ cần lấy 1 lần có thể sử dụng nhiều lần*/
    private ArrayList<DiemDAL> ArrD=new ArrayList<DiemDAL>();
    //icon reset lại dữ liệu trên thanh navigation
    private JLabel reset;
    //icon save lại dữ liệu trên thanh navigation
    private JLabel save;
    //icon thông báo
    private JLabel thongbao;
    
    //hàm khởi tạo
    public DiemBLL(FrmMain frmmain)
    {
        this.frmmain=frmmain;
        
        /*  lấy dữ liệu trong csdl
            mục đích của việc đặt hàm getData() trong hàm khởi tạo là để
            khi chương trình vừa chạy sẽ lấy toàn bộ dữ liệu trong csdl lên.
            để có thể sử dụng khi cần.
            - Lợi ích của cách này là giúp chương trình không phải liên tục lấy
            dữ liệu từ csdl mà chỉ cần lấy 1 lần sử dụng nhiều lần, như vậy sẽ giúp 
            chương trình chạy nhanh hơn trong 'khi sử dụng'.
            - Bất lợi: chương trình sẽ 'khởi động' chậm hơn.
        */
        getData();
    }
    //Lấy dữ liệu từ csdl sau đó add vào ArrayList để sử dụng khi cần
    private void getData()
    {
        try {
            //rss dùng để lấy dữ liệu trả về khi thực thi thành công câu lệnh truy vần sql
            //SelectDiem là Store procedure trong csdl sql server
            ResultSet rss=frmmain.connect.ExecuteSPSelect("SelectDiem");
            //dùng vòng lập để lấy toàn bộ dữ liêu trong csdl
            while(rss.next())
            {
                DiemDAL d=new DiemDAL();
                
                d.setHocSinh(frmmain.hocsinhtronglop.SearchHocSinh(rss.getString(1).trim()));
                d.setMonHoc(frmmain.monhoc.SearchMonHoc(rss.getInt(2)));
                d.setHocKi(frmmain.hocki.SearchTenHK(rss.getInt(3)));
                d.setNamHoc(frmmain.namhoc.SearchTenNamHoc(rss.getInt(4)));
                d.setLoaiDiem(frmmain.loaidiem.SearchLoaiDiem(rss.getInt(5)));
                d.setDiem(rss.getString(6));
                
                ArrD.add(d);
            }
            //dóng rss để giải phóng bộ nhờ
            rss.close();
        } catch (Exception e) {
        }
                
    }
    //Set control table
    public void setTable(JTable tablediem,JPanel pnl)
    {
        nav=new NavigationBLL(tablediem, pnl, reset, reset, reset, reset, reset, reset);
        this.tablediem=tablediem;
    }
    
    //Thêm Dữ liệu vào table
    public void InsertTable()
    {
        DefaultTableModel tableModel = new DefaultTableModel((cbbhocki.getSelectedIndex()!=2)?getDatatable():
                getDatatableCaNam(),(cbbhocki.getSelectedIndex()!=2)?frmmain.loaidiem.ThemVaoComlumn(true):
                        frmmain.loaidiem.ThemVaoComlumn(false)) {
            @Override
            //không cho edit row table
            public boolean isCellEditable(int row, int column) {
                return column !=0;
            }
        };
        tablediem.setModel(tableModel);
        this.nav.selectrowtable(0);
        nav.getSumRow();
    }
    //Lấy data table
    private Vector getDatatable()
    {
        //lấy thông tin mà người dùng muốn lấy bao gồm: học kì, năm học, lớp, môn hoc
        //để lấy dữ liệu đúng với yêu cầu của người dùng
        int MaHK=((HocKiDAL)(cbbhocki.getSelectedItem())).getMa();
        int MaLop=((LopDAL)(cbblop.getSelectedItem())).getMaLop();
        int MaMH=((MonHocDAL)(cbbmonnhoc.getSelectedItem())).getMa();
        int MaNH=((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa();
        Vector row=new Vector();
        Vector dt;
        
        //điểm 15 phút
        String phut15;
        //điểm 45 phút
        String phut45;
        //điểm thi
        String Thi;
        //điểm miệng
        String Mieng;
        
        //format lại điểm vd 7.666666 sẽ format thành 7.7
        DecimalFormat df=new DecimalFormat("#.#");
        
        //hiển thị học sinh trong lớp mà người dùng muốn xem
        for(HocSinh_LopDAL hsl : frmmain.hocsinhtronglop.getArrHSL(MaLop))
        {
            dt=new Vector();

            dt.add(hsl.getHocSinh().getMaHS());
            dt.add(hsl.getHocSinh().getHoTen());
            Mieng="";
            phut15="";
            phut45="";
            Thi="";
            for(DiemDAL d : ArrD)
            {
                if(d.getHocSinh().getHocSinh().getMaHS().equals(hsl.getHocSinh().getMaHS())
                        && d.getMonHoc().getMa()==MaMH && d.getHocKi().getMa()==MaHK && d.getNamHoc().getMa()==MaNH) 
                {
                    switch(d.getLoaiDiem().getMaLoai())
                    {
                        case 1:
                            Mieng=d.getDiem();
                            break;
                        case 2:
                            phut15=d.getDiem();
                            break;
                        case 3:
                            phut45=d.getDiem();
                            break;
                        case 4:
                            Thi=d.getDiem();
                            break;
                    }
                }
            }
            dt.add(Mieng);
            dt.add(phut15);
            dt.add(phut45);
            dt.add(Thi);
            dt.add(df.format(DiemTB(Mieng, phut15, phut45, Thi)));
            
            row.add(dt);
        }
        
        return row;
    }
    //Lấy data table cả năm
    private Vector getDatatableCaNam()
    {
        //tương tự hàm trên
        int MaLop=((LopDAL)(cbblop.getSelectedItem())).getMaLop();
        int MaMH=((MonHocDAL)(cbbmonnhoc.getSelectedItem())).getMa();
        int MaNH=((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa();
        Vector row=new Vector();
        Vector dt;
        
        String phut15;
        String phut45;
        String Thi;
        String Mieng;
        double d1=0.0;
        double d2=0.0;
        
        DecimalFormat df=new DecimalFormat("#.#");
        
        for(HocSinh_LopDAL hsl : frmmain.hocsinhtronglop.getArrHSL(MaLop))
        {
            dt=new Vector();

            dt.add(hsl.getHocSinh().getMaHS());
            dt.add(hsl.getHocSinh().getHoTen());
           
            d1=0.0;
            d2=0.0;
            for(int i=1;i<=2;i++){
            Mieng="";
            phut15="";
            phut45="";
            Thi="";
            for(DiemDAL d : ArrD)
            {
                if(d.getHocSinh().getHocSinh().getMaHS().equals(hsl.getHocSinh().getMaHS())
                        && d.getMonHoc().getMa()==MaMH && d.getHocKi().getMa()==i && d.getNamHoc().getMa()==MaNH) 
                {
                    switch(d.getLoaiDiem().getMaLoai())
                    {
                        case 1:
                            Mieng=d.getDiem();
                            break;
                        case 2:
                            phut15=d.getDiem();
                            break;
                        case 3:
                            phut45=d.getDiem();
                            break;
                        case 4:
                            Thi=d.getDiem();
                            break;
                    }
                }
            }
            if(i==1)
                d1=DiemTB(Mieng, phut15, phut45, Thi);
            else
                d2=DiemTB(Mieng, phut15, phut45, Thi);
            }
            dt.add(df.format((d2*2+d1)/3));
            row.add(dt);
        }
        
        return row;
    }
    //Tính điểm trung bình
    private double DiemTB(String Mieng,String phut15,String phut45,String Thi)
    {
        //kiểm tra xem học sinh cần tính có đủ 4 điểm chưa. nếu chưa đủ thì không tính được điểm trung bình
        if(Mieng!="" && phut15!="" && phut45!="" && Thi!="")
        {
            //điểm miệng vào 15 phút là hệ số 1, điểm 45 phút hệ số 2, điểm thi hệ số 3
            return (TachDiem(Mieng)+TachDiem(phut15)+(TachDiem(phut45)*2)+(TachDiem(Thi)*3))/7;
        }
        else
            return 0.0;
    }
    //tách điểm ra từ điểm 1
    //VD điểm gốc là 7;9;8
    //sẽ tách ra thành 7 9 8 sau đó cộng 3 số đó lại, chia cho 3 để lấy điểm trung bình 
    private double TachDiem(String diem){
        String[] str=diem.split(";");
        if(str.length==1)
            return Double.parseDouble(diem);
        else
        {
            float tong=0;
            for(int i=0;i<str.length;i++){
                tong+=Double.parseDouble(str[i].trim());
            }
            return tong/str.length;
        }
    }
    public void setComBoBox(JComboBox cbbnamhoc,JComboBox cbbhocki,JComboBox cbblop,JComboBox cbbmonnhoc)
    {
        this.cbbnamhoc=cbbnamhoc;
        this.cbbhocki=cbbhocki;
        this.cbblop=cbblop;
        this.cbbmonnhoc=cbbmonnhoc;
    }
    public void AddComBoBoNamHoc()
    {
        cbbnamhoc.removeAllItems();
        frmmain.namhoc.AddComboBox(cbbnamhoc);
    }
    public void AddComBoBoHocKi()
    {
        cbbhocki.removeAllItems();
        frmmain.hocki.AddCombobox(cbbhocki);
    }
    public void AddComBoBoLop()
    {
        cbblop.removeAllItems();
        frmmain.lop.AddComboBoxLop(cbblop, ((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa());
    }
    public void AddComBoBoxMonHoc()
    {
        cbbmonnhoc.removeAllItems();
        frmmain.monhoc.addComboBox(cbbmonnhoc);
    }
    public void setNav(JLabel thongbao)
    {
        this.thongbao=thongbao;
        this.reset=nav.create(206, 2,20,22, "Làm Mới Danh Sách");
        this.save=nav.create(228, 2,20,22, "Lưu");
        eventlabel();
    }
    //Sự kiện thêm, sửa, xóa, reset
    private void eventlabel()
    {
        reset.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ArrD.clear();
                    getData();
                    InsertTable();
                }
                catch(Exception ex)
                {
                    
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                reset.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                reset.setBorder(null);
            }
        });
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    LuuLai();
                }
                catch(Exception ex)
                {
                    
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                save.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                save.setBorder(null);
            }
        });
        /*int result = JOptionPane.showConfirmDialog(null, "Bạn Có Chắc?", "Thông Báo", JOptionPane.YES_NO_OPTION);
                    if (result == JOptionPane.YES_OPTION)*/
    }
    //Hàm của sự kiện save
    private void LuuLai()
    {
        for(int i=0;i<tablediem.getRowCount();i++)
        {
            for(int j=2;j<=5;j++)
            {
                if(KTDiem(i, j))
                    frmmain.connect.ExecuteSPAction("InsertDiem", getValue(i, j));
            }
        }
        thongbao.setText("Cập Nhật Thành Công");
    }
    //Lấy giá trị của từng dòng trên table
    // Dùng để thêm, sửa trong csdl
    private String[] getValue(int row,int column)
    {
        String[] value=new String[6];
        value[0]=tablediem.getValueAt(row, 0).toString();
        value[1]=String.valueOf(((MonHocDAL)(cbbmonnhoc.getSelectedItem())).getMa());
        value[2]=String.valueOf(((HocKiDAL)(cbbhocki.getSelectedItem())).getMa());
        value[3]=String.valueOf(((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa());
        value[4]=String.valueOf(column-1);
        value[5]=tablediem.getValueAt(row, column).toString();
        return value;
    }
    //Kiểm tra điểm nhập vào có hợp lệ hay không
    //Điểm hợp lệ là điểm trong khoảng từ 0->10
    private boolean KTDiem(int row,int column)
    {
        String Diem=tablediem.getValueAt(row, column).toString().trim();
        if(Diem!="" && KTDiem(Diem))
            return true;
        return false;
    }
    //Tách từng điểm trong 1 chuỗi để kiểm tra
    private boolean KTDiem(String Diem)
    {
        try
        {
            String[] str=Diem.split(";");
            for(int i=0;i<str.length;i++){
                if(!KTDiem(Float.parseFloat(str[i])))
                    return false;
            }
            return true;
        }
        catch(Exception ex)
        {
            return false;
        }
    }
    //Kiểm tra hợp lệ của điểm
    private boolean KTDiem(float diem){
        if(diem<0 || diem>10)
        {
             JOptionPane.showMessageDialog(null, "Điểm Từ 0 Đến 10");
              return false;
        }
        return true;
    }
    //Kiểm tra xem môn học đó đã có điểm chưa, nếu có rồi thi không cho xoa1
    public boolean KiemTraXoaMonHoc(int MaMH)
    {
        for(DiemDAL d : ArrD)
        {
            if(d.getMonHoc().getMa()==MaMH)
                return true;
        }
        return false;
    }
    //Kiểm tra học sinh đã có điểm chưa, nếu đã có thì không cho xóa
    public boolean KiemTraXoaHS(String MaHS,int MaHK,int MaNH)
    {
        for(DiemDAL d : ArrD)
        {
            if(d.getHocSinh().getHocSinh().getMaHS().equals(MaHS) && d.getHocKi().getMa()==MaHK && d.getNamHoc().getMa()==MaNH)
                return true;
        }
        return false;
    }
    //Lấy danh sách điểm
    public ArrayList<DiemDAL> getArrayDiem()
    {
        return this.ArrD;
    }
}
