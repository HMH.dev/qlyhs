
package BusinessLogicLayers;

import DataAccessLayers.HocKiDAL;
import PresentationLayers.FrmMain;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class HocKiBLL {
    
    //Tất cả các biến dưới đã được giái thích bên class DanTocBLL
    private FrmMain frmmain;
    
    private ArrayList<HocKiDAL> ArrHK;
    private JTable tablehocki;
    private NavigationBLL nav;
    private JLabel sumrow;
    private JLabel rowcurrent;
    private JLabel reset;
    private JLabel delete;
    private JLabel save;
    private JLabel add;
    private JLabel thongbao;
    private boolean ThemMoi=false;
    private int SoLuongThem=0;
    private JTextField txtmahk;
    private JTextField txttenhk;
    
    public HocKiBLL(FrmMain frmmain)
    {
        this.frmmain=frmmain;
        //tạo mới 1 danh sách liên kết
        ArrHK=new ArrayList();
        
        /*  lấy dữ liệu trong csdl
            mục đích của việc đặt hàm getData() trong hàm khởi tạo là để
            khi chương trình vừa chạy sẽ lấy toàn bộ dữ liệu trong csdl lên.
            để có thể sử dụng khi cần.
            - Lợi ích của cách này là giúp chương trình không phải liên tục lấy
            dữ liệu từ csdl mà chỉ cần lấy 1 lần sử dụng nhiều lần, như vậy sẽ giúp 
            chương trình chạy nhanh hơn trong 'khi sử dụng'.
            - Bất lợi: chương trình sẽ 'khởi động' chậm hơn.
        */
        getData();
    }
    //lấy dữ liệu học ki trong csdl sau đó add vào ArrayList để sử dụng khi cần
    //Đã được giải thích bên class DanTocBLL
    private void getData()
    {
        try {
            //rss dùng để lấy dữ liệu trả về khi thực thi thành công câu lệnh truy vần sql
            //SelectAllDanToc là Store procedure trong csdl sql server
            ResultSet rss=frmmain.connect.ExecuteSPSelect("SelectAllHocKi");
            //lấy tất cả dữ liệu có trong rss ra
            while(rss.next())
            {
                HocKiDAL hk=new HocKiDAL();
                
                hk.setMa(rss.getInt(1));
                hk.setTen(rss.getString(2));
                
                ArrHK.add(hk);
            }
            //giải phóng bộ nhớ
            rss.close();
        } catch (Exception e) {
        }
    }
    //Tìm kiếm học kì theo mã học kì
    public HocKiDAL SearchTenHK(int mahk)
    {
        //duyện toàn bộ danh sách để tìm. nếu tìm thấy thì trả về học kì tìm thấy
        // còn không tìm thấy thì trả về null
        for(HocKiDAL hk : ArrHK)
        {
            if(hk.getMa()==mahk)
                return hk;
        }
        return null;
    }
    //đổ dữ liệu vào combobox học kì không có học kì cả năm
    public void AddComboBox(JComboBox cbb)
    {
        for(HocKiDAL hk : ArrHK)
        {
            if(hk.getMa()!=3) 
                cbb.addItem(hk);
        }
    }
     //đổ dữ liệu vào combobox học kì
    public void AddCombobox(JComboBox cbb)
    {
        for(HocKiDAL hk : ArrHK)
        {
                cbb.addItem(hk);
        }
    }
    //set control của table. Được truyên từ FrmHocKi
    public void setTable(JTable tablehocki,NavigationBLL nav)
    {
        this.tablehocki=tablehocki;
        this.sumrow=nav.getJSumRow();
        this.rowcurrent=nav.getCurrentRow();
        this.nav=nav;
        this.nav.set(this);
    }
    //đổ dữ liệu vào table để cho người dùng xem
    public void insertTable()
    {
        DefaultTableModel tableModel = new DefaultTableModel(getDatatable(),getColumnTable()) {
            @Override
            //không cho edit row table
            public boolean isCellEditable(int row, int column) {
                return column == 1;
            }
        };
        tablehocki.setModel(tableModel);
        
        //select vào row 0
        nav.selectrowtable(0);
    }
    //Tạo Comlumn table
    private Vector getColumnTable()
    {
        Vector Comlumn=new Vector();
        Comlumn.add("Mã Học Kì");
        Comlumn.add("Tên Học Kì");
        return Comlumn;
    }
    //Lấy data của table
    private Vector getDatatable()
    {
        Vector row=new Vector();
        Vector dr;
        
        /*
            ---------------
           | dr | dr | dr | row 1
            --------------
           | dr | dr | dr | row 2
            --------------
        */
        for(HocKiDAL hk : ArrHK)
        {
            dr=new Vector();
            dr.add(hk.getMa());
            dr.add(hk.getTen());
            row.add(dr);
        }
        
        return row;
    }
    //khởi tạo control của thanh navigation
    public void thanhnav(JLabel thongbao)
    {
        this.thongbao=thongbao;
        reset=nav.create(228, 3,20,22, "Làm Mới Danh Sách");
        delete=nav.create(250, 3,20,22, "Xóa");
        save=nav.create(272, 3,21,22, "Lưu");
        add=nav.create(206, 5,17,19, "Thêm Mới Học Kì");
        eventlabel();
    }
    //Thêm 1 row mới vào table,dùng để thêm mới
    private void AddNewRowTable()
    {
        DefaultTableModel Model = (DefaultTableModel)tablehocki.getModel(); 
        
        int row=tablehocki.getRowCount()-1;
        int MaMH=0;
        MaMH=Integer.parseInt(tablehocki.getValueAt(row, 0).toString())+1;
        Model.addRow(new Object[]{MaMH,""});
        nav.selectrowtable(tablehocki.getRowCount()-1);
        sumrow.setText("Của "+String.valueOf(tablehocki.getRowCount()));
    }
    //Thêm mới học kì
    private void InsertHocKi()
    {
         //duyệt toàn bộ table để lấy dữ liệu trên table insert vào csdl
        int row=tablehocki.getRowCount();
        for(int i=row-SoLuongThem;i<row;i++)
        {
            try
            {
                //thực hiện câu lên để insert vào table
                //InsertHocKi là Store procedure trong csdl sql server
                frmmain.connect.ExecuteSPAction("InsertHocKi", getValue(i));
                thongbao.setText("Thêm Thành Công");
                SoLuongThem--;
            }
            catch(Exception ex)
            {
                //nếu có lỗi trong quá trình thêm thì sẽ select vào row lỗi rồi thoát ra
                thongbao.setText("Có Lỗi");
                nav.selectrowtable(i);
                break;
            }
        }
        if(SoLuongThem==0)
            ThemMoi=false;
    }
    //sửa học kì
    private void EditHocKi()
    {
        //duyệt toàn bộ table rồi lấy dữ liệu edit trong csdl
        for(int i=0;i<tablehocki.getRowCount();i++)
        {
            try {
                //thực hiện câu lệnh store produre trong sql server
                frmmain.connect.ExecuteSPAction("EditHocKi", getValue2(i));
                thongbao.setText("Sửa Thành Công "+i+" Học Kì");
            } catch (Exception e) {
                //nếu có lỗi thì thoát ra, không sửa nữa
                thongbao.setText("Có Lỗi");
                nav.selectrowtable(i);
                break;
            }
        }
    }
    //lấy giá trên trên table dùng để thêm
    private String[] getValue(int row)
    {
        String[] value=new String[1];
        value[0]=tablehocki.getValueAt(row, 1).toString();
        return value;
    }
    //lấy giá trên trên table dùng để xóa
    private String[] getValue2(int row)
    {
        String[] value=new String[2];
        value[0]=tablehocki.getValueAt(row, 0).toString();
        value[1]=tablehocki.getValueAt(row, 1).toString();
        return value;
    }
    //xóa 1 dòng trên table, dùng để xóa học kì
    private void DeleteRowTable()
    {
        //lấy row cần xóa
        int rowselect=tablehocki.getSelectedRow();
        //kiểm tra xem học kì cần xóa có thể xóa được ko
        //chỉ xóa được học kì khi ko có học sinh nào trong học kì đó
        if(KiemTraXoaHocKi(rowselect))
        {
            //xuất hộp thoại yes/no để chống click nhầm
            int result = JOptionPane.showConfirmDialog(null, "Bạn Có Chắc?", "Thông Báo", JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION)
            {
                    try
                    {
                        if(SoLuongThem>0 && rowselect>=((tablehocki.getRowCount()+1)-SoLuongThem))
                        {
                            SoLuongThem--;
                            if(SoLuongThem==0)
                                ThemMoi=false;
                        }
                        else
                        {
                            String[] value={tablehocki.getValueAt(rowselect, 0).toString().trim()};
                            frmmain.connect.ExecuteSPAction("DeleteHocKi", value);
                            RemoveHK(Integer.parseInt(value[0]));
                            CapNhat();
                        }
                        ((DefaultTableModel)tablehocki.getModel()).removeRow(rowselect);
                        nav.selectrowtable((rowselect==tablehocki.getRowCount())?rowselect-1:rowselect);
                        sumrow.setText("Của "+String.valueOf(tablehocki.getRowCount()));
                    }
                    catch(Exception ex)
                    {
                        
                    }
           }
        }
    }
    //kiểm tra xóa học kì
    private boolean KiemTraXoaHocKi(int row)
    {
        //lấy mã học kì cần xóa, vì mã học kì là khóa chính trong csdl
        int MaHK=Integer.parseInt(tablehocki.getValueAt(row, 0).toString().trim());
        if(frmmain.hocsinhtronglop.KiemTraXoaHocKi(MaHK))
        {
            JOptionPane.showMessageDialog(null, "Đã Có Đối Tượng Sử Dụng Học Kỳ "+tablehocki.getValueAt(row, 1).toString()+
                    "\nBạn Không Thê Xóa");
            return false;
        }
        return true;
    }
    //xóa học kì đã xóa ra khỏi danh sách
    private void RemoveHK(int MaHK)
    {
        for(HocKiDAL hk : ArrHK)
        {
            if(hk.getMa()==MaHK)
            {
                ArrHK.remove(hk);
                break;
            }
        }
    }
    //cập nhật lại dữ liệu cho các đối tượng khác
    private void CapNhat()
    {
        frmmain.diem.AddComBoBoHocKi();
        frmmain.hocsinhtronglop.AddComBoBoxHocKi();
        frmmain.kqht.AddComBoBoxHocKi();
        frmmain.phanlop.AddComboBoxHocKi();
        frmmain.chuyenlop.AddComBoBoxHocKi();
        frmmain.phancong.AddComBoBoxHocKi();
    }
    //tạo sự kiện cho control navigation
    private void eventlabel()
    {
        reset.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ArrHK.clear();
                    getData();
                    insertTable();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                reset.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                reset.setBorder(null);
            }
        });
        delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    DeleteRowTable();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                delete.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                delete.setBorder(null);
            }
        });
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if(ThemMoi) 
                    InsertHocKi();
                else
                    EditHocKi();
                ArrHK.clear();
                getData();
                CapNhat();
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                save.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                save.setBorder(null);
            }
        });
        add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ThemMoi=true;
                    SoLuongThem++;
                    AddNewRowTable();
                    HienThiChiTiet();
                }
                catch(Exception ex)
                {
                    //JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                add.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                add.setBorder(null);
            }
        });
    }
    //set control của khung trái
    public void setKhungTrai(JTextField txtmahk,JTextField txttenhk)
    {
        this.txtmahk=txtmahk;
        this.txttenhk=txttenhk;
    }
    //Hiển Thị Row Hiện Tại Lên Nav
    public void setCurrentRow()
    {
        try
        {
            rowcurrent.setText(String.valueOf(tablehocki.getSelectedRow()+1));
        }
        catch(Exception ex)
        {
        }
    }
    //hiển thị dữ liệu chi tiết lên khung bên trái
    public void HienThiChiTiet()
    {
        try
        {
            //lấy row đang select vào
            int row=tablehocki.getSelectedRow();
            //lấy giá trị của row đó rồi đổ vào control của khung trái
            txtmahk.setText(tablehocki.getValueAt(row, 0).toString());
            txttenhk.setText(tablehocki.getValueAt(row, 1).toString());
        }catch(Exception ex)
        {
            
        }
    }
    //sự kiện click vào nút lưu vào danh sách của khung trái
    public void LuuVaoDanhSach()
    {
        try {
            if(SearchTenHK(Integer.parseInt(txtmahk.getText()))!=null)
            {
                String[] value={txtmahk.getText().trim(),txttenhk.getText()};
                frmmain.connect.ExecuteSPAction("EditHocKi", value);
                thongbao.setText("Sửa Thành Công");
                Sua(Integer.parseInt(value[0]),value[1]);
            }
            else
            {
                String[] value={txttenhk.getText()};
                frmmain.connect.ExecuteSPAction("InsertHocKi", value);
                thongbao.setText("Thêm Thành Công");
                ArrHK.add(new HocKiDAL(Integer.parseInt(txtmahk.getText().trim()), txttenhk.getText()));
            }
            CapNhatLenTable(txttenhk.getText());
            CapNhat();
        } catch (Exception e) {
        }
    }
    private void Sua(int MaHK,String TenHK)
    {
        for(HocKiDAL hk : ArrHK)
        {
            if(hk.getMa()==MaHK)
            {
                hk.setMa(MaHK);
                hk.setTen(TenHK);
                break;
            }
        }
    }
    private void CapNhatLenTable(String TenHK)
    {
        tablehocki.setValueAt(TenHK, tablehocki.getSelectedRow(), 1);
    }
}
