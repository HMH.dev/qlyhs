
package BusinessLogicLayers;

import DataAccessLayers.TonGiaoDAL;
import PresentationLayers.FrmMain;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class TonGiaoBLL {
    
    private ArrayList<TonGiaoDAL> ArrTonGiao;
    
    private JLabel reset;
    private JLabel delete;
    private JLabel save;
    private JLabel add;
    private JLabel sumrow;
    private JLabel rowcurrent;
    private JLabel thongbao;
    
    private JTable tabletongiao;
    private NavigationBLL nav;
    
    private FrmMain frmmain;
    
    private JTextField txtmatg;
    private JTextField txttentg;
    private int SoLuongThem=0;
    private boolean ThemMoi=false;
    
    public TonGiaoBLL(FrmMain frmmain)
    {
        ArrTonGiao=new ArrayList<TonGiaoDAL>();
        this.frmmain=frmmain;
        
        getData();
    }
    public void getData()
    {
        try {
             ResultSet rss = frmmain.connect.ExecuteSPSelect("SelectAllTonGiao");
            if (rss != null) {
                while (rss.next()) {
                    TonGiaoDAL tongiao=new TonGiaoDAL();
                    tongiao.setMa(rss.getInt(1));
                    tongiao.setTen(rss.getString(2));
                    ArrTonGiao.add(tongiao);
                }
                rss.close();
            }
        } catch (Exception ex) {
            
        }
    }
    public TonGiaoDAL SearchTonGiao(int MaTG)
    {
        for(TonGiaoDAL tg : ArrTonGiao)
        {
            if(MaTG==tg.getMa())
                return tg;
        }
        return null;
    }
    public void AddComboBox(JComboBox cbb)
    {
        for(TonGiaoDAL tg: ArrTonGiao)
        {
            cbb.addItem(tg);
        }
    }
    public int TimKiemMaTonGiao(String TenTG)
    {
        for(TonGiaoDAL tg : ArrTonGiao)
        {
            if(tg.getTen().equals(TenTG))
                return tg.getMa();
        }
        return 1;
    }
    
    public void setTable(JTable tabletongiao,NavigationBLL nav)
    {
        this.tabletongiao=tabletongiao;
        this.sumrow=nav.getJSumRow();
        this.rowcurrent=nav.getCurrentRow();
        this.nav=nav;
        this.nav.set(this);
    }
    
    public void insertTable()
    {
        //Đổ dữ liệu lên jtable
        tabletongiao.setModel(new DefaultTableModel(getDatatable(),getColumnTable()));
        //Select vào row 0
        nav.selectrowtable(0);
    }
    public void setCurrentRow()
    {
        try
        {
            rowcurrent.setText(String.valueOf(tabletongiao.getSelectedRow()+1));
        }
        catch(Exception ex)
        {
        }
    }
    public void setKhungTrai(JTextField txtmatg,JTextField txttentg)
    {
        this.txtmatg=txtmatg;
        this.txttentg=txttentg;
    }
    public void HienThiChiTiet()
    {
        try
        {
            int row=tabletongiao.getSelectedRow();
            txtmatg.setText(tabletongiao.getValueAt(row, 0).toString());
            txttentg.setText(tabletongiao.getValueAt(row, 1).toString());
        }catch(Exception ex)
        {
            
        }
    }
    public void setNav(JLabel reset,JLabel delete,JLabel save,JLabel add,JLabel thongbao)
    {
        this.reset=reset;
        this.delete=delete;
        this.save=save;
        this.add=add;
        this.thongbao=thongbao;
        thanhnav();
    }
    
    /*
        - Hàm tạo thanh navigation.
    */
    private void thanhnav()
    {
        reset=nav.create(228, 3,20,22, "Làm Mới Danh Sách");
        delete=nav.create(250, 3,20,22, "Xóa");
        save=nav.create(272, 3,21,22, "Lưu");
        add=nav.create(206, 5,17,19, "Thêm Mới Học Sinh");
        eventlabel();
    }
    private void AddNewRowTable()
    {
        DefaultTableModel Model = (DefaultTableModel)tabletongiao.getModel(); 
        
        int row=tabletongiao.getRowCount()-1;
        int MaMH=0;
        MaMH=Integer.parseInt(tabletongiao.getValueAt(row, 0).toString())+1;
        Model.addRow(new Object[]{MaMH,""});
        nav.selectrowtable(tabletongiao.getRowCount()-1);
        sumrow.setText("Của "+String.valueOf(tabletongiao.getRowCount()));
    }
    private void InsertTonGiao()
    {
        int row=tabletongiao.getRowCount();
        for(int i=row-SoLuongThem;i<row;i++)
        {
            try
            {
                frmmain.connect.ExecuteSPAction("InsertTonGiao", getValue(i));
                thongbao.setText("Thêm Thành Công");
                SoLuongThem--;
            }
            catch(Exception ex)
            {
                thongbao.setText("Có Lỗi");
                nav.selectrowtable(i);
                break;
            }
        }
        if(SoLuongThem==0)
            ThemMoi=false;
    }
    private void EditTonGiao()
    {
        for(int i=0;i<tabletongiao.getRowCount();i++)
        {
            try {
                frmmain.connect.ExecuteSPAction("EditTonGiao", getValue2(i));
                thongbao.setText("Sửa Thành Công "+i+" Dân Tộc");
            } catch (Exception e) {
                thongbao.setText("Có Lỗi");
                nav.selectrowtable(i);
                break;
            }
        }
    }
    private String[] getValue(int row)
    {
        String[] value=new String[1];
        value[0]=tabletongiao.getValueAt(row, 1).toString();
        return value;
    }
    private String[] getValue2(int row)
    {
        String[] value=new String[2];
        value[0]=tabletongiao.getValueAt(row, 0).toString();
        value[1]=tabletongiao.getValueAt(row, 1).toString();
        return value;
    }
    private void DeleteRowTable()
    {
        int rowselect=tabletongiao.getSelectedRow();
        if(KiemTraXoaDanToc(rowselect))
        {
            int result = JOptionPane.showConfirmDialog(null, "Bạn Có Chắc?", "Thông Báo", JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION)
            {
                    try
                    {
                        if(SoLuongThem>0 && rowselect>=((tabletongiao.getRowCount()+1)-SoLuongThem))
                        {
                            SoLuongThem--;
                            if(SoLuongThem==0)
                                ThemMoi=false;
                        }
                        else
                        {
                            String[] value={tabletongiao.getValueAt(rowselect, 0).toString().trim()};
                            frmmain.connect.ExecuteSPAction("DeleteTonGiao", value);
                            RemoveTG(Integer.parseInt(value[0]));
                            frmmain.hocsinh.addComboBoxTG();
                            frmmain.giaovien.addComboBoxTG();
                        }
                        ((DefaultTableModel)tabletongiao.getModel()).removeRow(rowselect);
                        nav.selectrowtable((rowselect==tabletongiao.getRowCount())?rowselect-1:rowselect);
                        sumrow.setText("Của "+String.valueOf(tabletongiao.getRowCount()));
                    }
                    catch(Exception ex)
                    {
                        
                    }
           }
        }
    }
    private boolean KiemTraXoaDanToc(int row)
    {
        int MaTG=Integer.parseInt(tabletongiao.getValueAt(row, 0).toString().trim());
        if(frmmain.hocsinh.SearchTonGiao(MaTG) || frmmain.giaovien.SearchTonGiao(MaTG))
        {
            JOptionPane.showMessageDialog(null, "Đã Có Đối Tượng Sử Dụng Tôn Giáo "+tabletongiao.getValueAt(row, 1).toString()+
                    "\nBạn Không Thê Xóa");
            return false;
        }
        return true;
    }
    private void RemoveTG(int MaTG)
    {
        for(TonGiaoDAL tg : ArrTonGiao)
        {
            if(tg.getMa()==MaTG)
            {
                ArrTonGiao.remove(tg);
                break;
            }
        }
    }
    //tạo sự kiện cho control navigation
    private void eventlabel()
    {
        reset.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ArrTonGiao.clear();
                    getData();
                    insertTable();
                    HienThiChiTiet();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                reset.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                reset.setBorder(null);
            }
        });
        delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    DeleteRowTable();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                delete.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                delete.setBorder(null);
            }
        });
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if(ThemMoi) 
                    InsertTonGiao();
                else
                    EditTonGiao();
                ArrTonGiao.clear();
                getData();
                frmmain.hocsinh.addComboBoxTG();
                frmmain.giaovien.addComboBoxTG();
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                save.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                save.setBorder(null);
            }
        });
        add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ThemMoi=true;
                    SoLuongThem++;
                    AddNewRowTable();
                }
                catch(Exception ex)
                {
                    //JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                add.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                add.setBorder(null);
            }
        });
    }
    
    //Tạo Comlumn table
    private Vector getColumnTable()
    {
        Vector Comlumn=new Vector();
        Comlumn.add("Mã Tôn Giáo");
        Comlumn.add("Tên Tôn Giáo");
        return Comlumn;
    }
    //Lấy data của table
    private Vector getDatatable()
    {
        Vector row=new Vector();
        Vector dr;
        
        
        for(TonGiaoDAL tg : ArrTonGiao)
        {
            dr=new Vector();
            dr.add(tg.getMa());
            dr.add(tg.getTen());
            row.add(dr);
        }
        
        return row;
    }
    
}
