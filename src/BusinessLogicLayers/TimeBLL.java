

package BusinessLogicLayers;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JLabel;


public class TimeBLL implements Runnable{
    
    //label hiển thị thời gian bên FrmMain truyền sang
    private JLabel lbtime;
    //mặc định giờ,phút, giây bằng 0
    private int Gio=0;
    private int Phut=0;
    private int Giay=0;
    //ngày tháng năm
    private String date;
    //hiện thực hàm của Runnable
    public void run(){
        //chạy vĩnh cửu
        while(true){
            try {
                //cứ 1 giây sẽ cập nhật lại thời gian lên lbtime
                Thread.sleep(1000);
                TimeTick();
            } catch (Exception e) {
            }
        }
    }
    //chạy thời gian
   private void TimeTick(){
       if(Giay++==59){
           Giay=0;
           if(Phut++==59){
               Phut=0;
               if(Gio++==23){
                   Gio=0;
                   getTime();
               } 
           }
       } 
       lbtime.setText(((Gio<10)?"0":"")+Gio+":"+((Phut<10)?"0":"")+Phut+":"+((Giay<10)?"0":"")+Giay+" "+date);
   }
   //lấy thời gian của hệ thộng
    private void getTime(){
        //lấy thời gian
        Date thoiGian = new Date();
        //lấy giây
        Giay=thoiGian.getSeconds();
        //lấy phút 
        Phut=thoiGian.getMinutes();
        //lấy giờ
        Gio=thoiGian.getHours();
        SimpleDateFormat dinhDangThoiGian = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy ");
        String strtime=dinhDangThoiGian.format(thoiGian.getTime());
        date=strtime.substring(strtime.indexOf(" "));
        //hiện thi lên lbtime
        lbtime.setText(((Gio<10)?"0":"")+Gio+":"+((Phut<10)?"0":"")+Phut+":"+((Giay<10)?"0":"")+Giay+" "+date);
    }
    //hàm khởi tạo sẽ nhân dữ liệu truyền từ FrmMain sang
    public TimeBLL(JLabel lbtime){
        this.lbtime=lbtime;
        getTime();
    }
}
