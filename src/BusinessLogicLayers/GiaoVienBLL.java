

package BusinessLogicLayers;

import DataAccessLayers.DanTocDAL;
import DataAccessLayers.GiaoVienDAL;
import DataAccessLayers.MonHocDAL;
import DataAccessLayers.TonGiaoDAL;
import PresentationLayers.FrmCalendar;
import PresentationLayers.FrmDanToc;
import PresentationLayers.FrmGiaoVien;
import PresentationLayers.FrmMain;
import PresentationLayers.FrmTonGiao;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;


public class GiaoVienBLL {
    
    //các biến này đã được giải thích bên class DanTocDLL, DiemBLL
    private JLabel reset;
    private JLabel delete;
    private JLabel save;
    private JLabel add;
    private JLabel sumrow;
    private JLabel rowcurrent;
    private JLabel thongbao;
    
    private JTable tablegiaovien;
    
    private NavigationBLL nav;
    /*  dùng để lưu danh sách giáo viên, để có thể sử dụng khi cần.
        dữ liệu được lấy trong sql server.
        dùng ArrayList để giúp cho chương trình không phải liên tục lấy dữ liệu trong csdl.
        mà chỉ cần lấy 1 lần có thể sử dụng nhiều lần*/
    private ArrayList<GiaoVienDAL> ArrGV;
    
    //dùng để liên kết với  các đối tượng khác
    private final FrmMain frmmain;
    
    //để kiểm tra xem người dùng muốn thêm mới giáo viên hay là sửa giáo viên
    //nếu ThemMoi=false thì tức là sửa, còn ngược lại là Thêm mới
    private boolean ThemMoi=false;
    //số lượng giáo viên muốn thêm
    private int soluongthem=0;
    
    //danh sách môn học
    private JComboBox cbbchuyenmon;
    //danh sách dân tộc
    private JComboBox cbbdantoc;
    //danh sách tôn giáo
    private JComboBox cbbtongiao;
    
    //các control của khung trái
    private JTextField TenGVt;
    private JTextField NgaySinht;
    private JTextField NoiSinht;
    private JComboBox GioiTinht;
    private JComboBox dtt;
    private JComboBox tgt;
    private JComboBox cbbcmt;
    private JTextField txtsdtt;
    
    //hàm khởi tạo có tham số truyền vào
    public GiaoVienBLL(FrmMain frmmain)
    {
        //tạo 1 danh sách liên kết mới
        ArrGV=new ArrayList<GiaoVienDAL>();
        
        this.frmmain=frmmain;
        
        /*  lấy dữ liệu trong csdl
            mục đích của việc đặt hàm getData() trong hàm khởi tạo là để
            khi chương trình vừa chạy sẽ lấy toàn bộ dữ liệu trong csdl lên.
            để có thể sử dụng khi cần.
            - Lợi ích của cách này là giúp chương trình không phải liên tục lấy
            dữ liệu từ csdl mà chỉ cần lấy 1 lần sử dụng nhiều lần, như vậy sẽ giúp 
            chương trình chạy nhanh hơn trong 'khi sử dụng'.
            - Bất lợi: chương trình sẽ 'khởi động' chậm hơn.
        */
        getData();
    }
    
    //Lấy dữ liệu trên csdl, sau đó lưu vào ArrayList để sử dụng khi cần.
    private void getData()
    {
        try {
            //rss dùng để lấy dữ liệu trả về khi thực thi thành công câu lệnh truy vần sql
            //SelectAllGiaoVien là Store procedure trong csdl sql server
             ResultSet rss = frmmain.connect.ExecuteSPSelect("SelectAllGiaoVien");
             //Kiểm tra dữ liệu có bị lỗi không
                if (rss != null) {
                    //lấy toàn bộ dữ liệu trong csld
                    while (rss.next()) {
                        GiaoVienDAL gv=new GiaoVienDAL();
                        gv.setMaGV(rss.getInt(1));
                        gv.setHoTen(rss.getString(2));
                        gv.setGioiTinh(rss.getBoolean(3));
                        gv.setNgaySinh(rss.getString(4));
                        gv.setNoiSinh(rss.getString(5));
                        gv.setMaMonHoc(frmmain.monhoc.SearchMonHoc(rss.getInt(6)));
                        gv.setDanToc(frmmain.DanToc.SearchDanToc(rss.getInt(7)));
                        gv.setTonGiao(frmmain.TonGiao.SearchTonGiao(rss.getInt(8)));
                        gv.setDienThoai(rss.getString(9));
                        ArrGV.add(gv);
                    }
                rss.close();
            }
        } catch (Exception ex) {
            
        }
    }
    //Tìm kiếm giáo viên theo mã giáo viên
    public GiaoVienDAL SearchTenGiaoVien(int MaGV)
    {
        //duyện toàn bộ danh sách để tìm. nếu tìm thấy thì trả về giáo viên tìm thấy
        // còn không tìm thấy thì trả về null
        for(GiaoVienDAL gv : ArrGV)
        {
            if(gv.getMaGV()==MaGV)
                return gv;
        }
        return null;
    }
    //Đổ giáo viên vào combobox
    public void AddComboBox(JComboBox cbb)
    {
        for(GiaoVienDAL gv : ArrGV)
        {
            cbb.addItem(gv);
        }
    }
    //Sửa giáo viên
    private void EditGiaoVien()
    {
        //duyệt toàn bộ table giáo viên để update vào csdl
        for(int i=0;i<tablegiaovien.getRowCount();i++)
        {
            try
            {
                
                frmmain.connect.ExecuteSPAction("EditGiaoVien", getValue(i));
                 thongbao.setText("Sửa Thành Công "+(i+1)+" Giáo Viên");
            }
            catch(Exception ex)
            {
                thongbao.setText("Có Lỗi Ở Giáo Viên "+tablegiaovien.getValueAt(i, 1).toString());
                break;
            }
        }
    }
    //Thêm mới giáo viên
    private void InsertGiaoVien()
    {
          
        int row=tablegiaovien.getRowCount();
        for(int i=row-soluongthem;i<row;i++)
        {
            try {
                //Insert Vào CSDL
                frmmain.connect.ExecuteSPAction("InsertGiaoVien", getValue(i));
                thongbao.setText("Thêm Thành Công");
                soluongthem--;
            } catch (Exception e) {
                thongbao.setText("Có Lỗi "+e.getMessage());
                nav.selectrowtable(i);
                break;
            }
                    
               
        }
        if(soluongthem==0) 
            ThemMoi=false;
    }
    //Lấy từng giáo viên trong row table để thêm và sửa giáo viên
    private String[] getValue(int row)
    {
        String[] value=new String[9];
        value[0]=tablegiaovien.getValueAt(row, 0).toString().trim();
        value[1]=FormatName(tablegiaovien.getValueAt(row, 1).toString());
        value[2]=(tablegiaovien.getValueAt(row, 2).toString().equals("Nam"))?"true":"false";
        value[3]=tablegiaovien.getValueAt(row, 3).toString();
        value[4]=tablegiaovien.getValueAt(row, 4).toString();
        value[5]=String.valueOf(((MonHocDAL)(tablegiaovien.getValueAt(row, 5))).getMa());
        value[6]=String.valueOf(((DanTocDAL)(tablegiaovien.getValueAt(row, 6))).getMa());
        value[7]=String.valueOf(((TonGiaoDAL)(tablegiaovien.getValueAt(row,7))).getMa());
        value[8]=tablegiaovien.getValueAt(row, 8).toString();
        return value;
    }
    
    //Xóa 1 row trên table giáo viên, Và xóa giáo viên. Đã được giải thích khá kĩ bên hàm xóa của DanTocBLL
    private void DeleteRowTable()
    {
        //lấy giáo viên muốn xóa
        int rowselect=tablegiaovien.getSelectedRow();
        //Kiểm tra xem giáo viên muốn xóa có thể xóa được không
        //Giáo viên chỉ xóa được khi giáo viên đó không phải là GVCN của bất kì lớp nào
        if(KiemTraXoaGV(rowselect))
        {
            //xuất hiện hộp thoại thông báo(chống click nhầm)
            int result = JOptionPane.showConfirmDialog(null, "Bạn Có Chắc?", "Thông Báo", JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION)
            {
                    try
                    {
                        if(soluongthem>0 && rowselect>=((tablegiaovien.getRowCount()+1)-soluongthem))
                        {
                            soluongthem--;
                            if(soluongthem==0)
                                ThemMoi=false;
                        }
                        else
                        {
                            String[] value={tablegiaovien.getValueAt(rowselect, 0).toString().trim()};
                            frmmain.connect.ExecuteSPAction("DeleteGiaoVien", value);
                            RemoveGV(Integer.parseInt(value[0]));
                        }
                        ((DefaultTableModel)tablegiaovien.getModel()).removeRow(rowselect);
                        nav.selectrowtable((rowselect==tablegiaovien.getRowCount())?rowselect-1:rowselect);
                        sumrow.setText("Của "+String.valueOf(tablegiaovien.getRowCount()));
                    }
                    catch(Exception ex)
                    {
                    }
           }
        }
    }
    //Kiểm xem giáo viên cần xóa có là giáo viên chủ nhiệm của lớp nào chưa.
    //Nếu đã là GVCN của 1 lớp bất kì thì không cho xóa
    private boolean KiemTraXoaGV(int row)
    {
        int MaGV=Integer.parseInt(tablegiaovien.getValueAt(row, 0).toString().trim());
        if(frmmain.lop.KiemTraXoaGiaoVien(MaGV))
        {
            JOptionPane.showMessageDialog(frmmain, "Giáo Viên "+tablegiaovien.getValueAt(row, 1).toString()+
                    "\nĐã Là GVCN Của Một Lớp Học.\nKhông Thể Xóa");
            return false;
        }
        if(!frmmain.phancong.KiemTraXoaGV(MaGV))
        {
            JOptionPane.showMessageDialog(frmmain, "Giáo Viên "+tablegiaovien.getValueAt(row, 1).toString()+
                    "\nĐã Có Trong Danh Sách Phân Công Dạy Học.\nKhông Thể Xóa");
            return false;
        }
        return true;
    }
    //Xóa giáo viên khỏi ArrayList
    private void RemoveGV(int MaGV)
    {
        for(GiaoVienDAL gv : ArrGV)
        {
            if(gv.getMaGV()==MaGV)
            {
                ArrGV.remove(gv);
                break;
            }
        }
    }
    //Hàm Định Dạng Lại Tên
    //VD:
    //Input:  nguyen van a
    //Output: Nguyen Van A
    private String FormatName(String Name)
    {
        //Chuyển Đổi Về Chữ Thường
        Name=Name.toLowerCase().trim();
        String str="";
        char c=Name.charAt(0);
        c-=32;
        str+=c;
        for(int i=1;i<Name.length();i++)
        {
            //Nếu Kí Tự Là Khoảng Trắng Thị Kí Tự Tiếp Theo Sẽ Được Chuyển Về Hoa
            if(Name.charAt(i)==' ')
            {
                //Lấy Kí Tự Tiếp Theo
                c=Name.charAt(i+1);
                //Kiểm Tra Xem Kí Tự Đó Có Phải là Khoảng Trắng.
                //Nếu Không Phải Là Khoảng trắng thì chuyển đổi về hoa con không thì thôi.
                if(c!=' ')
                {
                    c-=32;
                    str+=" "+c;
                    i++;
                }
            }
            else
                str+=Name.charAt(i);
        }
        return str;
    }
    
    
    //Set Control Của Table
    public void setTable(JTable tablegiaovien,JPanel pnl)
    {
        nav=new NavigationBLL(tablegiaovien, pnl, reset, reset, reset, reset, rowcurrent, sumrow,1);
        this.tablegiaovien=tablegiaovien;
        this.sumrow=nav.getJSumRow();
        this.rowcurrent=nav.getCurrentRow();
        this.nav.set(this);
    }
    //Đổ dữ liệu vào table
    public void insertTable()
    {
        InsertDataTable();
        
        EditColumnTable();
    }
    private void InsertDataTable()
    {
        tablegiaovien.setModel(new DefaultTableModel(getDatatable(),getColumnTable()));
        nav.selectrowtable(0);
        nav.getSumRow();
    }
    //Sửa lại row của table
    private void EditColumnTable()
    {
        String[] gioitinh = {"Nam", "Nữ"};
        JComboBox gioitinhCombo = new JComboBox(gioitinh);
        TableColumn Column = tablegiaovien.getColumnModel().getColumn(2);
        Column.setCellEditor(new DefaultCellEditor(gioitinhCombo));
        
        EditColumnDanToc();
        EditColumnChuyenMon();
        EditColumnTonGiao();
        
        JTextField txt=new JTextField();
        txt.setEnabled(false);
        Column = tablegiaovien.getColumnModel().getColumn(0);
        Column.setCellEditor(new DefaultCellEditor(txt));
    }
 
    private void EditColumnDanToc()
    {
        JComboBox cbbdt=new JComboBox();
        frmmain.DanToc.AddComboBox(cbbdt);
        TableColumn Column = tablegiaovien.getColumnModel().getColumn(6);
        Column.setCellEditor(new DefaultCellEditor(cbbdt));
    }
    private void EditColumnTonGiao()
    {
        JComboBox cbbtg=new JComboBox();
        frmmain.TonGiao.AddComboBox(cbbtg);
        TableColumn Column = tablegiaovien.getColumnModel().getColumn(7);
        Column.setCellEditor(new DefaultCellEditor(cbbtg));
    }
    private void EditColumnChuyenMon()
    {
        JComboBox cbbcm=new JComboBox();
        frmmain.monhoc.addComboBox(cbbcm);
        TableColumn Column = tablegiaovien.getColumnModel().getColumn(5);
        Column.setCellEditor(new DefaultCellEditor(cbbcm));
    }
    
    public void addComboBox(JComboBox cbbchuyenmon,JComboBox cbbDanToc,JComboBox cbbTonGiao)
    {
        frmmain.monhoc.addComboBox(cbbchuyenmon);
        frmmain.DanToc.AddComboBox(cbbDanToc);
        frmmain.TonGiao.AddComboBox(cbbTonGiao);
        this.cbbchuyenmon=cbbchuyenmon;
        this.cbbdantoc=cbbDanToc;
        this.cbbtongiao=cbbTonGiao;
    }
    public void addComboBoxCM()
    {
        cbbchuyenmon.removeAllItems();
        frmmain.monhoc.addComboBox(cbbchuyenmon);
        EditColumnChuyenMon();
    }
    public void addComboBoxTG()
    {
        cbbtongiao.removeAllItems();
        frmmain.TonGiao.AddComboBox(cbbtongiao);
        EditColumnTonGiao();
    }
    public void addComboBoxDT()
    {
        cbbdantoc.removeAllItems();
        frmmain.DanToc.AddComboBox(cbbdantoc);
        EditColumnDanToc();
    }
    //Set Control Navigation
    public void setNav(JLabel thongbao)
    {
        this.thongbao=thongbao;
        thanhnav();
    }
    //Thanh Navigation
    private void thanhnav()
    {
        reset=nav.create(228, 3,20,22, "Làm Mới Danh Sách");
        delete=nav.create(250, 3,20,22, "Xóa");
        save=nav.create(272, 3,21,22, "Lưu");
        add=nav.create(206, 5,17,19, "Thêm Mới Giáo Viên");
        eventlabel();
    }
    public void setKhungTrai(JTextField TenGVt,JTextField NgaySinht ,JTextField NoiSinht,JComboBox GioiTinht,JComboBox dtt,
            JComboBox tgt,JComboBox cbbcmt,JTextField txtsdtt)
    {
        this.TenGVt=TenGVt;
        this.NgaySinht=NgaySinht;
        this.NoiSinht=NoiSinht;
        this.GioiTinht=GioiTinht;
        this.dtt=dtt;
        this.tgt=tgt;
        this.cbbcmt=cbbcmt;
        this.txtsdtt=txtsdtt;
    }
     public void HienThiChiTiet()
    {
        try
        {
            int row=tablegiaovien.getSelectedRow();
            TenGVt.setText(tablegiaovien.getValueAt(row, 1).toString());
            NgaySinht.setText(tablegiaovien.getValueAt(row, 3).toString());
            NoiSinht.setText(tablegiaovien.getValueAt(row, 4).toString());
            GioiTinht.setSelectedItem(tablegiaovien.getValueAt(row, 2).toString());
            dtt.setSelectedItem((DanTocDAL)tablegiaovien.getValueAt(row, 6));
            tgt.setSelectedItem((TonGiaoDAL)tablegiaovien.getValueAt(row, 7));
            cbbcmt.setSelectedItem((MonHocDAL)tablegiaovien.getValueAt(row, 5));
            txtsdtt.setText(tablegiaovien.getValueAt(row, 8).toString());
        }
        catch(Exception ex)
        {
            
        }
    }
     //Hiển Thị Row Hiện Tại Lên Nav
    public void setCurrentRow()
    {
        try
        {
            rowcurrent.setText(String.valueOf(tablegiaovien.getSelectedRow()+1));
        }
        catch(Exception ex)
        {
        }
    }
    //reset lại table
    private void ResetTable()
    {
        tablegiaovien.removeAll();
        ArrGV.clear();
        getData();
        InsertDataTable();
        setCurrentRow();
        HienThiChiTiet();
    }
    //các sự kiện của icon trên thanh navigation
    private void eventlabel()
    {
        reset.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ResetTable();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                reset.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                reset.setBorder(null);
            }
        });
        delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    DeleteRowTable();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                delete.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                delete.setBorder(null);
            }
        });
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if(ThemMoi) 
                    InsertGiaoVien();
                else
                    EditGiaoVien();
                
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                save.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                save.setBorder(null);
            }
        });
        add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ThemMoi=true;
                    soluongthem++;
                    AddNewRowTable();
                    HienThiChiTiet();
                }
                catch(Exception ex)
                {
                    //JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                add.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                add.setBorder(null);
            }
        });
    }
    //Thếm 1 Row Mới Vào Table. Dùng để thêm giáo viên mới
    private void AddNewRowTable()
    {
        DefaultTableModel Model = (DefaultTableModel)tablegiaovien.getModel(); 
        
        int MaGV=ArrGV.get(ArrGV.size()-1).getMaGV();
        Model.addRow(new Object[]{MaGV+1,"","Nam","","","","","","",""});
        nav.selectrowtable(tablegiaovien.getRowCount()-1);
        sumrow.setText("Của "+String.valueOf(tablegiaovien.getRowCount()));
        
    }
     //Tạo Comlumn table
    private Vector getColumnTable()
    {
        Vector Comlumn=new Vector();
        Comlumn.add("Mã Giáo Viên");
        Comlumn.add("Tên Giáo Viên");
        Comlumn.add("Giới Tính");
        Comlumn.add("Ngày Sinh");
        Comlumn.add("Nơi Sinh");
        Comlumn.add("Chuyên Môn");
        Comlumn.add("Dân Tộc");
        Comlumn.add("Tôn Giáo");
        Comlumn.add("Số Điện Thoại");
        return Comlumn;
    }
    
    //Lấy data của table
    private Vector getDatatable()
    {
        Vector row=new Vector();
        Vector dr;
        
        
        for(GiaoVienDAL gv : ArrGV)
        {
            dr=new Vector();
            dr.add(gv.getMaGV());
            dr.add(gv.getHoTen());
            dr.add((gv.isGioiTinh())?"Nam":"Nữ");
            dr.add(gv.getNgaySinh());
            dr.add(gv.getNoiSinh());
            dr.add(gv.getMaMonHoc());
            dr.add(gv.getDanToc());
            dr.add(gv.getTonGiao());
            dr.add(gv.getDienThoai());
            row.add(dr);
        }
        
        return row;
    }
    //lấy data của table khi có điều kiện
    private Vector getDatatable(int MaGV,String TenGV)
    {
        Vector row=new Vector();
        Vector dr;
        
        
        for(GiaoVienDAL gv : ArrGV)
        {
            if((MaGV!=0 && gv.getMaGV()==MaGV) || (TenGV!="" && gv.getHoTen().indexOf(TenGV)!=-1))
            {
                dr=new Vector();
                dr.add(gv.getMaGV());
                dr.add(gv.getHoTen());
                dr.add((gv.isGioiTinh())?"Nam":"Nữ");
                dr.add(gv.getNgaySinh());
                dr.add(gv.getNoiSinh());
                dr.add(gv.getMaMonHoc());
                dr.add(gv.getDanToc());
                dr.add(gv.getTonGiao());
                dr.add(gv.getDienThoai());
                row.add(dr);
            }
        }
        
        return row;
    }
    //Tìm kiếm giáo viên
    private void SearchGV()
    {
        try
        {
            int loaisearch=cbbnoisearch.getSelectedIndex();
            tablegiaovien.setModel(new DefaultTableModel(getDatatable((loaisearch==0)?Integer.parseInt(txtsdtt.getText()):0,(loaisearch==0)?"":txtsdtt.getText()),getColumnTable()));
            nav.selectrowtable(0);
        }
        catch(Exception ex)
        {
            
        }
    }
    //form search
    private boolean taochua=true;
    private JComboBox cbbnoisearch=new JComboBox();
    private JLabel lbsearchh=new JLabel(new ImageIcon(getClass().getResource("/PresentationLayers/Images/buttonsearch.png")));
    public void frmsearch(JPanel pnlthongtinhocsinnh,JTextField txtngaysinh,JLabel jLabel6)
    {
        if(taochua)
        {
            cbbnoisearch.setSize(169,25);
            cbbnoisearch.setLocation(txtngaysinh.getX(),txtngaysinh.getY()+8);
            cbbnoisearch.addItem("Mã Giáo Viên");
            cbbnoisearch.addItem("Tên Giáo Viên");
            pnlthongtinhocsinnh.add(cbbnoisearch);
            lbsearchh.setSize(172,25);
            lbsearchh.setLocation(jLabel6.getX(),jLabel6.getY()+8);
            pnlthongtinhocsinnh.add(lbsearchh);
            lbsearchh.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
            taochua=false;
            lbsearchh.addMouseListener(new java.awt.event.MouseAdapter() {
                public void mouseClicked(java.awt.event.MouseEvent evt) {
                    SearchGV();
                }
            });
        }
        else
        {
            cbbnoisearch.setVisible(true);
            lbsearchh.setVisible(true);
        } 
    }
    public void frmchitiet(JTextField txtsodienthoai)
    {
        cbbnoisearch.setVisible(false);
         lbsearchh.setVisible(false);
         txtsodienthoai.setText(tablegiaovien.getValueAt(tablegiaovien.getSelectedRow(), 8).toString());
    }
    public boolean getTaoChua()
    {
        return this.taochua;
    }
    public void showCalendar(JTextField txtngays)
    {
        JDialog dialog=new JDialog(frmmain,"Calendar",true);
        dialog.setSize(158, 170);
        dialog.setLocation(txtngays.getX(), txtngays.getY()+dialog.getHeight()+45);
        dialog.add(new FrmCalendar(txtngays,dialog));
        dialog.setVisible(true);
    }   
    public void showformdantoc()
    {
        frmmain.createTab("Dân Tộc", new FrmDanToc(frmmain.DanToc), 11);
    }
    public void showformtongiao()
    {
        frmmain.createTab("Tôn Giáo", new FrmTonGiao(frmmain.TonGiao), 12);
    }
    public void showformmonhoc(FrmGiaoVien frmgiaovien)
    {
        frmmain.createtabmohoc();
    }
    public boolean SearchDanToc(int MaDT)
    {
        for(GiaoVienDAL gv : ArrGV)
        {
            if(gv.getDanToc().getMa()==MaDT)
            {
                return true; 
            }
        }
        return false;
    }
    public boolean SearchTonGiao(int MaTG)
    {
        for(GiaoVienDAL gv : ArrGV)
        {
            if(gv.getTonGiao().getMa()==MaTG)
            {
                return true; 
            }
        }
        return false;
    }
    public boolean KiemTraXoaMonHoc(int MaMH)
    {
        for(GiaoVienDAL gv : ArrGV)
        {
            if(gv.getMaMonHoc().getMa()==MaMH)
            {
                return true; 
            }
        }
        return false;
    }
    public ArrayList<GiaoVienDAL> getArrayGV()
    {
        return this.ArrGV;
    }
    public GiaoVienDAL getGiaoVien()
    {
        return ArrGV.get(0);
    }
    public void LuuVaoDanhSach()
    {
        String[] value=new String[9];
        value[0]=tablegiaovien.getValueAt(tablegiaovien.getSelectedRow(), 0).toString();
        value[1]=TenGVt.getText();
        value[2]=(GioiTinht.getSelectedItem().toString().equals("Nam"))?"True":"false";
        value[3]=NgaySinht.getText();
        value[4]=NoiSinht.getText();
        value[5]=String.valueOf(((MonHocDAL)(cbbcmt.getSelectedItem())).getMa());
        value[6]=String.valueOf(((DanTocDAL)(cbbdantoc.getSelectedItem())).getMa());
        value[7]=String.valueOf(((TonGiaoDAL)(cbbtongiao.getSelectedItem())).getMa());
        value[8]=txtsdtt.getText();
        try
            {
                if(SearchTenGiaoVien(Integer.parseInt(value[0]))!=null) 
                {
                    frmmain.connect.ExecuteSPAction("EditGiaoVien", value);
                    thongbao.setText("Sửa Thành Công");
                }
                else
                {
                    frmmain.connect.ExecuteSPAction("InsertGiaoVien", value);
                    thongbao.setText("Thêm Thành Công");
                }
                CapNhatLenTable(value);
            }
            catch(Exception ex)
            {
                thongbao.setText("Có Lỗi");
            }
    }
    private void CapNhatLenTable(String[] value)
    {
        int row=tablegiaovien.getSelectedRow();
        tablegiaovien.setValueAt(value[1], row, 1);
        tablegiaovien.setValueAt((value[2].equals("true"))?"Nam":"Nữ", row, 2);
        tablegiaovien.setValueAt(value[3], row, 3);
        tablegiaovien.setValueAt(value[4], row, 4);
        tablegiaovien.setValueAt(cbbcmt.getSelectedItem().toString(), row, 5);
        tablegiaovien.setValueAt(cbbdantoc.getSelectedItem().toString(), row, 6);
        tablegiaovien.setValueAt(cbbtongiao.getSelectedItem().toString(), row, 7);
        tablegiaovien.setValueAt(value[8], row, 8);
    }
}
