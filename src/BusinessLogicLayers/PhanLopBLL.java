

package BusinessLogicLayers;

import DataAccessLayers.HocKiDAL;
import DataAccessLayers.HocSinh_LopDAL;
import DataAccessLayers.LopDAL;
import DataAccessLayers.NamHocDAL;
import DataAccessLayers.PhanLopDAL;
import PresentationLayers.FrmMain;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class PhanLopBLL {
    private FrmMain frmmain;
    
    private JComboBox cbbnamhoc;
    private JComboBox cbblop;
    
    private ArrayList<PhanLopDAL> Arr=new ArrayList<PhanLopDAL>();
    
    private JTable tablehocsinh;
    private JTable tablelop;
    private JComboBox cbbhocki;
    
    public PhanLopBLL(FrmMain frmmain)
    {
        this.frmmain=frmmain;
        
        getData();
    }
    public void AddComboBoxNamHoc(JComboBox cbbnamhoc)
    {
        this.cbbnamhoc=cbbnamhoc;
        AddComBoxBoxNamHoc();
    }
    public void AddComBoxBoxNamHoc()
    {
        cbbnamhoc.removeAllItems();
        frmmain.namhoc.AddComboBox(cbbnamhoc);
    }
    public void AddComboBoxLop(JComboBox cbblop)
    {
        cbblop.removeAllItems();
        frmmain.lop.AddComboBoxLop(cbblop, ((NamHocDAL)cbbnamhoc.getSelectedItem()).getMa());
        this.cbblop=cbblop;
    }
    public void AddComboBoxHocKi(JComboBox cbbhocki)
    {
        this.cbbhocki=cbbhocki;
        AddComboBoxHocKi();
    }
    public void AddComboBoxHocKi()
    {
        cbbhocki.removeAllItems();
        frmmain.hocki.AddComboBox(cbbhocki);
    }
    private void getData()
    {
        try {
             ResultSet rss = frmmain.connect.ExecuteSPSelect("SelectHSKoCoLop");
                if (rss != null) {
                    while (rss.next()) {
                        PhanLopDAL pl=new PhanLopDAL();
                        
                        pl.setMaHS(rss.getString(1));
                        pl.setTenHS(rss.getString(2));
                        
                        Arr.add(pl);
                        
                    }
                rss.close();
            }
        } catch (Exception ex) {
            
        }
    }
    public void setTable(JTable tablehocsinh,JTable tablelop)
    {
        this.tablehocsinh=tablehocsinh;
        this.tablelop=tablelop;
    }
    public void insertTable()
    {
        DefaultTableModel tableModel = new DefaultTableModel(getDatatable(),getColumnTable()) {
            @Override
            public boolean isCellEditable(int row, int column) {
               return false;
            }
        };
        tablehocsinh.setModel(tableModel);
    }
    public void insertTableLop()
    {
        try
        {
            int MaLop=((LopDAL)(cbblop.getSelectedItem())).getMaLop();
            DefaultTableModel tableModel = new DefaultTableModel(frmmain.hocsinhtronglop.getDatatable(MaLop),getColumnTable()) {
                @Override
                public boolean isCellEditable(int row, int column) {
                   return false;
                }
            };
            tablelop.setModel(tableModel);
        }
        catch(Exception ex)
        {
            
        }
    }
    //Tạo Comlumn table
    private Vector getColumnTable()
    {
        Vector Comlumn=new Vector();
        Comlumn.add("Mã Học Sinh");
        Comlumn.add("Tên Học Sinh");
        return Comlumn;
    }
    //Lấy data của table
    private Vector getDatatable()
    {
        Vector row=new Vector();
        Vector dr;
        
        
        for(PhanLopDAL pl : Arr)
        {
            dr=new Vector();
            dr.add(pl.getMaHS().trim());
            dr.add(pl.getTenHS());
            row.add(dr);
        }
        
        return row;
    }
    public void UpdateData(String MaHS,String TenHS)
    {
        DefaultTableModel Model = (DefaultTableModel)tablehocsinh.getModel(); 
        Model.addRow(new Object[]{MaHS,TenHS});
    }
    private void insertDatatableChuyen(int r)
    {
        HocSinh_LopDAL hsl=new HocSinh_LopDAL();
        
        hsl.setHocSinh(frmmain.hocsinh.SearchHocSinh(tablehocsinh.getValueAt(r, 0).toString().trim()));
        hsl.setHocKi(frmmain.hocki.SearchTenHK(((HocKiDAL)cbbhocki.getSelectedItem()).getMa()));
        hsl.setLop(frmmain.lop.SearchLop(((LopDAL)(cbblop.getSelectedItem())).getMaLop()));
        hsl.setNamHoc(frmmain.namhoc.SearchTenNamHoc(((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa()));
        
        frmmain.hocsinhtronglop.UpdateData(hsl);
        DefaultTableModel Model = (DefaultTableModel)tablelop.getModel(); 
        Model.addRow(new Object[]{tablehocsinh.getValueAt(r, 0).toString(),tablehocsinh.getValueAt(r, 1).toString()});
        Model = (DefaultTableModel)tablehocsinh.getModel();
        Model.removeRow(r);
    }
    private void insertDatatableXoa(int r)
    {
        HocSinh_LopDAL hsl=new HocSinh_LopDAL();
        
        hsl.setHocSinh(frmmain.hocsinh.SearchHocSinh(tablelop.getValueAt(r, 0).toString().trim()));
        hsl.setHocKi(frmmain.hocki.SearchTenHK(cbbhocki.getSelectedIndex()+1));
        hsl.setLop(frmmain.lop.SearchLop(((LopDAL)(cbblop.getSelectedItem())).getMaLop()));
        hsl.setNamHoc(frmmain.namhoc.SearchTenNamHoc(((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa()));
        
        frmmain.hocsinhtronglop.UpdateData(hsl);
        DefaultTableModel Model = (DefaultTableModel)tablehocsinh.getModel(); 
        Model.addRow(new Object[]{tablelop.getValueAt(r, 0).toString(),tablelop.getValueAt(r, 1).toString()});
        Model = (DefaultTableModel)tablelop.getModel();
        Model.removeRow(r);
    }
    public void ChuyenHocSinh()
    {
        try
        {
            int row=tablehocsinh.getSelectedRow();
            String MaHocSinh=tablehocsinh.getValueAt(row, 0).toString().trim();
            int MaLop=((LopDAL)(cbblop.getSelectedItem())).getMaLop();
            int MaHK=((HocKiDAL)(cbbhocki.getSelectedItem())).getMa();
            int MaNH=((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa();
            String[] value={MaHocSinh,String.valueOf(MaLop),String.valueOf(MaHK),String.valueOf(MaNH)};
            frmmain.connect.ExecuteSPAction("PhanLop", value);
            insertDatatableChuyen(row);
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Hãy Chọn Một Học Sinh Cần Chuyển");
        }
    }
    public void XoaHocSinhKhoiLop()
    {
        try
        {
            int row=tablelop.getSelectedRow();
            String MaHocSinh=tablelop.getValueAt(row, 0).toString().trim();
            int MaLop=((LopDAL)(cbblop.getSelectedItem())).getMaLop();
            int MaHK=((HocKiDAL)(cbbhocki.getSelectedItem())).getMa();
            int MaNH=((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa();
            String[] value={MaHocSinh.trim(),String.valueOf(MaLop),String.valueOf(MaHK),String.valueOf(MaNH)};
            if(KiemTraXoaKhoiLop(MaHocSinh, MaHK, MaNH, row)) 
            {
                int result = JOptionPane.showConfirmDialog(null, "Bạn Có Chắc?", "Thông Báo", JOptionPane.YES_NO_OPTION);
                if (result == JOptionPane.YES_OPTION)
                {
                    frmmain.connect.ExecuteSPAction("XoaHSKhoiLop", value);
                    insertDatatableXoa(row);
                }
            }
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Hãy Chọn Một Học Sinh Cần Xóa Khỏi Lớp");
        }
    }
    private boolean KiemTraXoaKhoiLop(String MaHS,int MaHK,int MaNH,int row)
    {
        if(frmmain.diem.KiemTraXoaHS(MaHS.trim(), MaHK, MaNH)) 
        {
            JOptionPane.showMessageDialog(frmmain, "Học Sinh "+tablelop.getValueAt(row, 1).toString()+
                    " Đã Có Điểm.\nKhông Thể Xóa");
            return false;
        }
        if(frmmain.hocsinhtronglop.KiemTraXoaHS(MaHS.trim(), MaNH)>1)
        {
            JOptionPane.showMessageDialog(frmmain, "Học Sinh "+tablelop.getValueAt(row, 1).toString()+
                    " Đã Có Trong Hồ Sơ.\nKhông Thể Xóa");
            return false;
        }
        return true;
         
    }
    public void UpdateData()
    {
        Arr.clear();
        getData();
        insertTable();
    }
    public void UpdateCbbLop()
    {
        cbblop.removeAllItems();
        frmmain.lop.AddComboBoxLop(cbblop, ((NamHocDAL)cbbnamhoc.getSelectedItem()).getMa());
    }
}
