

package BusinessLogicLayers;

import DataAccessLayers.clsDatabase;
import PresentationLayers.FrmMain;
import java.io.File;
import java.sql.PreparedStatement;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;


public class SaoLuuPhucHoiBLL {
    public SaoLuuPhucHoiBLL(FrmMain frmmain)
    {
        phuchoi(frmmain);
    }
    public SaoLuuPhucHoiBLL(clsDatabase connect)
    {
        saoluu(connect);
    }
    private void saoluu(clsDatabase connect)
    {
        try
        {
            String Path="";
            if(getClass().getResource("/DataAccessLayers").toString().indexOf("jar!")!=-1)
            {
                Path=getClass().getResource("/DataAccessLayers").toString().replace("jar:file:/", "").replace("/QuanLyHocSinhPhoThong.jar!", "").replace("/DataAccessLayers", "");
            }
            else
                Path=getClass().getResource("/DataAccessLayers").toString().replace("file:/", "");
            JFileChooser fc = new JFileChooser(Path);
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Backup","bak");
            fc.setFileFilter(filter);
            fc.setSelectedFile(new File("QLHS_PhoThong_BAK.bak"));
            int returnVal = fc.showSaveDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                String[] value={connect.getBatabase(),file.getPath()};
                connect.ExecuteSPAction("BackupData", value);
                JOptionPane.showMessageDialog(null, "Sao Lưu Dữ Liệu Thành Công");
            }
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Lỗi "+ex.getMessage());
        }
    }
    private void phuchoi(FrmMain main)
    {
        try {
            String Path="";
            if(getClass().getResource("/DataAccessLayers").toString().indexOf("jar!")!=-1)
            {
                Path=getClass().getResource("/DataAccessLayers").toString().replace("jar:file:/", "").replace("/QuanLyHocSinhPhoThong.jar!", "").replace("/DataAccessLayers", "");
            }
            else
                Path=getClass().getResource("/DataAccessLayers").toString().replace("file:/", "");
            JFileChooser fc = new JFileChooser(Path);
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Backup","bak");
            fc.setFileFilter(filter);
            fc.setSelectedFile(new File("QLHS_PhoThong_BAK.bak"));
            int returnVal = fc.showOpenDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                update("USE master RESTORE DATABASE "+main.connect.getBatabase()+" FROM DISK = '"+file.getPath()+"'",main);
                JOptionPane.showMessageDialog(null, "Phục Hồi Dữ Liệu Thành Công");
                main.connectdb();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Lỗi "+ex.getMessage());
        }
            //
    }
    public void update(String query,FrmMain main)
    {
        try
        {
            PreparedStatement pstmt=main.connect.getConnect().prepareStatement(query);
            pstmt.executeUpdate();
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, ex.getMessage());
        }
    }
}
