

package BusinessLogicLayers;

import DataAccessLayers.MonHocDAL;
import PresentationLayers.FrmMain;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;


public class MonHocBLL {
    
    
    private JLabel reset;
    private JLabel delete;
    private JLabel save;
    private JLabel add;
    private JLabel sumrow;
    private JLabel rowcurrent;
    private JLabel thongbao;
    private ArrayList<MonHocDAL> ArrMonHoc;
    
    private JTable tablemonhoc;
    private NavigationBLL nav;
    
    private int soluongthem=0;
    private boolean ThemMoi=false;
    
    private FrmMain frmmain;
    
    private JTextField txttenmonhoc;
    private JTextField txtheso;
    
    public MonHocBLL(FrmMain frmmain)
    {
        ArrMonHoc=new ArrayList<MonHocDAL>();
        this.frmmain=frmmain;
        
        getData();
    }
    
    private void getData()
    {
        try
        {
            ResultSet rs=frmmain.connect.ExecuteSPSelect("SelectAllMonHoc");
            while(rs.next())
            {
                MonHocDAL mh=new MonHocDAL();
                mh.setMa(rs.getInt(1));
                mh.setTen(rs.getString(2));
                mh.setHeSo(rs.getInt(3));
                ArrMonHoc.add(mh);
            }
             rs.close();
        }
        catch(Exception ex)
        {
            
        }
    }
    
    public void setTable(JTable tablemonhoc,NavigationBLL nav)
    {
        this.tablemonhoc=tablemonhoc;
        this.nav=nav;
        this.sumrow=nav.getJSumRow();
        this.rowcurrent=nav.getCurrentRow();
        this.nav.set(this);
    }
    
    public void InsertTable()
    {
        DefaultTableModel tableModel = new DefaultTableModel(getDatatable(),getColumnTable()) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return column !=0;
            }
        };
        tablemonhoc.setModel(tableModel);
        nav.selectrowtable(0);
    }
    
    
    public void addComboBox(JComboBox cbb)
    {
        for(MonHocDAL mh : ArrMonHoc)
        {
            cbb.addItem(mh);
        }
    }
    
    public MonHocDAL SearchMonHoc(int MaMH)
    {
        for(MonHocDAL mh : ArrMonHoc)
        {
            if(mh.getMa()==MaMH)
                return mh;
        }
        return null;
    }
    
    public int SearchMaMonHoc(String TenMonHoc)
    {
        for(MonHocDAL mh : ArrMonHoc)
        {
            if(mh.getTen().equals(TenMonHoc))
                return mh.getMa();
        }
        return 0;
    }
    
    //Set Control Navigation
    public void setNav(JLabel reset,JLabel delete,JLabel save,JLabel add,JLabel thongbao)
    {
        this.reset=reset;
        this.delete=delete;
        this.save=save;
        this.add=add;
        this.thongbao=thongbao;
        thanhnav();
    }
    //Thanh Navigation
    private void thanhnav()
    {
        reset=nav.create(228, 3,20,22, "Làm Mới Danh Sách");
        delete=nav.create(250, 3,20,22, "Xóa");
        save=nav.create(272, 3,21,22, "Lưu");
        add=nav.create(206, 5,17,19, "Thêm Mới Môn Học");
        eventlabel();
    }
    public void setKhungTrai(JTextField txttenmonhoc,JTextField txtheso)
    {
        this.txttenmonhoc=txttenmonhoc;
        this.txtheso=txtheso;
    }
    public void HienThiChiTiet()
    {
        try
        {
            int row=tablemonhoc.getSelectedRow();
            txttenmonhoc.setText(tablemonhoc.getValueAt(row, 1).toString());
            txtheso.setText(tablemonhoc.getValueAt(row, 2).toString());
        }
        catch(Exception ex)
        {
            
        }
    }
    //Hiển Thị Row Hiện Tại Lên Nav
    public void setCurrentRow()
    {
        try
        {
            rowcurrent.setText(String.valueOf(tablemonhoc.getSelectedRow()+1));
        }
        catch(Exception ex)
        {
            rowcurrent=nav.create(60, 5, 20, 17, "");
            rowcurrent.setText(String.valueOf(tablemonhoc.getSelectedRow()+1));
        }
    }
    //Thếm 1 Row Mới Vào Table
    private void AddNewRowTable()
    {
        DefaultTableModel Model = (DefaultTableModel)tablemonhoc.getModel(); 
        
        int row=tablemonhoc.getRowCount()-1;
        int MaMH=0;
        MaMH=Integer.parseInt(tablemonhoc.getValueAt(row, 0).toString())+1;
        Model.addRow(new Object[]{MaMH,"","1"});
        nav.selectrowtable(tablemonhoc.getRowCount()-1);
        sumrow.setText("Của "+String.valueOf(tablemonhoc.getRowCount()));
        
    }
    private void InsertMonHoc()
    {
        int row=tablemonhoc.getRowCount();
        for(int i=row-soluongthem;i<row;i++)
        {
            try
            {
                frmmain.connect.ExecuteSPAction("InsertMonHoc", getValue(i));
                thongbao.setText("Thêm Thành Công");
                soluongthem--;
            }
            catch(Exception ex)
            {
                thongbao.setText("Có Lỗi");
                nav.selectrowtable(i);
                break;
            }
        }
        if(soluongthem==0)
            ThemMoi=false;
    }
    private void EditMonHoc()
    {
        for(int i=0;i<tablemonhoc.getRowCount();i++)
        {
            try {
                frmmain.connect.ExecuteSPAction("EditMonHoc", getValue2(i));
                thongbao.setText("Sửa Thành Công "+i+" Môn Học");
            } catch (Exception e) {
                thongbao.setText("Có Lỗi");
                nav.selectrowtable(i);
                break;
            }
        }
    }
    private String[] getValue(int row)
    {
        String[] value=new String[2];
        value[0]=tablemonhoc.getValueAt(row, 1).toString();
        value[1]=tablemonhoc.getValueAt(row, 2).toString();
        return value;
    }
    private String[] getValue2(int row)
    {
        String[] value=new String[3];
        value[0]=tablemonhoc.getValueAt(row, 0).toString();
        value[1]=tablemonhoc.getValueAt(row, 1).toString();
        value[2]=tablemonhoc.getValueAt(row, 2).toString();
        return value;
    }
    private void DeleteRowTable()
    {
        int rowselect=tablemonhoc.getSelectedRow();
        if(KiemTraXoaNamHoc(rowselect))
        {
            int result = JOptionPane.showConfirmDialog(null, "Bạn Có Chắc?", "Thông Báo", JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION)
            {
                    try
                    {
                        if(soluongthem>0 && rowselect>=((tablemonhoc.getRowCount()+1)-soluongthem))
                        {
                            soluongthem--;
                            if(soluongthem==0)
                                ThemMoi=false;
                        }
                        else
                        {
                            String[] value={tablemonhoc.getValueAt(rowselect, 0).toString().trim()};
                            frmmain.connect.ExecuteSPAction("DeleteMonHoc", value);
                            RemoveMH(Integer.parseInt(value[0]));
                            frmmain.giaovien.addComboBoxCM();
                            frmmain.diem.AddComBoBoxMonHoc();
                        }
                        ((DefaultTableModel)tablemonhoc.getModel()).removeRow(rowselect);
                        nav.selectrowtable((rowselect==tablemonhoc.getRowCount())?rowselect-1:rowselect);
                        sumrow.setText("Của "+String.valueOf(tablemonhoc.getRowCount()));
                    }
                    catch(Exception ex)
                    {
                        
                    }
           }
        }
    }
    private boolean KiemTraXoaNamHoc(int row)
    {
        int MaNH=Integer.parseInt(tablemonhoc.getValueAt(row, 0).toString().trim());
        if(frmmain.giaovien.KiemTraXoaMonHoc(MaNH) || frmmain.diem.KiemTraXoaMonHoc(MaNH))
        {
            JOptionPane.showMessageDialog(null, "Đã Có Đối Tượng Sử Dụng Môn Học "+tablemonhoc.getValueAt(row, 1).toString()+
                    "\nBạn Không Thê Xóa");
            return false;
        }
        return true;
    }
    private void RemoveMH(int MaMH)
    {
        for(MonHocDAL mh : ArrMonHoc)
        {
            if(mh.getMa()==MaMH)
            {
                ArrMonHoc.remove(mh);
                break;
            }
        }
    }
    private void eventlabel()
    {
        reset.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                   ArrMonHoc.clear();
                   getData();
                   InsertTable();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                reset.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                reset.setBorder(null);
            }
        });
        delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    DeleteRowTable();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                delete.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                delete.setBorder(null);
            }
        });
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if(ThemMoi) 
                    InsertMonHoc();
                else
                    EditMonHoc();
                ArrMonHoc.clear();
                getData();
                frmmain.giaovien.addComboBoxCM();
                frmmain.diem.AddComBoBoxMonHoc();
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                save.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                save.setBorder(null);
            }
        });
        add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ThemMoi=true;
                    soluongthem++;
                    AddNewRowTable();
                    HienThiChiTiet();
                    
                }
                catch(Exception ex)
                {
                    //JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                add.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                add.setBorder(null);
            }
        });
    }
    
    //Tạo Comlumn table
    private Vector getColumnTable()
    {
        Vector Comlumn=new Vector();
        Comlumn.add("Mã Môn Học");
        Comlumn.add("Tên Môn Học");
        Comlumn.add("Hệ Số");
        return Comlumn;
    }
    private Vector getDatatable()
    {
        Vector row=new Vector();
        Vector dr;
        
        
        for(MonHocDAL mh : ArrMonHoc)
        {
            dr=new Vector();
            dr.add(mh.getMa());
            dr.add(mh.getTen());
            dr.add(mh.getHeSo());
            row.add(dr);
        }
        
        return row;
    }
    public void LuuVaoDanhSach()
    {
        try {
            int MaMH=Integer.parseInt(tablemonhoc.getValueAt(tablemonhoc.getSelectedRow(), 0).toString());
            if(SearchMonHoc(MaMH)!=null)
            {
                String[] value={String.valueOf(MaMH),txttenmonhoc.getText().trim(),txtheso.getText()};
                //frmmain.connect.ExecuteSPAction("EditMonHoc", value);
                thongbao.setText("Sửa Thành Công");
                Sua(MaMH,value[1],Integer.parseInt(value[2]));
            }
            else
            {
                String[] value={txttenmonhoc.getText(),txtheso.getText()};
                //frmmain.connect.ExecuteSPAction("InsertMonHoc", value);
                thongbao.setText("Thêm Thành Công");
                ArrMonHoc.add(new MonHocDAL(MaMH, txttenmonhoc.getText(),Integer.parseInt(txtheso.getText())));
            }
            CapNhatLenTable(txttenmonhoc.getText(),txtheso.getText());
        } catch (Exception e) {
        }
    }
    private void Sua(int MaHK,String TenHK,int HeSo)
    {
        for(MonHocDAL mh : ArrMonHoc)
        {
            if(mh.getMa()==MaHK)
            {
                mh.setTen(TenHK);
                mh.setHeSo(HeSo);
                break;
            }
        }
    }
    private void CapNhatLenTable(String TenHK,String Heso)
    {
        int row=tablemonhoc.getSelectedRow();
        tablemonhoc.setValueAt(TenHK, row, 1);
        tablemonhoc.setValueAt(Heso, row, 3);
    }
    public ArrayList<MonHocDAL> getArrayMH()
    {
        return this.ArrMonHoc;
    }
}
