

package BusinessLogicLayers;

import PresentationLayers.FrmLoading;
import PresentationLayers.FrmMain;
import java.awt.Component;
import javax.swing.JDialog;


public class ShowDiaLogBLL{
    private FrmMain frmmain=null;
    private JDialog dialog=null;
    public ShowDiaLogBLL(){
    }
    public ShowDiaLogBLL(FrmMain frmmain){
        this.frmmain=frmmain;
    }
    public void Show(String Title,Component cpt,int width,int height){
        dialog=new JDialog(frmmain, Title, true);
        dialog.add(cpt);
        dialog.setSize(width,height);
        dialog.setLocationRelativeTo(frmmain);
        dialog.setResizable(false);
        dialog.setVisible(true);
    }
    public void Show(int width,int height){
        dialog=new JDialog();
        dialog.add(new FrmLoading());
        dialog.setSize(width,height);
        dialog.setResizable(false);
        dialog.setUndecorated(true);
        dialog.setLocationRelativeTo(null);
        dialog.setVisible(true);
    }
    public void Hidden(){
        dialog.dispose();
    }
}
