

package BusinessLogicLayers;
import DataAccessLayers.GiaoVienDAL;
import DataAccessLayers.LopDAL;
import DataAccessLayers.NamHocDAL;
import PresentationLayers.FrmMain;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;


public class LopBLL {
    
    private JLabel reset;
    private JLabel delete;
    private JLabel save;
    private JLabel add;
    private JLabel sumrow;
    private JLabel rowcurrent;
    private JLabel thongbao;
    
    private JTable tablelop;
    
    private ArrayList<LopDAL> ArrLop;
    
    private FrmMain frmmain;
    
    private NavigationBLL nav;
    
    private JComboBox cbbnamhoc;
    
    private boolean themmoi;
    private int soluongthem;
    
    public LopBLL(FrmMain frmmain) {
        this.frmmain=frmmain;
        ArrLop=new ArrayList<LopDAL>();
        
        themmoi=false;
        soluongthem=0;
        
        getData();
    }
    private void getData()
    {
        try {
             ResultSet rss = frmmain.connect.ExecuteSPSelect("SelectAllLop");
                if (rss != null) {
                    while (rss.next()) {
                        LopDAL l=new LopDAL();
                        
                        l.setMaLop(rss.getInt(1));
                        l.setTenLop(rss.getString(2));
                        l.setMaNamHoc(frmmain.namhoc.SearchTenNamHoc(rss.getInt(3)));
                        l.setMaGiaoVien(frmmain.giaovien.SearchTenGiaoVien(rss.getInt(4)));
                        l.setSiSo(rss.getInt(5));
                        
                        ArrLop.add(l);
                    }
                rss.close();
            }
        } catch (Exception ex) {
            
        }
    }
    public void HienThiDanhSach()
    {
        insertTable();
    }
    public void setTable(JTable tablelop,JPanel pnl)
    {
        nav=new NavigationBLL(tablelop, pnl, reset, reset, reset, reset, rowcurrent, sumrow);
        this.tablelop=tablelop;
        this.sumrow=nav.getJSumRow();
        this.rowcurrent=nav.getCurrentRow();
        //this.nav.set(this);
    }
    public void insertTable()
    {
        InsertDataTable();
        
        EditColumnTable();
    }
   private void InsertDataTable()
    {
        DefaultTableModel tableModel = new DefaultTableModel(getDatatable(),getColumnTable()) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return column !=0;
            }
        };
        tablelop.setModel(tableModel);
 
        nav.selectrowtable(0);
        nav.getSumRow();
    }
    private void EditColumnTable()
    {
        JComboBox cbb=new JComboBox();
        frmmain.namhoc.AddComboBox(cbb);
        TableColumn Column = tablelop.getColumnModel().getColumn(2);
        Column.setCellEditor(new DefaultCellEditor(cbb));
        
        cbb=new JComboBox();
        frmmain.giaovien.AddComboBox(cbb);
        Column=tablelop.getColumnModel().getColumn(3);
        Column.setCellEditor(new DefaultCellEditor(cbb));
    }
    public LopDAL SearchLop(int MaLop)
    {
        for(LopDAL l : ArrLop)
        {
            if(l.getMaLop()==MaLop)
                return l;
        }
        return null;
    }
    //Tạo Comlumn table
    private Vector getColumnTable()
    {
        Vector Comlumn=new Vector();
        Comlumn.add("Mã Lớp Học");
        Comlumn.add("Tên Lớp Học");
        Comlumn.add("Năm Học");
        Comlumn.add("Giáo Viên Chủ Nhiệm");
        Comlumn.add("Sỉ Số");
        return Comlumn;
    }
    //Lấy data của table
    private Vector getDatatable()
    {
        try
        {
            Vector row=new Vector();
            Vector dr;
            int manamhoc=((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa();

            for(LopDAL l : ArrLop)
            {
                if(l.getMaNamHoc().getMa()==manamhoc)
                {
                    dr=new Vector();
                    dr.add(l.getMaLop());
                    dr.add(l.getTenLop());
                    dr.add(l.getMaNamHoc());
                    dr.add(l.getMaGiaoVien());
                    dr.add(l.getSiSo());
                    row.add(dr);
                }
            }

            return row;
        }
        catch(Exception ex)
        {
            return null;
        }
    }
    public void AddComboxBox(JComboBox cbb)
    {
        cbbnamhoc=cbb;
        AdddComBoxBoxNamHoc();
    }
    public void AdddComBoxBoxNamHoc()
    {
        cbbnamhoc.removeAllItems();
        frmmain.namhoc.AddComboBox(cbbnamhoc);
    }
    public void AddComboBoxLop(JComboBox cbb,int MaNH)
    {
        for(LopDAL l : ArrLop)
        {
            if(l.getMaNamHoc().getMa()==MaNH) 
                cbb.addItem(l);
        }
    }
    public void AddComboBoxLop(JComboBox cbb,int MaNH,String TenLop)
    {
        for(LopDAL l : ArrLop)
        {
            if(l.getMaNamHoc().getMa()==MaNH && !l.getTenLop().equals(TenLop)) 
                cbb.addItem(l);
        }
    }
    public void getCurrentRow()
    {
        try
        {
            rowcurrent.setText(String.valueOf(tablelop.getSelectedRow()+1));
        }
        catch(Exception ex)
        {
        }
    }
    public void setNav(JLabel thongbao)
    {
        this.thongbao=thongbao;
        thanhnav();
    }
    private void thanhnav()
    {
        reset=nav.create(228, 3,20,22, "Làm Mới Danh Sách");
        delete=nav.create(250, 3,20,22, "Xóa");
        save=nav.create(272, 3,21,22, "Lưu");
        add=nav.create(206, 5,17,19, "Thêm Mới Lớp");
        eventlabel();
    }
    private void insertnewlop()
    {
        int row=tablelop.getRowCount();
        for(int i=row-soluongthem;i<row;i++)
        {
            try {
                if(KiemTraInsertLop(i))
                {
                        frmmain.connect.ExecuteSPAction("InsertLop", getValue(i));
                        thongbao.setText("Thêm Thành Công "+soluongthem+" Lớp Vào Danh Sách");
                }
                else
                {
                    thongbao.setText("Giáo Viên "+tablelop.getValueAt(i, 3).toString()+" Đã Là GVCN Của Lớp Khác");
                    break;
                }
            } catch (Exception e) {
                thongbao.setText("Có Lỗi "+e.getMessage());
                nav.selectrowtable(i);
                break;
            }
        }
    }
    public boolean KiemTraInsertLop(int row)
    {
        int MaNH=((NamHocDAL)(tablelop.getValueAt(row, 2))).getMa();
        int MaGV=((GiaoVienDAL)(tablelop.getValueAt(row, 3))).getMaGV();
        for(LopDAL l : ArrLop)
        {
            if(l.getMaGiaoVien().getMaGV()==MaGV && l.getMaNamHoc().getMa()==MaNH) 
                return false;
        }
        return true;
        
    }
    private void EditGiaoVien()
    {
        int row=tablelop.getRowCount();
        for(int i=0;i<row;i++)
        {
            try
            {
                frmmain.connect.ExecuteSPAction("Editlop", getValue(i));
                thongbao.setText("Sửa Thành Công "+i+" Lớp");
            }
            catch(Exception ex)
            {
                thongbao.setText("Có Lỗi "+ ex.getMessage());
            }
        }
    }
            
    private String[] getValue(int row)
    {
        String[] value=new String[3];
        value[0]=tablelop.getValueAt(row, 1).toString().trim();
        value[1]=String.valueOf(((NamHocDAL)(tablelop.getValueAt(row, 2))).getMa());
        value[2]=String.valueOf(((GiaoVienDAL)(tablelop.getValueAt(row, 3))).getMaGV());
        return value;
    }
    private void DeleteRowTable()
    {
        int rowselect=tablelop.getSelectedRow();
        if(KiemTraXoaLop(rowselect))
        {
            int result = JOptionPane.showConfirmDialog(null, "Bạn Có Chắc?", "Thông Báo", JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION)
            {
                    try
                    {
                        if(soluongthem>0 && rowselect>=((tablelop.getRowCount()+1)-soluongthem))
                        {
                            soluongthem--;
                            if(soluongthem==0)
                                themmoi=false;
                        }
                        else
                        {
                            String[] value={tablelop.getValueAt(rowselect, 0).toString().trim()};
                            frmmain.connect.ExecuteSPAction("DeleteLop", value);
                            RemoveLop(Integer.parseInt(value[0]));
                            frmmain.hocsinhtronglop.updatecbblop();
                            frmmain.phanlop.UpdateCbbLop();
                            frmmain.chuyenlop.updatecbblop();
                        }
                        ((DefaultTableModel)tablelop.getModel()).removeRow(rowselect);
                        nav.selectrowtable((rowselect==tablelop.getRowCount())?rowselect-1:rowselect);
                        sumrow.setText("Của "+String.valueOf(tablelop.getRowCount()));
                    }
                    catch(Exception ex)
                    {
                    }
           }
        }
    }
    private boolean KiemTraXoaLop(int row)
    {
        int MaLop=Integer.parseInt(tablelop.getValueAt(row, 0).toString().trim());
        if(frmmain.hocsinhtronglop.KiemTraXoaLop(MaLop))
        {
            JOptionPane.showMessageDialog(frmmain, "Lớp "+tablelop.getValueAt(row, 1).toString()+
                    "\nĐã Có Học Sinh.\nKhông Thể Xóa");
            return false;
        }
        return true;
    }
    private void RemoveLop(int MaLop)
    {
        for(LopDAL l : ArrLop)
        {
            if(l.getMaLop()==MaLop)
            {
                ArrLop.remove(l);
                break;
            }
        }
    }
    //tạo sự kiện cho control navigation
    private void eventlabel()
    {
        reset.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ArrLop.clear();
                    getData();
                    insertTable();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                reset.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                reset.setBorder(null);
            }
        });
        delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    DeleteRowTable();
                }
                catch(Exception ex)
                {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                delete.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                delete.setBorder(null);
            }
        });
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if(themmoi)
                    insertnewlop();
                else
                    EditGiaoVien();
                ArrLop.clear();
                getData();
                frmmain.hocsinhtronglop.updatecbblop();
                frmmain.phanlop.UpdateCbbLop();
                frmmain.chuyenlop.updatecbblop();
                frmmain.phancong.AddComBoBoxLop();
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                save.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                save.setBorder(null);
            }
        });
        add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    themmoi=true;
                    soluongthem++;
                    AddNewRowTable();
                }
                catch(Exception ex)
                {
                    //JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                add.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                add.setBorder(null);
            }
        });
    }
    private void AddNewRowTable()
    {
        DefaultTableModel Model = (DefaultTableModel)tablelop.getModel(); 
        int MaLop=1;
        int size=ArrLop.size();
        if(size>0)
        {
            MaLop=ArrLop.get(size-1).getMaLop();
        }
        Model.addRow(new Object[]{MaLop+1,"","","",0});
        nav.selectrowtable(tablelop.getRowCount()-1);
        sumrow.setText("Của "+String.valueOf(tablelop.getRowCount()));
    }
    public boolean KiemTraXoaGiaoVien(int MaGV)
    {
        for(LopDAL l : ArrLop)
        {
            if(l.getMaGiaoVien().getMaGV()==MaGV)
                return true;
        }
        return false;
    }
    public boolean KiemTraXoaNamHoc(int manh)
    {
        for(LopDAL l : ArrLop)
        {
            if(l.getMaNamHoc().getMa()==manh)
                return true;
        }
        return false;
    }
    public ArrayList<LopDAL> getArrayLop()
    {
        return this.ArrLop;
    }
}
