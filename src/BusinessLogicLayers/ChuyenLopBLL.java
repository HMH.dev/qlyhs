

package BusinessLogicLayers;

import DataAccessLayers.HocKiDAL;
import DataAccessLayers.LopDAL;
import DataAccessLayers.NamHocDAL;
import PresentationLayers.FrmMain;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


public class ChuyenLopBLL {
    //dùng để liên kết với  các đối tượng khác
    private final FrmMain frmmain;
    
    //hiển thị năm học hiện tại của học sinh
    private JComboBox cbbnamhoccu;
    //hiển thị năm học mà học sinh sẽ chuyển đến
    private JComboBox cbbnamhocmoi;
    //hiển thị lớp củ của học sinh
    private JComboBox cbblopcu;
    //hiển thị lớp mà học sinh chuyển đến
    private JComboBox cbblopmoi;
    //hiện thị học kì
    private JComboBox cbbhocki;
    
    //hiển thị dữ liệu lớp cũ
    private JTable tablelopcu;
    //hiển thị dữ liệu lớp mới
    private JTable tablelopmoi;
    
    //hàm khởi tạo, truyền vào 1 frmmain để liên kết với form chuyển lớp
    public ChuyenLopBLL(FrmMain frmmain)
    {
        //gán frmmain của class này với frmmain
        this.frmmain=frmmain;
    }
    //Đổ dữ liệu vào ComboBox Năm Học Cũ
    public void AddComboxBoxNamHocCu(JComboBox cbbnamhoccu)
    {
        frmmain.namhoc.AddComboBox(cbbnamhoccu);
        this.cbbnamhoccu=cbbnamhoccu;
    }
    public void AddComBoxBoxNamHoc()
    {
        cbbnamhoccu.removeAllItems();
        cbbnamhocmoi.removeAllItems();
        frmmain.namhoc.AddComboBox(cbbnamhoccu);
        frmmain.namhoc.AddComboBox(cbbnamhocmoi);
    }
    //Đổ dữ liệu vào ComboBox Lớp Học Cũ
    public void AddComboxBoxLopHocCu(JComboBox cbblophoccu)
    {
        //xóa tất cả dữ liệu đã có trong lớp cũ
        cbblophoccu.removeAllItems();
        frmmain.lop.AddComboBoxLop(cbblophoccu, ((NamHocDAL)cbbnamhoccu.getSelectedItem()).getMa());
        this.cbblopcu=cbblophoccu;
    }
    public void AddComboxBoxNamHocMoi(JComboBox cbbnamhocmoi)
    {
        frmmain.namhoc.AddComboBox(cbbnamhocmoi);
        this.cbbnamhocmoi=cbbnamhocmoi;
    }
    public void AddComboxBoxLopHocMoi(JComboBox cbblophocmoi)
    {
        cbblophocmoi.removeAllItems();
        frmmain.lop.AddComboBoxLop(cbblophocmoi, ((NamHocDAL)cbbnamhocmoi.getSelectedItem()).getMa(),cbblopcu.getSelectedItem().toString());
        this.cbblopmoi=cbblophocmoi;
    }
    
    public void AddComboBoxHocKi(JComboBox cbbhocki)
    {
        this.cbbhocki=cbbhocki;
        AddComBoBoxHocKi();
    }
    public void AddComBoBoxHocKi()
    {
        cbbhocki.removeAllItems();
        frmmain.hocki.AddComboBox(cbbhocki);
    }
    //set control của table. Dữ liệu này được truyền từ PresentationLayers.FrmChuyenLop sang
    public void setTable(JTable tablelopcu,JTable tablelopmoi)
    {
        this.tablelopcu=tablelopcu;
        this.tablelopmoi=tablelopmoi;
    }
    //đổ dữ liệu vào table lớp cũ
    public void InsertTableLopCu()
    {
        //lấy mã lớp để lấy học sinh trong lớp đó
        int MaLop=0;
        try {
            MaLop=((LopDAL)(cbblopcu.getSelectedItem())).getMaLop();
        } catch (Exception e) {
            MaLop=1;
        }
        //đổ dữ liệu vào table
        DefaultTableModel tableModel = new DefaultTableModel(frmmain.hocsinhtronglop.getDatatable(MaLop),getColumnTable()) {
            //không cho edit row
            @Override
            public boolean isCellEditable(int row, int column) {
               return false;
            }
        };
        tablelopcu.setModel(tableModel);
    }
    //tương tự table lớp cũ
    public void InsertTableLopMoi()
    {
        int MaLop=((LopDAL)(cbblopmoi.getSelectedItem())).getMaLop();
        DefaultTableModel tableModel = new DefaultTableModel(frmmain.hocsinhtronglop.getDatatable(MaLop),getColumnTable()) {
            @Override
            public boolean isCellEditable(int row, int column) {
               return false;
            }
        };
        tablelopmoi.setModel(tableModel);
    }
    //Tạo Comlumn table
    private Vector getColumnTable()
    {
        Vector Comlumn=new Vector();
        Comlumn.add("Mã Học Sinh");
        Comlumn.add("Tên Học Sinh");
        return Comlumn;
    }
    //thực hiện event chuyển học sinh qua lớp mới
    public void ChuyenHocSinh()
    {
        try
        {
            //lấy row đang chọn
            int row=tablelopcu.getSelectedRow();
            //lấy mã học sinh đang chọn, để chuyển qua lớp khác. Vì mã học sinh là khóa chính của học sinh
            String MaHocSinh=tablelopcu.getValueAt(row, 0).toString().trim();
            //lấy mã lớp cần chuyển đến
            int MaLop=((LopDAL)(cbblopmoi.getSelectedItem())).getMaLop();
            //lấy học kì cần chuyển đến
            int MaHK=((HocKiDAL)(cbbhocki.getSelectedItem())).getMa();
            //lấy năm học mới
            int MaNH=((NamHocDAL)(cbbnamhocmoi.getSelectedItem())).getMa();
            //gán vào value để insert vào csdl sql server
            String[] value={MaHocSinh,String.valueOf(MaLop),String.valueOf(MaHK),String.valueOf(MaNH)};
            //nếu năm học mới bằng năm học cũ, tức là người dùng muốn chuyển lớp
            if(((NamHocDAL)(cbbnamhoccu.getSelectedItem())).getMa()==MaNH) 
                frmmain.connect.ExecuteSPAction("chuyenlop", value);
            else
                frmmain.connect.ExecuteSPAction("PhanLop", value);
            //xóa row vừa chuyển
            DefaultTableModel model=(DefaultTableModel)tablelopmoi.getModel();
            model.addRow(new Object[]{MaHocSinh,tablelopcu.getValueAt(row, 1).toString()});
            model=(DefaultTableModel)tablelopcu.getModel();
            model.removeRow(row);
            
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Hãy Chọn Một Học Sinh Cần Chuyển");
        }
    }
    //tương tự hàm chuyển học sinh trên. Chỉ khác là chuyển từ table phải qua table trái
    public void ChuyenHS()
    {
        try
        {
            int row=tablelopmoi.getSelectedRow();
            String MaHocSinh=tablelopmoi.getValueAt(row, 0).toString().trim();
            int MaLop=((LopDAL)(cbblopcu.getSelectedItem())).getMaLop();
            int MaHK=((HocKiDAL)(cbbhocki.getSelectedItem())).getMa();
            int MaNH=((NamHocDAL)(cbbnamhoccu.getSelectedItem())).getMa();
            String[] value={MaHocSinh,String.valueOf(MaLop),String.valueOf(MaHK),String.valueOf(MaNH)};
            if(((NamHocDAL)(cbbnamhocmoi.getSelectedItem())).getMa()==MaNH) 
                frmmain.connect.ExecuteSPAction("chuyenlop", value);
            else
                frmmain.connect.ExecuteSPAction("PhanLop", value);
            DefaultTableModel model=(DefaultTableModel)tablelopcu.getModel();
            model.addRow(new Object[]{MaHocSinh,tablelopmoi.getValueAt(row, 1).toString()});
            model=(DefaultTableModel)tablelopmoi.getModel();
            model.removeRow(row);
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Hãy Chọn Một Học Sinh Cần Xóa Khỏi Lớp");
        }
    }
    //nếu bên class LopBLL có cập nhật lớp mới thì cập nhật lại dữ liệu lên cbb lớp
    public void updatecbblop()
    {
        cbblopcu.removeAllItems();
        frmmain.lop.AddComboBoxLop(cbblopcu, ((NamHocDAL)cbbnamhoccu.getSelectedItem()).getMa());
        cbblopmoi.removeAllItems();
        frmmain.lop.AddComboBoxLop(cbblopmoi, ((NamHocDAL)cbbnamhocmoi.getSelectedItem()).getMa(),cbblopcu.getSelectedItem().toString());
    }
}
