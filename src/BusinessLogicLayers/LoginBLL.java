

package BusinessLogicLayers;

import DataAccessLayers.LoginDAL;
import PresentationLayers.FrmLogin;
import PresentationLayers.FrmMain;
import java.sql.ResultSet;
import javax.swing.JLabel;
import javax.swing.JOptionPane;


public class LoginBLL extends LoginDAL{
    private ShowDiaLogBLL dialog;
    private FrmMain frmmain;
    private JLabel lbnddn;
    public LoginBLL(){
    }
    public void ShowLogin(FrmMain frmmain,JLabel lbnddn){
        this.frmmain=frmmain;
        this.lbnddn=lbnddn;
        dialog=new ShowDiaLogBLL(frmmain);
        dialog.Show("Login", new FrmLogin(this), 708, 288);
    }
    public void HiddenLogin(){
        dialog.Hidden();
    }
    public void HiddenLogin1(){
        dialog.Hidden();
    }
    public void Login(String Username,String Password){
        try {
            String value[]={Username,Password};
            ResultSet rss=frmmain.connect.ExecuteSPSelect("DangNhap", value);
            int KQ=-1;
            rss.next();
            KQ=rss.getInt(1);
            rss.close();
            ThongBao(KQ,Username,Password);
        } catch (Exception e) {
        }
    }
    private void ThongBao(int KQ,String Username,String Password){
        switch(KQ){
            case 0:
                HiddenLogin();
                LayThongTin(Username, Password);
                lbnddn.setText("<html><b>"+getName()+"</b> ("+getTenLoai()+")</html>");
                frmmain.iconlogin(false);
                break;
            case 1:
                JOptionPane.showMessageDialog(frmmain, "Mật khẩu sai");
                break;
            case 2:
                break;
            case 3:
                JOptionPane.showMessageDialog(frmmain, "Tài khoản và mật khẩu sai");
                break;
            case 4:
                JOptionPane.showMessageDialog(frmmain, "Tài khoản của bạn đã bị khóa,"
                        + " vui lòng liên hệ admin để biết thêm thông tin.");
                break;
        }
    }
    private void LayThongTin(String Username,String Password){
        try {
            String[] value={Username,Password};
            ResultSet rss=frmmain.connect.ExecuteSPSelect("LayThongTin",value);
            rss.next();
            setName(rss.getString(1));
            setTenLoai(rss.getString(2));
            setThem(rss.getBoolean(3));
            setSua(rss.getBoolean(4));
            setXoa(rss.getBoolean(5));
            setThemThanhVienMoi(rss.getBoolean(6));
            setXoaThanhVien(rss.getBoolean(7));
            setKhoaThanhVien(rss.getBoolean(8));
            rss.close();
        } catch (Exception e) {
        }
    }
}
