

package BusinessLogicLayers;

import DataAccessLayers.DanTocDAL;
import PresentationLayers.FrmMain;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class DanTocBLL {
    /*  dùng để lưu dữ danh sách dân tộc, để có thể sử dụng khi cần.
        dữ liệu được lấy trong sql server.
        dùng ArrayList để giúp cho chương trình không phải liên tục lấy dữ liệu trong csdl.
        mà chỉ cần lấy 1 lần có thể sử dụng nhiều lần*/
    private ArrayList<DanTocDAL> ArrDanToc;
    
    //icon reset trên thanh navigation
    private JLabel reset;
    //icon delete trên thanh navigation
    private JLabel delete;
    //icon save trên thanh navigation
    private JLabel save;
    //icon add trên thanh navigation
    private JLabel add;
    //icon sumrow trên thanh navigation
    private JLabel sumrow;
    //icon rowcurrent trên thanh navigation
    private JLabel rowcurrent;
    //icon thongbao trên thanh navigation
    private JLabel thongbao;
    
    //table dân tộc(nơi hiển thị danh sách dân tộc lên cho người dùng xem)
    private JTable tabledantoc;
    
    //thanh navigation dùng để thêm sửa xóa,.. dữ liệu trên table
    private NavigationBLL nav;
    
    //dùng để liên kết với  các đối tượng khác
    private final FrmMain frmmain;
            
    //Hiện thi mã dân tộc lên khung bên trái
    private JTextField txtmadt;
     //Hiện thi tên dân tộc lên khung bên trái
    private JTextField txttendt;
    
    //để kiểm tra xem người dùng muốn thêm mới dân tộc hay là sửa dân tộc
    //nếu ThemMoi=false thì tức là sửa, còn ngược lại là Thêm mới
    private boolean ThemMoi=false;
    //Số lượng dân tộc muốn thêm
    private int SoLuongThem=0;
    
    //hàm khởi tạo
    public DanTocBLL(FrmMain frmmain)
    {
        //khởi tạo mới 1 danh sách liên kết
        ArrDanToc=new ArrayList<DanTocDAL>();
        
        this.frmmain=frmmain;
        
        /*  lấy dữ liệu trong csdl
            mục đích của việc đặt hàm getData() trong hàm khởi tạo là để
            khi chương trình vừa chạy sẽ lấy toàn bộ dữ liệu trong csdl lên.
            để có thể sử dụng khi cần.
            - Lợi ích của cách này là giúp chương trình không phải liên tục lấy
            dữ liệu từ csdl mà chỉ cần lấy 1 lần sử dụng nhiều lần, như vậy sẽ giúp 
            chương trình chạy nhanh hơn trong 'khi sử dụng'.
            - Bất lợi: chương trình sẽ 'khởi động' chậm hơn.
        */
        getData();
    }
    //Lấy dữ liệu từ csdl sql server sau đó add vào ArrayList Để sử dụng khi cần
    public void getData()
    {
        try {
            //rss dùng để lấy dữ liệu trả về khi thực thi thành công câu lệnh truy vần sql
            //SelectAllDanToc là Store procedure trong csdl sql server
             ResultSet rss = frmmain.connect.ExecuteSPSelect("SelectAllDanToc");
             //kiểm tra xem dữ liệu có bị lỗi hay không
            if (rss != null) {
                //dùng vòng lập để lấy toàn bộ dữ liêu trong csdl
                while (rss.next()) {
                    //tạo 1 biến dân tộc mới để add vào danh sách liên kết
                    DanTocDAL dantoc=new DanTocDAL();
                    dantoc.setMa(rss.getInt(1));
                    dantoc.setTen(rss.getString(2));
                    //add vào danh sách liên kết
                    ArrDanToc.add(dantoc);
                }
                //dóng rss để giải phóng bộ nhờ
                rss.close();
            }
        } catch (Exception ex) {
            
        }
    }
    //Tìm Kiếm Dân Tộc Theo Mã DT
    public DanTocDAL SearchDanToc(int MaDT)
    {
        //duyện toàn bộ danh sách để tìm. nếu tìm thấy thì trả về dân tộc tìm thấy
        // còn không tìm thấy thì trả về null
        for(DanTocDAL dt : ArrDanToc)
        {
            if(MaDT==dt.getMa())
                return dt;
        }
        return null;
    }
    //Tìm Kiếm Dân Tộc Theo Tên Dân Tộc
    public int TimKiemMaDanToc(String TenDT)
    {
        //tương tự hàm phải trên
        for(DanTocDAL dt: ArrDanToc)
        {
            if(dt.getTen().equals(TenDT)) 
                return dt.getMa();
        }
        return 1;
    }
    
    //đổ danh sách dân tộc vào combobox
    public void AddComboBox(JComboBox cbb)
    {
        for(DanTocDAL dt : ArrDanToc)
        {
            cbb.addItem(dt);
        }
    }
    //đổ dữ liệu vào table
    public void insertTable()
    {
        //Đổ dữ liệu lên jtable
        tabledantoc.setModel(new DefaultTableModel(getDatatable(),getColumnTable()));
        //Select vào row 0
        nav.selectrowtable(0);
    }
    //Hiển Thị Row Hiện Tại Lên Nav
    public void setCurrentRow()
    {
        try
        {
            //lấy row hiện tại đang select vào
            rowcurrent.setText(String.valueOf(tabledantoc.getSelectedRow()+1));
        }
        catch(Exception ex)
        {
        }
    }
    
    //set control của table
    public void setTable(JTable tabledantoc,NavigationBLL nav)
    {
        this.tabledantoc=tabledantoc;
        this.sumrow=nav.getJSumRow();
        this.rowcurrent=nav.getCurrentRow();
        this.nav=nav;
        this.nav.set(this);
    }
    //set control của thanh navigation
    public void setNav(JLabel reset,JLabel delete,JLabel save,JLabel add,JLabel thongbao)
    {
        this.reset=reset;
        this.delete=delete;
        this.save=save;
        this.add=add;
        this.thongbao=thongbao;
        thanhnav();
    }
    //set control khung trái
    public void setKhungTrai(JTextField txtmadt,JTextField txttendt)
    {
        this.txtmadt=txtmadt;
        this.txttendt=txttendt;
    }
    //hiển thị chi tiết lên khung trái
    public void HienThiChiTiet()
    {
        try
        {
            int row=tabledantoc.getSelectedRow();
            txtmadt.setText(tabledantoc.getValueAt(row, 0).toString());
            txttendt.setText(tabledantoc.getValueAt(row, 1).toString());
        }catch(Exception ex)
        {
            
        }
    }
    
    /*
        - Hàm tạo thanh navigation.
    */
    private void thanhnav()
    {
        reset=nav.create(228, 3,20,22, "Làm Mới Danh Sách");
        delete=nav.create(250, 3,20,22, "Xóa");
        save=nav.create(272, 3,21,22, "Lưu");
        add=nav.create(206, 5,17,19, "Thêm Mới Học Sinh");
        eventlabel();
    }
    //thêm 1 row mới vào table dân tộc
    private void AddNewRowTable()
    {
        DefaultTableModel Model = (DefaultTableModel)tabledantoc.getModel(); 
        
        //lấy tổng số lượng row trên table. Mục đích là thêm row mới vào cuối cũng của table
        int row=tabledantoc.getRowCount()-1;
        int MaMH=0;
        MaMH=Integer.parseInt(tabledantoc.getValueAt(row, 0).toString())+1;
        Model.addRow(new Object[]{MaMH,""});
        nav.selectrowtable(tabledantoc.getRowCount()-1);
        sumrow.setText("Của "+String.valueOf(tabledantoc.getRowCount()));
    }
    //thêm mới 1 dân tộc vào csdl
    private void InsertDanToc()
    {
        //duyệt toàn bộ table để lấy dữ liệu trên table insert vào csdl
        int row=tabledantoc.getRowCount();
        for(int i=row-SoLuongThem;i<row;i++)
        {
            try
            {
                //thực hiện câu lên để insert vào table
                //InsertDanToc là Store procedure trong csdl sql server
                frmmain.connect.ExecuteSPAction("InsertDanToc", getValue(i));
                thongbao.setText("Thêm Thành Công");
                SoLuongThem--;
            }
            catch(Exception ex)
            {
                thongbao.setText("Có Lỗi");
                nav.selectrowtable(i);
                break;
            }
        }
        if(SoLuongThem==0)
            ThemMoi=false;
    }
    //sữa dân tộc
    private void EditDanToc()
    {
        for(int i=0;i<tabledantoc.getRowCount();i++)
        {
            try {
                //tương tự hàm trên
                frmmain.connect.ExecuteSPAction("EditDanToc", getValue2(i));
                thongbao.setText("Sửa Thành Công "+i+" Dân Tộc");
            } catch (Exception e) {
                thongbao.setText("Có Lỗi");
                nav.selectrowtable(i);
                break;
            }
        }
    }
    /*lấy value trên table dùng để insert. Vì mã dân tộc 
        cho mặc định tự động tăng nên không cần lấy mã dt trên table*/
    private String[] getValue(int row)
    {
        String[] value=new String[1];
        value[0]=tabledantoc.getValueAt(row, 1).toString();
        return value;
    }
        /*lấy value trên table dùng để edit.*/
    private String[] getValue2(int row)
    {
        String[] value=new String[2];
        value[0]=tabledantoc.getValueAt(row, 0).toString();
        value[1]=tabledantoc.getValueAt(row, 1).toString();
        return value;
    }
    //xóa 1 row trên table. dùng để xóa dân tộc
    private void DeleteRowTable()
    {
        //lấy row đang select vào (row cần xóa)
        int rowselect=tabledantoc.getSelectedRow();
        /*kiểm tra xem có xóa được không.
            Dân tộc chỉ xóa được khi học sinh vào giáo viên không sử dụng dân tộc cần xóa
        */
        if(KiemTraXoaDanToc(rowselect))
        {
            //xuất hộp thoại yes/no để chống click nhầm
            int result = JOptionPane.showConfirmDialog(null, "Bạn Có Chắc?", "Thông Báo", JOptionPane.YES_NO_OPTION);
            //nếu như thật sự muốn xóa(không phải click nhâm) thì tiến hành xóa trong csld
            if (result == JOptionPane.YES_OPTION)
            {
                    try
                    {
                        //nếu như dân tộc cần xóa không có trong csdl thì khỏi xóa
                        if(SoLuongThem>0 && rowselect>=((tabledantoc.getRowCount()+1)-SoLuongThem))
                        {
                            SoLuongThem--;
                            if(SoLuongThem==0)
                                ThemMoi=false;
                        }
                        else
                        {
                            String[] value={tabledantoc.getValueAt(rowselect, 0).toString().trim()};
                            frmmain.connect.ExecuteSPAction("DeleteDanToc", value);
                            //xóa dân tộc vừa xóa ra khỏi danh sách liên kết
                            RemoveDT(Integer.parseInt(value[0]));
                            frmmain.hocsinh.addComboBoxDT();
                            frmmain.giaovien.addComboBoxDT();
                        }
                        ((DefaultTableModel)tabledantoc.getModel()).removeRow(rowselect);
                        nav.selectrowtable((rowselect==tabledantoc.getRowCount())?rowselect-1:rowselect);
                        //cập nhật lại số lượng row trên table
                        sumrow.setText("Của "+String.valueOf(tabledantoc.getRowCount()));
                    }
                    catch(Exception ex)
                    {
                        
                    }
           }
        }
    }
    //kiểm tra dân tộc có xóa được không(đã giải thích ở trên)
    private boolean KiemTraXoaDanToc(int row)
    {
        //lấy dân tộc cần xóa
        int MaDT=Integer.parseInt(tabledantoc.getValueAt(row, 0).toString().trim());
        //kiểm tra xem học sinh hoặc giáo viên có sử dụng dân tộc cần xóa không
        if(frmmain.hocsinh.SearchDanToc(MaDT) || frmmain.giaovien.SearchDanToc(MaDT))
        {
            //nếu đã có học sinh hoặc giáo viên sử dụng dân tộc cần xóa thì thông báo ra cho người dùng biết
            JOptionPane.showMessageDialog(null, "Đã Có Đối Tượng Sử Dụng Dân Tộc "+tabledantoc.getValueAt(row, 1).toString()+
                    "\nBạn Không Thê Xóa");
            return false;
        }
        return true;
    }
    //xóa dân tộc ra khỏi danh sách liên kết
    private void RemoveDT(int MaDT)
    {
        for(DanTocDAL dt : ArrDanToc)
        {
            if(dt.getMa()==MaDT)
            {
                ArrDanToc.remove(dt);
                break;
            }
        }
    }
    //tạo sự kiện cho control navigation
    private void eventlabel()
    {
        reset.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    //ResetTable();
                }
                catch(Exception ex)
                {
                    
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                reset.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                reset.setBorder(null);
            }
        });
        delete.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    DeleteRowTable();
                }
                catch(Exception ex)
                {
                    
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                delete.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                delete.setBorder(null);
            }
        });
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if(ThemMoi) 
                    InsertDanToc();
                else
                    EditDanToc();
                ArrDanToc.clear();
                getData();
                frmmain.hocsinh.addComboBoxDT();
                frmmain.giaovien.addComboBoxDT();
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                save.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                save.setBorder(null);
            }
        });
        add.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                try
                {
                    ThemMoi=true;
                    SoLuongThem++;
                    AddNewRowTable();
                }
                catch(Exception ex)
                {
                    //JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                add.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 255), 1, false));
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                add.setBorder(null);
            }
        });
    }
    //Tạo Comlumn table
    private Vector getColumnTable()
    {
        Vector Comlumn=new Vector();
        Comlumn.add("Mã Dân Tộc");
        Comlumn.add("Tên Dân Tộc");
        return Comlumn;
    }
    //Lấy data của table
    private Vector getDatatable()
    {
        Vector row=new Vector();
        Vector dr;
        
        
        for(DanTocDAL dt : ArrDanToc)
        {
            dr=new Vector();
            dr.add(dt.getMa());
            dr.add(dt.getTen());
            row.add(dr);
        }
        
        return row;
    }
}
