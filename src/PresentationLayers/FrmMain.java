

package PresentationLayers;

import BusinessLogicLayers.*;
import DataAccessLayers.clsDatabase;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class FrmMain extends javax.swing.JFrame {
    
    //dùng để lưu lại danh sách các tab dang mở
    private ArrayList<DataTab> btn=new ArrayList();
    //icon close tab khi chưa rê chuột vào
    private final ImageIcon icon=new ImageIcon(getClass().getResource("Images/close.png"));
    //icon close tab khi rê chuột vào
    private final ImageIcon iconhover=new ImageIcon(getClass().getResource("Images/close_hover.png"));
    //form loading dữ liệu
    private ShowDiaLogBLL dialog;
    //kết nới đến csdl slq data
    public clsDatabase connect;
    //dùng để kiểm tra xem quá trình load dữ liệu có thành công hay không
    //true là thành công, false là thất bại
    private boolean lock=true;
    
    //khai báo các đối tượng để các đối tượng có thể liên kết được với nhau
    public DanTocBLL DanToc;
    public TonGiaoBLL TonGiao;
    public HocSinhBLL hocsinh;
    public LopBLL lop;
    public MonHocBLL monhoc;
    public GiaoVienBLL giaovien;
    public NamHocBLL namhoc;
    public HocKiBLL hocki;
    public HocSinh_LopBLL hocsinhtronglop;
    public PhanLopBLL phanlop;
    public ChuyenLopBLL chuyenlop;
    public LoaiDiemBLL loaidiem;
    public DiemBLL diem;
    public KetQuaHocTapBLL kqht;
    public PhanCongBLL phancong;
    private LoginBLL login=new LoginBLL();
    
    
    // <editor-fold defaultstate="collapsed" desc="Data Tab">   
    public class DataTab {
        private JButton btn;
        private int id;
        public DataTab()
        {
            this.btn=new JButton();
            id=0;
        }
        public DataTab(JButton btn,int id)
        {
            this.btn=btn;
            this.id=id;
        }

        public JButton getBtn()
        {
            return this.btn;
        }

        public int getId()
        {
            return this.id;
        }
    }
    //</editor-fold>
    //Đóng 1 tab
    private void CloseTab(ActionEvent e)
    {
        //duyệt tất cả các tab đang mở
        for(int i=0;i<btn.size();i++)
        {
            //tìm tab vừa click vào. Nếu tìm thấy thì xóa tab đó khỏi tabmain
              if(e.getSource()==btn.get(i).getBtn()) 
              {
                    tabmain.remove(i);
                    btn.remove(i);
                    break;
              }
       }
    }
     //Đóng 1 tab
    public void CloseTab(int index)
    {
        tabmain.remove(index);
        btn.remove(index);
    }
    
    //sự kiện hover button close
    private void HoverButton(java.awt.event.MouseEvent evt,ImageIcon iin)
    {
        for(int i=0;i<btn.size();i++)
        {
              if(evt.getSource()==btn.get(i).getBtn()) 
              {
                    btn.get(i).getBtn().setIcon(iin);
                    break;
              }
       }
    }
    //kiểm tra tab đã được mở chưa
    private boolean KiemTraTab(int id)
    {
        for(int i=0;i<btn.size();i++)
        {
              if(btn.get(i).getId()==id) 
              {
                  //nếu đã được mở thì select vào tab đó
                    tabmain.setSelectedIndex(i);
                    return false;
              }
       }
        return true;
    }
    //tạo 1 tab mới
    public void createTab(String title,Component cpt,int id)
    {
            //add 1 tab vào jTabbedPane1
            tabmain.addTab("", cpt);
            FlowLayout f = new FlowLayout (FlowLayout.CENTER, 5, 0);
            JPanel pnlTab = new JPanel(f);
            pnlTab.setOpaque (false);
            //tiêu đề tab
            JLabel lb=new JLabel(title);

            //button close
            JButton closeTab = new JButton();
            closeTab.setIcon(icon);
            closeTab.setToolTipText("Close Tab");
            closeTab.setOpaque (false); 
            closeTab.setBorder (null);
            closeTab.setContentAreaFilled (false);
            closeTab.setFocusPainted (false);
            closeTab.setFocusable (false);

            pnlTab.add (lb);
            pnlTab.add (closeTab);
            btn.add(new DataTab(closeTab,id));
            tabmain.setTabComponentAt (tabmain.getTabCount () - 1, pnlTab);
            tabmain.setSelectedIndex(tabmain.getTabCount()-1);

            //Sư kiện close tab
            closeTab.addActionListener(new ActionListener(){
               @Override
               public void actionPerformed (ActionEvent e) {
                    CloseTab(e);
               }
            });
            

            //Sự kiện hover
            closeTab.addMouseListener(new java.awt.event.MouseAdapter() {
                    @Override
                    public void mouseEntered(java.awt.event.MouseEvent evt) {
                        HoverButton(evt, iconhover);
                    }
                    @Override
                    public void mouseExited(java.awt.event.MouseEvent evt) {
                        HoverButton(evt,icon);
                    }
            });
        
    }
    //khởi tạo dữ liệu
    private void init()
    {
            DanToc=new DanTocBLL(this);
            TonGiao=new TonGiaoBLL(this);
            hocsinh=new HocSinhBLL(this);
            monhoc=new MonHocBLL(this);
            giaovien=new GiaoVienBLL(this);
            namhoc=new NamHocBLL(this);
            lop=new LopBLL(this);
            hocki=new HocKiBLL(this);
            hocsinhtronglop=new HocSinh_LopBLL(this);
            phanlop=new PhanLopBLL(this);
            chuyenlop=new ChuyenLopBLL(this);
            loaidiem=new LoaiDiemBLL(this);
            diem=new DiemBLL(this);
            kqht=new KetQuaHocTapBLL(this);
            phancong=new PhanCongBLL(this);
    }
    //kết nối đến csdl
    public void connectdb()
    {
       //bắt lỗi
       try {
           
            connect=new clsDatabase(this);
            if(lock) 
                init();
        } catch (Exception e) {
        }
    }
    //dùng để kết nối đến csdl khi thất bại
    public void connectdb(clsDatabase connect)
    {
        this.connect=connect;
        //khởi tạo lại
        init();
        //làm icon lable ra giữa, nằm trong event form
        centerIconLabel(true);
        //hiển thị form đăng nhập
        //ShowLogin();
    }
    //hàm khởi tạo
    public FrmMain(){
        //hiện thị form loading dữ liệu
        ShowLoading();
        initComponents();
        //Tiêu đề của phần mềm
        setTitle("Quản Lý Học Sinh Trung Học Phổ Thông");
        //mở rộng phần mềm
        setExtendedState(getExtendedState() | JFrame.MAXIMIZED_BOTH);
        //tạo size bằng kích thước màn hình
        setSize(getMaximumSize());
        //tạo 1 tab mới
        createTab("Trang Chủ", new FrmHome(), 0);
        //ẩn icon
        IconKetNoi(false);
        //kết nối đến database
        connectdb();
        //xóa form loading
        HiddenLoading();
        //hiện thị thời gian
        Time();
        //nếu ko có lỗi thì show form login ra
        if(lock) {
            centerIconLabel(true);
            //ShowLogin();
        }
    }
    //hiện thị form loading
    private void ShowLoading(){
        dialog=new ShowDiaLogBLL();
        dialog.Show(336, 150);
    }
    //ẩn form loading
    private void HiddenLoading(){
        dialog.Hidden();
        this.setVisible(true);
    }
    //hiện thị form kết nối lại csdl
    //hàm này được sử dụng trong clsDatabase bên package DataAccessLayers
    public void KetNoiLaiSQL(){
        //kết nối thất bại
        lock=false;
        centerIconLabel(false);
        //ẩn form loading
        HiddenLoading();
        //show form kết nối csdl
        ShowDiaLogBLL connectdb=new ShowDiaLogBLL(this);
        connectdb.Show("Connect Database", new FrmConnect(this,connectdb), 460,330);
        if(!lbketnoi.isVisible()) {
            iconlogin(false);
            IconKetNoi(true);
        }
    }
    //show form login
    private void ShowLogin(){
        login.ShowLogin(this,lbnguoidungdangnhap);
    }
    //hiển thị thời gian
    private void Time(){
        //tạo 1 luồng mới để luôn luôn chạy thời gian
        Thread th=new Thread(new TimeBLL(lbtime));
        th.start();
    }
    public void iconlogin(boolean b){
        lbdangnhap.setVisible(b);
    }
    public void IconKetNoi(boolean b){
        lbketnoi.setVisible(b);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        pnlBackground = new javax.swing.JPanel(){
            public void paintComponent(Graphics g){

                ImageIcon icon=new ImageIcon(getClass().getResource("/PresentationLayers/Images/bg.png"));

                g.drawImage(icon.getImage(),0 , 0, this.getWidth(), this.getHeight(), null);
                setOpaque(false);
                super.paintComponent(g);
            }
        };
        jScrollPane1 = new javax.swing.JScrollPane();
        pnbg = new javax.swing.JPanel(){
            public void paintComponent(Graphics g){

                ImageIcon icon=new ImageIcon(getClass().getResource("/PresentationLayers/Images/nav.png"));

                g.drawImage(icon.getImage(),0 , 0, this.getWidth(), this.getHeight(), null);
                setOpaque(false);
                super.paintComponent(g);
            }
        };
        tabmain = new javax.swing.JTabbedPane();
        pnbottom = new javax.swing.JPanel(){
            public void paintComponent(Graphics g){

                ImageIcon icon=new ImageIcon(getClass().getResource("/PresentationLayers/Images/bgbottom.png"));

                g.drawImage(icon.getImage(),0 , 0, this.getWidth(), 30, null);
                setOpaque(false);
                super.paintComponent(g);
            }
        };
        lbtime = new javax.swing.JLabel();
        lbnguoidung = new javax.swing.JLabel();
        lbnguoidungdangnhap = new javax.swing.JLabel();
        lbdangnhap = new javax.swing.JLabel();
        lbketnoi = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        pnlheader = new javax.swing.JPanel(){
            public void paintComponent(Graphics g){
                ImageIcon icon=new ImageIcon(getClass().getResource("/PresentationLayers/Images/bg5.png"));

                g.drawImage(icon.getImage(),0 , 0, this.getWidth(), 140, null);
                setOpaque(false);
                super.paintComponent(g);
            }
        };
        tabHeader = new javax.swing.JTabbedPane();
        pnlHeader = new javax.swing.JPanel(){
            public void paintComponent(Graphics g){
                ImageIcon icon=new ImageIcon(getClass().getResource("/PresentationLayers/Images/bg4.png"));

                g.drawImage(icon.getImage(),0 , 0, this.getWidth(), 139, null);
                setOpaque(false);
                super.paintComponent(g);
            }
        };
        lbhocsinh1 = new javax.swing.JLabel();
        lbphanlop1 = new javax.swing.JLabel();
        lbchuyenlop1 = new javax.swing.JLabel();
        lbdiem1 = new javax.swing.JLabel();
        lbgiaovien1 = new javax.swing.JLabel();
        lbhocsinh2 = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        lblophoc = new javax.swing.JLabel();
        lbketqua = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel8 = new javax.swing.JPanel();
        lbnamhoc = new javax.swing.JLabel();
        lbhocki = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        lbmonhoc = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lbphancong = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel(){
            public void paintComponent(Graphics g){
                ImageIcon icon=new ImageIcon(getClass().getResource("/PresentationLayers/Images/bg4.png"));

                g.drawImage(icon.getImage(),0 , 0, this.getWidth(), 139, null);
                setOpaque(false);
                super.paintComponent(g);
            }
        };
        lblamviecvoiexcel1 = new javax.swing.JLabel();
        lblamviecvoiexcel5 = new javax.swing.JLabel();
        lblamviecvoiexcel4 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jPanel10 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        jPanel12 = new javax.swing.JPanel(){
            public void paintComponent(Graphics g){
                ImageIcon icon=new ImageIcon(getClass().getResource("/PresentationLayers/Images/bg4.png"));

                g.drawImage(icon.getImage(),0 , 0, this.getWidth(), 139, null);
                setOpaque(false);
                super.paintComponent(g);
            }
        };
        lbtrogiup = new javax.swing.JLabel();
        lbthongtinpm = new javax.swing.JLabel();
        jPanel13 = new javax.swing.JPanel(){
            public void paintComponent(Graphics g){
                ImageIcon icon=new ImageIcon(getClass().getResource("/PresentationLayers/Images/bg4.png"));

                g.drawImage(icon.getImage(),0 , 0, this.getWidth(), 139, null);
                setOpaque(false);
                super.paintComponent(g);
            }
        };
        lbphuchoi = new javax.swing.JLabel();
        lbsaoluu = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pnlBackground.setAlignmentY(0.0F);

        jScrollPane1.setBorder(null);
        jScrollPane1.setAutoscrolls(true);

        pnbg.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));

        tabmain.setAutoscrolls(true);

        javax.swing.GroupLayout pnbgLayout = new javax.swing.GroupLayout(pnbg);
        pnbg.setLayout(pnbgLayout);
        pnbgLayout.setHorizontalGroup(
            pnbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabmain, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1232, Short.MAX_VALUE)
        );
        pnbgLayout.setVerticalGroup(
            pnbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabmain, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 505, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(pnbg);

        lbnguoidung.setText("<html><i>Người dùng đăng nhập</i>:</html>");

        lbnguoidungdangnhap.setText("<html><b>Khách</b></html>");

        lbdangnhap.setForeground(new java.awt.Color(0, 0, 153));
        lbdangnhap.setText("<html><u>Đăng Nhập</u></html>");
        lbdangnhap.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbdangnhap.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbdangnhapMouseClicked(evt);
            }
        });

        lbketnoi.setForeground(new java.awt.Color(0, 0, 153));
        lbketnoi.setText("<html><u>Kết Nối Database</u></html>");
        lbketnoi.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbketnoi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbketnoiMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout pnbottomLayout = new javax.swing.GroupLayout(pnbottom);
        pnbottom.setLayout(pnbottomLayout);
        pnbottomLayout.setHorizontalGroup(
            pnbottomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnbottomLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lbnguoidung, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbnguoidungdangnhap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(42, 42, 42)
                .addComponent(lbdangnhap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbketnoi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbtime, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        pnbottomLayout.setVerticalGroup(
            pnbottomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnbottomLayout.createSequentialGroup()
                .addGroup(pnbottomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pnbottomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lbnguoidung, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbnguoidungdangnhap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbdangnhap, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lbketnoi, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lbtime, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        lbtime.getAccessibleContext().setAccessibleDescription("");

        jScrollPane2.setBorder(null);

        pnlheader.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0, 0, 0)));
        pnlheader.setPreferredSize(new java.awt.Dimension(1230, 130));

        pnlHeader.setLayout(null);

        lbhocsinh1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbhocsinh1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/pupils.png"))); // NOI18N
        lbhocsinh1.setText("<html><center>Danh Sách<br>Học Sinh</center></html>");
        lbhocsinh1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbhocsinh1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbhocsinh1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbhocsinh1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbhocsinh1MouseExited(evt);
            }
        });
        pnlHeader.add(lbhocsinh1);
        lbhocsinh1.setBounds(10, 7, 70, 80);

        lbphanlop1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/phanlop.png"))); // NOI18N
        lbphanlop1.setText("<html><center>Phân<br>Lớp</center></html>");
        lbphanlop1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbphanlop1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbphanlop1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbphanlop1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbphanlop1MouseExited(evt);
            }
        });
        pnlHeader.add(lbphanlop1);
        lbphanlop1.setBounds(180, 7, 70, 80);

        lbchuyenlop1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/chuyenlop.png"))); // NOI18N
        lbchuyenlop1.setText("<html><center>Chuyển<br>Lớp</center></html>");
        lbchuyenlop1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbchuyenlop1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbchuyenlop1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbchuyenlop1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbchuyenlop1MouseExited(evt);
            }
        });
        pnlHeader.add(lbchuyenlop1);
        lbchuyenlop1.setBounds(90, 7, 80, 80);

        lbdiem1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/diem.png"))); // NOI18N
        lbdiem1.setText("<html><center>Điểm<br><br></center></html>");
        lbdiem1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbdiem1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbdiem1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbdiem1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbdiem1MouseExited(evt);
            }
        });
        pnlHeader.add(lbdiem1);
        lbdiem1.setBounds(500, 7, 70, 80);

        lbgiaovien1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/giaovien.png"))); // NOI18N
        lbgiaovien1.setText("<html><center>Danh Sách<br>Giáo Viên</center></html>");
        lbgiaovien1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbgiaovien1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbgiaovien1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbgiaovien1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbgiaovien1MouseExited(evt);
            }
        });
        pnlHeader.add(lbgiaovien1);
        lbgiaovien1.setBounds(685, 7, 70, 80);

        lbhocsinh2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lbhocsinh2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/dshocsinh.png"))); // NOI18N
        lbhocsinh2.setText("<html><center>Học Sinh<br>Trong Lớp</center></html>");
        lbhocsinh2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbhocsinh2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbhocsinh2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbhocsinh2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbhocsinh2MouseExited(evt);
            }
        });
        pnlHeader.add(lbhocsinh2);
        lbhocsinh2.setBounds(370, 7, 95, 80);

        jPanel5.setBackground(new java.awt.Color(204, 204, 204));
        jPanel5.setPreferredSize(new java.awt.Dimension(1, 0));

        jPanel7.setBackground(new java.awt.Color(204, 204, 204));
        jPanel7.setPreferredSize(new java.awt.Dimension(1, 0));

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 115, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 0, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 137, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pnlHeader.add(jPanel5);
        jPanel5.setBounds(270, 0, 1, 115);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(102, 102, 102));
        jLabel1.setText("Học Sinh");
        pnlHeader.add(jLabel1);
        jLabel1.setBounds(110, 95, 50, 13);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(102, 102, 102));
        jLabel2.setText("Lớp Học");
        pnlHeader.add(jLabel2);
        jLabel2.setBounds(360, 95, 50, 13);

        jPanel6.setBackground(new java.awt.Color(204, 204, 204));
        jPanel6.setPreferredSize(new java.awt.Dimension(1, 120));

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 120, Short.MAX_VALUE)
        );

        pnlHeader.add(jPanel6);
        jPanel6.setBounds(480, 0, 1, 120);

        lblophoc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/lophoc.png"))); // NOI18N
        lblophoc.setText("<html><center>Danh Sách<br>Lớp Học</center></html>");
        lblophoc.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblophoc.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblophocMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblophocMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblophocMouseExited(evt);
            }
        });
        pnlHeader.add(lblophoc);
        lblophoc.setBounds(290, 7, 70, 80);

        lbketqua.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/ketqua.png"))); // NOI18N
        lbketqua.setText("<html><center>Tổng<br>Kết</center></html>");
        lbketqua.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbketqua.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbketquaMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbketquaMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbketquaMouseExited(evt);
            }
        });
        pnlHeader.add(lbketqua);
        lbketqua.setBounds(580, 7, 80, 80);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(102, 102, 102));
        jLabel3.setText("Kết Quả Học Tập");
        pnlHeader.add(jLabel3);
        jLabel3.setBounds(540, 95, 110, 13);

        jPanel4.setBackground(new java.awt.Color(204, 204, 204));
        jPanel4.setPreferredSize(new java.awt.Dimension(1, 120));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 120, Short.MAX_VALUE)
        );

        pnlHeader.add(jPanel4);
        jPanel4.setBounds(675, 0, 1, 120);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(102, 102, 102));
        jLabel4.setText("Giáo Viên");
        pnlHeader.add(jLabel4);
        jLabel4.setBounds(730, 95, 60, 13);

        jPanel8.setBackground(new java.awt.Color(204, 204, 204));

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1, Short.MAX_VALUE)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 120, Short.MAX_VALUE)
        );

        pnlHeader.add(jPanel8);
        jPanel8.setBounds(830, 0, 1, 120);

        lbnamhoc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/namhoc.png"))); // NOI18N
        lbnamhoc.setText("<html><center>Năm<br>Học</center></html>");
        lbnamhoc.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbnamhoc.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbnamhocMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbnamhocMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbnamhocMouseExited(evt);
            }
        });
        pnlHeader.add(lbnamhoc);
        lbnamhoc.setBounds(930, 7, 60, 80);

        lbhocki.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/hocky.png"))); // NOI18N
        lbhocki.setText("<html><center>Học<br>Kỳ</center></html>");
        lbhocki.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbhocki.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbhockiMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbhockiMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbhockiMouseExited(evt);
            }
        });
        pnlHeader.add(lbhocki);
        lbhocki.setBounds(850, 7, 60, 80);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(102, 102, 102));
        jLabel5.setText("Năm Học Và Học Kì");
        pnlHeader.add(jLabel5);
        jLabel5.setBounds(870, 95, 110, 13);

        jPanel9.setBackground(new java.awt.Color(204, 204, 204));

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1, Short.MAX_VALUE)
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 120, Short.MAX_VALUE)
        );

        pnlHeader.add(jPanel9);
        jPanel9.setBounds(1005, 0, 1, 120);

        lbmonhoc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/monhoc.png"))); // NOI18N
        lbmonhoc.setText("<html><center>Danh Sách<br>Môn Học</center></html>");
        lbmonhoc.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbmonhoc.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbmonhocMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbmonhocMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbmonhocMouseExited(evt);
            }
        });
        pnlHeader.add(lbmonhoc);
        lbmonhoc.setBounds(1020, 10, 80, 80);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(102, 102, 102));
        jLabel6.setText("Môn Học");
        pnlHeader.add(jLabel6);
        jLabel6.setBounds(1040, 95, 50, 13);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 120, Short.MAX_VALUE)
        );

        pnlHeader.add(jPanel1);
        jPanel1.setBounds(1115, 0, 1, 120);

        lbphancong.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/phancong.png"))); // NOI18N
        lbphancong.setText("<html><center>Phân<br>Công</center></html>");
        lbphancong.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbphancong.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbphancongMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbphancongMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbphancongMouseExited(evt);
            }
        });
        pnlHeader.add(lbphancong);
        lbphancong.setBounds(760, 7, 60, 80);

        tabHeader.addTab("Quản Lý", pnlHeader);

        jPanel2.setLayout(null);

        lblamviecvoiexcel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/TaoPhieuMoi.png"))); // NOI18N
        lblamviecvoiexcel1.setText("<html><center>Xuất<br>Kết Quả</center></html>");
        lblamviecvoiexcel1.setToolTipText("");
        lblamviecvoiexcel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblamviecvoiexcel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblamviecvoiexcel1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblamviecvoiexcel1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblamviecvoiexcel1MouseExited(evt);
            }
        });
        jPanel2.add(lblamviecvoiexcel1);
        lblamviecvoiexcel1.setBounds(230, 7, 70, 80);

        lblamviecvoiexcel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/dsgiaovien.png"))); // NOI18N
        lblamviecvoiexcel5.setText("<html><center>Xuất<br>Danh Sách</center></html>");
        lblamviecvoiexcel5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblamviecvoiexcel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblamviecvoiexcel5MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblamviecvoiexcel5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblamviecvoiexcel5MouseExited(evt);
            }
        });
        jPanel2.add(lblamviecvoiexcel5);
        lblamviecvoiexcel5.setBounds(125, 7, 70, 80);

        lblamviecvoiexcel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/dshocsinh.png"))); // NOI18N
        lblamviecvoiexcel4.setText("<html><center>Xuất<br>Danh Sách</center></html>");
        lblamviecvoiexcel4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblamviecvoiexcel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblamviecvoiexcel4MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lblamviecvoiexcel4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lblamviecvoiexcel4MouseExited(evt);
            }
        });
        jPanel2.add(lblamviecvoiexcel4);
        lblamviecvoiexcel4.setBounds(20, 7, 70, 80);

        jPanel3.setBackground(new java.awt.Color(204, 204, 204));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 110, Short.MAX_VALUE)
        );

        jPanel2.add(jPanel3);
        jPanel3.setBounds(110, 0, 1, 110);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(102, 102, 102));
        jLabel7.setText("Học Sinh");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(30, 95, 50, 13);

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(102, 102, 102));
        jLabel8.setText("Giáo Viên");
        jPanel2.add(jLabel8);
        jLabel8.setBounds(135, 96, 60, 13);

        jPanel10.setBackground(new java.awt.Color(204, 204, 204));

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1, Short.MAX_VALUE)
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 120, Short.MAX_VALUE)
        );

        jPanel2.add(jPanel10);
        jPanel10.setBounds(210, 0, 1, 120);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(102, 102, 102));
        jLabel9.setText("Kết Quả Học Tập");
        jPanel2.add(jLabel9);
        jLabel9.setBounds(230, 95, 80, 13);

        jPanel11.setBackground(new java.awt.Color(204, 204, 204));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1, Short.MAX_VALUE)
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 120, Short.MAX_VALUE)
        );

        jPanel2.add(jPanel11);
        jPanel11.setBounds(315, 0, 1, 120);

        tabHeader.addTab("Xuất Dữ Liệu", jPanel2);

        jPanel12.setLayout(null);

        lbtrogiup.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/huongdan.png"))); // NOI18N
        lbtrogiup.setText("<html><center>Trợ<br>Giúp</center></html>");
        lbtrogiup.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbtrogiup.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbtrogiupMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbtrogiupMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbtrogiupMouseExited(evt);
            }
        });
        jPanel12.add(lbtrogiup);
        lbtrogiup.setBounds(30, 7, 70, 80);

        lbthongtinpm.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/thongtinphanmem.png"))); // NOI18N
        lbthongtinpm.setText("<html><center>Thông Tin<br>Phần Mềm</center></html>");
        lbthongtinpm.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbthongtinpm.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbthongtinpmMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbthongtinpmMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbthongtinpmMouseExited(evt);
            }
        });
        jPanel12.add(lbthongtinpm);
        lbthongtinpm.setBounds(120, 7, 80, 80);

        tabHeader.addTab("Trợ Giúp", jPanel12);

        jPanel13.setLayout(null);

        lbphuchoi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/phuchoidulieu.png"))); // NOI18N
        lbphuchoi.setText("<html><center>Phục<br>Hồi</center></html>");
        lbphuchoi.setToolTipText("");
        lbphuchoi.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbphuchoi.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbphuchoiMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbphuchoiMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbphuchoiMouseExited(evt);
            }
        });
        jPanel13.add(lbphuchoi);
        lbphuchoi.setBounds(110, 7, 70, 80);

        lbsaoluu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/PresentationLayers/Images/saoluudulieu.png"))); // NOI18N
        lbsaoluu.setText("<html><center>Sao<br>Lưu</center></html>");
        lbsaoluu.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbsaoluu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbsaoluuMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                lbsaoluuMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                lbsaoluuMouseExited(evt);
            }
        });
        jPanel13.add(lbsaoluu);
        lbsaoluu.setBounds(20, 7, 70, 80);

        tabHeader.addTab("Sao Lưu, Phục Hồi Dữ Liệu", jPanel13);

        javax.swing.GroupLayout pnlheaderLayout = new javax.swing.GroupLayout(pnlheader);
        pnlheader.setLayout(pnlheaderLayout);
        pnlheaderLayout.setHorizontalGroup(
            pnlheaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabHeader, javax.swing.GroupLayout.DEFAULT_SIZE, 1232, Short.MAX_VALUE)
        );
        pnlheaderLayout.setVerticalGroup(
            pnlheaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabHeader, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
        );

        jScrollPane2.setViewportView(pnlheader);

        javax.swing.GroupLayout pnlBackgroundLayout = new javax.swing.GroupLayout(pnlBackground);
        pnlBackground.setLayout(pnlBackgroundLayout);
        pnlBackgroundLayout.setHorizontalGroup(
            pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(pnbottom, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jScrollPane2)
        );
        pnlBackgroundLayout.setVerticalGroup(
            pnlBackgroundLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlBackgroundLayout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnbottom, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBackground, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnlBackground, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // <editor-fold defaultstate="collapsed" desc="Event Form">   
    private void lblophocMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblophocMouseClicked
        if(KiemTraTab(2)) 
        createTab("Lớp Học",new FrmLop(lop),2);
    }//GEN-LAST:event_lblophocMouseClicked

    private void lblophocMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblophocMouseEntered
        lblophoc.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lblophocMouseEntered

    private void lblophocMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblophocMouseExited
        lblophoc.setBorder(null);
    }//GEN-LAST:event_lblophocMouseExited

    private void lbmonhocMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbmonhocMouseEntered
       lbmonhoc.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lbmonhocMouseEntered

    private void lbmonhocMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbmonhocMouseExited
        lbmonhoc.setBorder(null);
        
    }//GEN-LAST:event_lbmonhocMouseExited

    private void lbnamhocMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbnamhocMouseEntered
        lbnamhoc.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lbnamhocMouseEntered

    private void lbnamhocMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbnamhocMouseExited
        lbnamhoc.setBorder(null);
    }//GEN-LAST:event_lbnamhocMouseExited

    private void lbhockiMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbhockiMouseEntered
        lbhocki.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lbhockiMouseEntered

    private void lbhockiMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbhockiMouseExited
        lbhocki.setBorder(null);
    }//GEN-LAST:event_lbhockiMouseExited

    private void lbketquaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbketquaMouseEntered
        lbketqua.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lbketquaMouseEntered

    private void lbketquaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbketquaMouseExited
        lbketqua.setBorder(null);
    }//GEN-LAST:event_lbketquaMouseExited

    private void lbmonhocMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbmonhocMouseClicked
        createtabmohoc();
    }//GEN-LAST:event_lbmonhocMouseClicked

    private void lbnamhocMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbnamhocMouseClicked
        if(KiemTraTab(7)) 
        createTab("Năm Học",new FrmNamHoc(namhoc),7);
    }//GEN-LAST:event_lbnamhocMouseClicked

    private void lbhockiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbhockiMouseClicked
        if(KiemTraTab(8)) 
        createTab("Học Kỳ",new FrmHocKi(hocki),8);
    }//GEN-LAST:event_lbhockiMouseClicked

    private void lbketquaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbketquaMouseClicked
        if(KiemTraTab(9)) 
        createTab("Tổng Kết",new FrmKetQua(kqht),9);
    }//GEN-LAST:event_lbketquaMouseClicked

    private void lbhocsinh1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbhocsinh1MouseClicked
        if(KiemTraTab(1)) 
        createTab("Học Sinh",new FrmHocSinh(hocsinh),1);
    }//GEN-LAST:event_lbhocsinh1MouseClicked

    private void lbhocsinh1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbhocsinh1MouseEntered
        lbhocsinh1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lbhocsinh1MouseEntered

    private void lbhocsinh1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbhocsinh1MouseExited
        lbhocsinh1.setBorder(null);
    }//GEN-LAST:event_lbhocsinh1MouseExited

    private void lbphanlop1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbphanlop1MouseClicked
        if(KiemTraTab(3)) 
        createTab("Phân Lớp",new FrmPhanLop(phanlop),3);
    }//GEN-LAST:event_lbphanlop1MouseClicked

    private void lbphanlop1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbphanlop1MouseEntered
        lbphanlop1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lbphanlop1MouseEntered

    private void lbphanlop1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbphanlop1MouseExited
        lbphanlop1.setBorder(null);
    }//GEN-LAST:event_lbphanlop1MouseExited

    private void lbchuyenlop1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbchuyenlop1MouseClicked
        if(KiemTraTab(10)) 
        createTab("Chuyển Lớp",new FrmChuyenLop(chuyenlop),10);
    }//GEN-LAST:event_lbchuyenlop1MouseClicked

    private void lbchuyenlop1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbchuyenlop1MouseEntered
        lbchuyenlop1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lbchuyenlop1MouseEntered

    private void lbchuyenlop1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbchuyenlop1MouseExited
        lbchuyenlop1.setBorder(null);
    }//GEN-LAST:event_lbchuyenlop1MouseExited

    private void lbdiem1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbdiem1MouseClicked
        if(KiemTraTab(5)) 
        createTab("Điểm",new FrmDiem(diem),5);
    }//GEN-LAST:event_lbdiem1MouseClicked

    private void lbdiem1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbdiem1MouseEntered
        lbdiem1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lbdiem1MouseEntered

    private void lbdiem1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbdiem1MouseExited
        lbdiem1.setBorder(null);
    }//GEN-LAST:event_lbdiem1MouseExited

    private void lbgiaovien1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbgiaovien1MouseClicked
        if(KiemTraTab(6)) 
        createTab("Giáo Viên",new FrmGiaoVien(giaovien),6);
    }//GEN-LAST:event_lbgiaovien1MouseClicked

    private void lbgiaovien1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbgiaovien1MouseEntered
        lbgiaovien1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lbgiaovien1MouseEntered

    private void lbgiaovien1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbgiaovien1MouseExited
        lbgiaovien1.setBorder(null);
    }//GEN-LAST:event_lbgiaovien1MouseExited

    private void lblamviecvoiexcel1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblamviecvoiexcel1MouseEntered
        lblamviecvoiexcel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lblamviecvoiexcel1MouseEntered

    private void lblamviecvoiexcel1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblamviecvoiexcel1MouseExited
        lblamviecvoiexcel1.setBorder(null);
    }//GEN-LAST:event_lblamviecvoiexcel1MouseExited

    private void lblamviecvoiexcel4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblamviecvoiexcel4MouseEntered
        lblamviecvoiexcel4.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lblamviecvoiexcel4MouseEntered

    private void lblamviecvoiexcel4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblamviecvoiexcel4MouseExited
        lblamviecvoiexcel4.setBorder(null);
    }//GEN-LAST:event_lblamviecvoiexcel4MouseExited

    private void lblamviecvoiexcel5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblamviecvoiexcel5MouseEntered
        lblamviecvoiexcel5.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lblamviecvoiexcel5MouseEntered

    private void lblamviecvoiexcel5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblamviecvoiexcel5MouseExited
        lblamviecvoiexcel5.setBorder(null);
    }//GEN-LAST:event_lblamviecvoiexcel5MouseExited

    private void lblamviecvoiexcel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblamviecvoiexcel4MouseClicked
       if(KiemTraTab(14)) 
        createTab("Xuất Danh Sách Học Sinh",new FrmXuatDSHS(this),14);
    }//GEN-LAST:event_lblamviecvoiexcel4MouseClicked

    private void lblamviecvoiexcel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblamviecvoiexcel1MouseClicked
        if(KiemTraTab(18)) 
        createTab("Xuất Kết Quả Học Tập",new FrmXuatKQ(this),18);
    }//GEN-LAST:event_lblamviecvoiexcel1MouseClicked

    private void lblamviecvoiexcel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblamviecvoiexcel5MouseClicked
        if(KiemTraTab(19)) 
        createTab("Xuất Danh Sách Giáo Viên",new FrmXuatDSGV(this),19);
    }//GEN-LAST:event_lblamviecvoiexcel5MouseClicked

    private void lbtrogiupMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbtrogiupMouseEntered
        lbtrogiup.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lbtrogiupMouseEntered

    private void lbtrogiupMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbtrogiupMouseExited
        lbtrogiup.setBorder(null);
    }//GEN-LAST:event_lbtrogiupMouseExited

    private void lbtrogiupMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbtrogiupMouseClicked
        try {
            String path="Help.chm";
            int index=getClass().getResource("/DataAccessLayers").toString().indexOf("/build/classes");
            if(index!=-1) 
                path=getClass().getResource("/DataAccessLayers").toString().substring(0, index).replace("file:/", "")+"/HuongDan/Help.chm";
            System.out.println(path);
            Runtime runtime = Runtime.getRuntime();
            Process process =runtime.exec("cmd /c start "+path);
            
        } catch (Exception e) {
        }
    }//GEN-LAST:event_lbtrogiupMouseClicked

    private void lbhocsinh2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbhocsinh2MouseClicked
        if(KiemTraTab(17)) 
        createTab("Học Sinh Trong Lớp",new FrmHSTrongLop(hocsinhtronglop),17);
    }//GEN-LAST:event_lbhocsinh2MouseClicked

    private void lbhocsinh2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbhocsinh2MouseEntered
        lbhocsinh2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lbhocsinh2MouseEntered

    private void lbhocsinh2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbhocsinh2MouseExited
        lbhocsinh2.setBorder(null);
    }//GEN-LAST:event_lbhocsinh2MouseExited

    private void lbthongtinpmMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbthongtinpmMouseEntered
        lbthongtinpm.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lbthongtinpmMouseEntered

    private void lbthongtinpmMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbthongtinpmMouseExited
        lbthongtinpm.setBorder(null);
    }//GEN-LAST:event_lbthongtinpmMouseExited

    private void lbthongtinpmMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbthongtinpmMouseClicked
        new ShowDiaLogBLL(this).Show("Thông Tin Phần Mềm", new FrmThongTinPhanMem(), 410, 300);
        
    }//GEN-LAST:event_lbthongtinpmMouseClicked

    private void lbphancongMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbphancongMouseEntered
        lbphancong.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lbphancongMouseEntered

    private void lbphancongMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbphancongMouseExited
        lbphancong.setBorder(null);
    }//GEN-LAST:event_lbphancongMouseExited

    private void lbphancongMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbphancongMouseClicked
        if(KiemTraTab(22)) 
        createTab("Phân Công",new FrmPhanCong(phancong),22);
    }//GEN-LAST:event_lbphancongMouseClicked

    private void lbsaoluuMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbsaoluuMouseEntered
        lbsaoluu.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lbsaoluuMouseEntered

    private void lbsaoluuMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbsaoluuMouseExited
        lbsaoluu.setBorder(null);
    }//GEN-LAST:event_lbsaoluuMouseExited

    private void lbphuchoiMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbphuchoiMouseEntered
        lbphuchoi.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(153,153,255), 1, true));
    }//GEN-LAST:event_lbphuchoiMouseEntered

    private void lbphuchoiMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbphuchoiMouseExited
        lbphuchoi.setBorder(null);
    }//GEN-LAST:event_lbphuchoiMouseExited

    private void lbphuchoiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbphuchoiMouseClicked
        new SaoLuuPhucHoiBLL(this);
    }//GEN-LAST:event_lbphuchoiMouseClicked

    private void lbsaoluuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbsaoluuMouseClicked
        new SaoLuuPhucHoiBLL(connect);
    }//GEN-LAST:event_lbsaoluuMouseClicked

    private void lbdangnhapMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbdangnhapMouseClicked
        ShowLogin();
    }//GEN-LAST:event_lbdangnhapMouseClicked

    private void lbketnoiMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbketnoiMouseClicked
          KetNoiLaiSQL();
    }//GEN-LAST:event_lbketnoiMouseClicked
    public void createtabmohoc(){
        if(KiemTraTab(4)) 
        createTab("Môn Học",new FrmMonHoc(monhoc),4);
    }
    private void setCenterIconLabel(JLabel lb,boolean b)
    {
        lb.setVerticalTextPosition(AbstractButton.BOTTOM);
        lb.setHorizontalTextPosition(AbstractButton.CENTER);
        lb.setHorizontalAlignment(lb.CENTER);
        lb.setEnabled(b);
    }
    private void centerIconLabel(boolean b)
    {
        setCenterIconLabel(lbhocki,b);
        setCenterIconLabel(lbhocsinh1,b);
        setCenterIconLabel(lbchuyenlop1,b);
        setCenterIconLabel(lbdiem1,b);
        setCenterIconLabel(lbgiaovien1,b);
        setCenterIconLabel(lbketqua,b);
        setCenterIconLabel(lblamviecvoiexcel1,b);
        setCenterIconLabel(lbthongtinpm,b);
        setCenterIconLabel(lblophoc,b);
        setCenterIconLabel(lbnamhoc,b);
        setCenterIconLabel(lbphanlop1,b);
        setCenterIconLabel(lbmonhoc,b);
        setCenterIconLabel(lblamviecvoiexcel4,b);
        setCenterIconLabel(lblamviecvoiexcel5,b);
        setCenterIconLabel(lbtrogiup,b);
        setCenterIconLabel(lbhocsinh2,b);
        setCenterIconLabel(lbphancong,b);
        setCenterIconLabel(lbsaoluu,b);
        setCenterIconLabel(lbphuchoi,b);
    }
// </editor-fold>       
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmMain.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        //java.awt.EventQueue.invokeLater(new Runnable() {
        //    public void run() {
                new FrmMain();
        //    }
        //});
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lbchuyenlop1;
    private javax.swing.JLabel lbdangnhap;
    private javax.swing.JLabel lbdiem1;
    private javax.swing.JLabel lbgiaovien1;
    private javax.swing.JLabel lbhocki;
    private javax.swing.JLabel lbhocsinh1;
    private javax.swing.JLabel lbhocsinh2;
    private javax.swing.JLabel lbketnoi;
    private javax.swing.JLabel lbketqua;
    private javax.swing.JLabel lblamviecvoiexcel1;
    private javax.swing.JLabel lblamviecvoiexcel4;
    private javax.swing.JLabel lblamviecvoiexcel5;
    private javax.swing.JLabel lblophoc;
    private javax.swing.JLabel lbmonhoc;
    private javax.swing.JLabel lbnamhoc;
    private javax.swing.JLabel lbnguoidung;
    private javax.swing.JLabel lbnguoidungdangnhap;
    private javax.swing.JLabel lbphancong;
    private javax.swing.JLabel lbphanlop1;
    private javax.swing.JLabel lbphuchoi;
    private javax.swing.JLabel lbsaoluu;
    private javax.swing.JLabel lbthongtinpm;
    private javax.swing.JLabel lbtime;
    private javax.swing.JLabel lbtrogiup;
    private javax.swing.JPanel pnbg;
    private javax.swing.JPanel pnbottom;
    private javax.swing.JPanel pnlBackground;
    private javax.swing.JPanel pnlHeader;
    private javax.swing.JPanel pnlheader;
    private javax.swing.JTabbedPane tabHeader;
    private javax.swing.JTabbedPane tabmain;
    // End of variables declaration//GEN-END:variables
}