
package PresentationLayers;

import DataAccessLayers.DiemDAL;
import DataAccessLayers.HocKiDAL;
import DataAccessLayers.HocSinh_LopDAL;
import DataAccessLayers.KQDAL;
import DataAccessLayers.LopDAL;
import DataAccessLayers.MonHocDAL;
import DataAccessLayers.NamHocDAL;
import java.awt.Graphics;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author THOA
 */
public class FrmXuatKQ extends javax.swing.JPanel {

    private FrmMain frmmain;
    private ArrayList<KQDAL> arrkq=new ArrayList<KQDAL>();
    public FrmXuatKQ(FrmMain frmmain) {
        initComponents();
        this.frmmain=frmmain;
        frmmain.namhoc.AddComboBox(cbbnamhoc);
        frmmain.hocki.AddCombobox(cbbhocki);
        AddTableDiem();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        pnltop = new javax.swing.JPanel(){
            public void paintComponent(Graphics g){
                ImageIcon icon=new ImageIcon(getClass().getResource("/PresentationLayers/Images/bg7.png"));

                g.drawImage(icon.getImage(),0 , 0, this.getWidth(), 94, null);
                setOpaque(false);
                super.paintComponent(g);
            }
        };
        cbblop = new javax.swing.JComboBox();
        lbdieukien = new javax.swing.JLabel();
        lbnam = new javax.swing.JLabel();
        cbbnamhoc = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        cbbhocki = new javax.swing.JComboBox();
        pnlbottom = new javax.swing.JPanel(){
            public void paintComponent(Graphics g){
                ImageIcon icon=new ImageIcon(getClass().getResource("/PresentationLayers/Images/bg7.png"));

                g.drawImage(icon.getImage(),0 , 0, this.getWidth(), 126, null);
                setOpaque(false);
                super.paintComponent(g);
            }
        };
        jLabel3 = new javax.swing.JLabel();
        cbbxuatra = new javax.swing.JComboBox();
        jLabel4 = new javax.swing.JLabel();
        txtnoiluufile = new javax.swing.JTextField();
        btnchooser = new javax.swing.JButton();
        btnxuat = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablehocsinh = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablediem = new javax.swing.JTable();

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        pnltop.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 153), 1, true));

        cbblop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbblopActionPerformed(evt);
            }
        });

        lbdieukien.setText("Lớp Học:");

        lbnam.setText("Năm Học:");

        cbbnamhoc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbnamhocActionPerformed(evt);
            }
        });

        jLabel1.setText("Học Kì:");

        cbbhocki.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbhockiActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnltopLayout = new javax.swing.GroupLayout(pnltop);
        pnltop.setLayout(pnltopLayout);
        pnltopLayout.setHorizontalGroup(
            pnltopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnltopLayout.createSequentialGroup()
                .addGap(216, 216, 216)
                .addComponent(lbnam)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbbnamhoc, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lbdieukien)
                .addGap(18, 18, 18)
                .addComponent(cbblop, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbbhocki, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnltopLayout.setVerticalGroup(
            pnltopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnltopLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(pnltopLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cbblop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbdieukien)
                    .addComponent(lbnam)
                    .addComponent(cbbnamhoc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(cbbhocki, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(39, Short.MAX_VALUE))
        );

        pnlbottom.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 153), 1, true));

        jLabel3.setText("Xuất Ra:");

        cbbxuatra.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Microsoft Excel", "Microsoft Word" }));
        cbbxuatra.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbxuatraActionPerformed(evt);
            }
        });

        jLabel4.setText("Nơi Lưu File:");

        txtnoiluufile.setText("C:\\\\DanhSachHocSinh.xls");
        txtnoiluufile.setEnabled(false);

        btnchooser.setText("...");
        btnchooser.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnchooserActionPerformed(evt);
            }
        });

        btnxuat.setText("Xuất Danh Sách");
        btnxuat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnxuatActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pnlbottomLayout = new javax.swing.GroupLayout(pnlbottom);
        pnlbottom.setLayout(pnlbottomLayout);
        pnlbottomLayout.setHorizontalGroup(
            pnlbottomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlbottomLayout.createSequentialGroup()
                .addGap(104, 104, 104)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbbxuatra, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtnoiluufile, javax.swing.GroupLayout.PREFERRED_SIZE, 335, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnchooser, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41)
                .addComponent(btnxuat, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(166, Short.MAX_VALUE))
        );
        pnlbottomLayout.setVerticalGroup(
            pnlbottomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnlbottomLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(pnlbottomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cbbxuatra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(txtnoiluufile, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnchooser)
                    .addComponent(btnxuat, javax.swing.GroupLayout.DEFAULT_SIZE, 39, Short.MAX_VALUE))
                .addContainerGap(61, Short.MAX_VALUE))
        );

        tablehocsinh.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
            },
            new String [] {
                "Mã Học Sinh", "Tên Học Sinh"
            }
        ));
        tablehocsinh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablehocsinhMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablehocsinh);

        tablediem.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null,null, null, null, null, null},
            },
            new String [] {
                "Môn Học","Miệng", "15 Phút", "45 Phút", "Thi", "Trung Bình Môn"
            }
        ));
        jScrollPane2.setViewportView(tablediem);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnltop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(pnlbottom, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 744, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(pnltop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlbottom, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1128, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 461, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void cbblopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbblopActionPerformed
        try {
            setPatch();
            tablehocsinh.setModel(new DefaultTableModel(getDataTableHocSinh(),getColumnTableHocSinh()));
            tablehocsinh.changeSelection(tablehocsinh.getSelectedRow(),0,true,false);
            tablehocsinh.changeSelection(0,0,true,false);
            AddTableDiem();
        } catch (Exception e) {
        }
    }//GEN-LAST:event_cbblopActionPerformed

    private void cbbnamhocActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbnamhocActionPerformed
        try {
            cbblop.removeAllItems();
            frmmain.lop.AddComboBoxLop(cbblop, ((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa());
            setPatch();
        } catch (Exception e) {
        }
    }//GEN-LAST:event_cbbnamhocActionPerformed

    private void cbbxuatraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbxuatraActionPerformed
      try {
            txtnoiluufile.setText(removeDuoi(txtnoiluufile.getText())+"."+GetDuoi());
        } catch (Exception e) {
        }
    }//GEN-LAST:event_cbbxuatraActionPerformed

    private void btnchooserActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnchooserActionPerformed
        JFileChooser fc = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(cbbxuatra.getSelectedItem().toString(),GetDuoi());
        fc.setFileFilter(filter);
        fc.setSelectedFile(new File(txtnoiluufile.getText()));
        int returnVal = fc.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fc.getSelectedFile();
            txtnoiluufile.setText(file.getPath());
        }
    }//GEN-LAST:event_btnchooserActionPerformed

    private void btnxuatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnxuatActionPerformed
        if(tablehocsinh.getRowCount()==0)
            JOptionPane.showMessageDialog(null, "Không Có Dữ Liệu");
        else
            XuatKQHT();
    }//GEN-LAST:event_btnxuatActionPerformed

    private void tablehocsinhMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablehocsinhMouseClicked
        AddTableDiem();
    }//GEN-LAST:event_tablehocsinhMouseClicked

    private void cbbhockiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbhockiActionPerformed
        try {
            AddTableDiem();
        } catch (Exception e) {
        }
    }//GEN-LAST:event_cbbhockiActionPerformed
    private String GetDuoi()
    {
        if(cbbxuatra.getSelectedIndex()==0)
            return "xls";
        return "doc";
    }
    private void setPatch()
    {
        txtnoiluufile.setText("C:\\KetQuaHocTapLop"+cbblop.getSelectedItem().toString()+"NamHoc"+cbbnamhoc.getSelectedItem().toString()+"."+GetDuoi());
    }
    private void AddTableDiem()
    {
        try {
            if(cbbhocki.getSelectedIndex()==2){
                tablediem.setModel(new DefaultTableModel(getDataTableKQ1(),getColumnTableDiem()));
                AddRow1();
            }
            else{
                getKQ();
                tablediem.setModel(new DefaultTableModel(getDataTableKQ(),getColumnTableDiem()));
                AddRow();
            }
        } catch (Exception e) {
        }
    }
    private Vector getColumnTableHocSinh()
    {
        Vector column=new Vector();
        
        column.add("Mã Học Sinh");
        column.add("Tên Học Sinh");
        
        return column;
    }
    private Vector getColumnTableDiem()
    {
        Vector column=new Vector();
        
        column.add("");
        if(cbbhocki.getSelectedIndex()<2)
        {
            column.add("Miệng");
            column.add("15 Phút");
            column.add("45 Phút");
            column.add("Thi");
        }
        column.add("Trung Bình Môn");
        return column;
    }
    private Vector getDataTableHocSinh()
    {
        Vector row=new Vector();
        Vector dr;
        for(HocSinh_LopDAL hsl : frmmain.hocsinhtronglop.getArrHSL(((LopDAL)(cbblop.getSelectedItem())).getMaLop()))
        {
            dr=new Vector();
            
            dr.add(hsl.getHocSinh().getMaHS());
            dr.add(hsl.getHocSinh().getHoTen());
            
            row.add(dr);
        }
        return row;
    }
    private void getKQ()
    {
        arrkq.clear();
        int MaNH=((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa();
        for(HocSinh_LopDAL hsl : frmmain.hocsinhtronglop.getArrHSL(((LopDAL)(cbblop.getSelectedItem())).getMaLop()))
        {
                for(MonHocDAL mh : frmmain.monhoc.getArrayMH())
                {
                    KQDAL kq=new KQDAL();
                    
                    kq.setMaMH(mh.getMa());
                    kq.setMaHS(hsl.getHocSinh().getMaHS());
                    kq.setMaNH(MaNH);
                    kq.setM(getDiem(MaNH,hsl.getHocSinh().getMaHS(),mh.getMa(),1,kq));
                    kq.setP1(getDiem(MaNH,hsl.getHocSinh().getMaHS(),mh.getMa(),2,kq));
                    kq.setP2(getDiem(MaNH,hsl.getHocSinh().getMaHS(),mh.getMa(),3,kq));
                    kq.setT(getDiem(MaNH,hsl.getHocSinh().getMaHS(),mh.getMa(),4,kq));
                    kq.setTB(DiemTB(kq.getM(), kq.getP1(), kq.getP2(), kq.getT()));
                    
                    arrkq.add(kq);
                }
        }      
    }
    private String getDiem(int MaNH,String MaHS,int MaMH,int Loai,KQDAL kq)
    {
        for(DiemDAL d : frmmain.diem.getArrayDiem())
        {
            if(d.getHocSinh().getHocSinh().getMaHS().equals(MaHS) && d.getLoaiDiem().getMaLoai()==Loai 
                    && d.getMonHoc().getMa()==MaMH && d.getMonHoc().getMa()==MaMH && d.getNamHoc().getMa()==MaNH) 
            {
                kq.setMaHK(d.getHocKi().getMa());
                return d.getDiem();
            }
        }
        return "";
    }
    private Vector getDataTableKQ()
    {
        Vector row=new Vector();
        Vector dr;
        int MaNH=((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa();
        int MaHK=((HocKiDAL)(cbbhocki.getSelectedItem())).getMa();
        String MaHS=tablehocsinh.getValueAt(tablehocsinh.getSelectedRow(), 0).toString();
        for(MonHocDAL mh : frmmain.monhoc.getArrayMH())
        {
            dr=new Vector();
            dr.add(mh.getTen());
            for(KQDAL kq: arrkq)
            {
                if(kq.getMaHS().equals(MaHS) && kq.getMaMH()==mh.getMa() && kq.getMaNH()==MaNH && kq.getMaHK()==MaHK) 
                {
                    dr.add(kq.getM());
                    dr.add(kq.getP1());
                    dr.add(kq.getP2());
                    dr.add(kq.getT());
                    dr.add(kq.getTB());
                }
            }
            row.add(dr);
        }
        return row;
    }
    private Vector getDataTableKQ1()
    {
        Vector row=new Vector();
        Vector dr;
        int MaNH=((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa();
        String MaHS=tablehocsinh.getValueAt(tablehocsinh.getSelectedRow(), 0).toString();
        double diem1;
        double diem2;
        DecimalFormat df=new DecimalFormat("#.#"); 
        for(MonHocDAL mh : frmmain.monhoc.getArrayMH())
        {
                dr=new Vector();
                dr.add(mh.getTen());
                diem1=0.0;
                diem2=0.0;
                for(int i=1;i<=2;i++)
                {  
                    for(KQDAL kq: arrkq)
                    {
                        if(kq.getMaHS().equals(MaHS) && kq.getMaMH()==mh.getMa() && kq.getMaNH()==MaNH && kq.getMaHK()==i) 
                        {
                            if(i==1)
                                diem1=Double.parseDouble(kq.getTB());
                            else
                                diem2=Double.parseDouble(kq.getTB());
                        }
                    }
                       
                }
                //điểm trung bình cả năm bằng ((hk2 * 2) +hk1)/3
                 dr.add(df.format((diem2*2+diem1)/3));
                row.add(dr);
        }
        return row;
    }
    private void AddRow()
    {
        DefaultTableModel Model = (DefaultTableModel)tablediem.getModel(); 
        Model.addRow(new Object[]{"Trung Bình","","","","",getDiemTB()});
        Model.addRow(new Object[]{"Học Lực","","","","",getHocLuc()});
        Model.addRow(new Object[]{"Hạnh Kiểm","","","","",frmmain.kqht.getHanhKiem(
                tablehocsinh.getValueAt(tablehocsinh.getSelectedRow(), 0).toString())});
    }
    private void AddRow1()
    {
        DefaultTableModel Model = (DefaultTableModel)tablediem.getModel(); 
        DecimalFormat df=new DecimalFormat("#.#");
        double diemtb=getDiemTBCaNam();
        Model.addRow(new Object[]{"Trung Bình",df.format(diemtb)});
        Model.addRow(new Object[]{"Học Lực",getHocLuc(diemtb)});
        Model.addRow(new Object[]{"Hạnh Kiểm",frmmain.kqht.getHanhKiem(
                tablehocsinh.getValueAt(tablehocsinh.getSelectedRow(), 0).toString())});
    }
    private double getDiemTBCaNam()
    {
        double diemtb=0.0;
        for(int i=0;i<tablediem.getRowCount();i++)
        {
            diemtb+=Double.parseDouble(tablediem.getValueAt(i, 1).toString())*getHeSo(tablediem.getValueAt(i, 0).toString().trim());
        }
        return diemtb/getTong();
    }
    private String getHocLuc()
    {
        double diemtb=Double.parseDouble(tablediem.getValueAt(tablediem.getRowCount()-1, 5).toString());
        if(diemtb>=8)
            return "Giỏi";
        if(diemtb>=6.5)
            return "Khá";
        if(diemtb>=5)
            return "Trung Bình";
        if(diemtb>=3.5)
            return "Yếu";
        return "Kém";
    }
    private String getHocLuc(double diemtb)
    {
        if(diemtb>=8)
            return "Giỏi";
        if(diemtb>=6.5)
            return "Khá";
        if(diemtb>=5)
            return "Trung Bình";
        if(diemtb>=3.5)
            return "Yếu";
        return "Kém";
    }
    private double getTong()
    {
        double tong=0.0;
        for(MonHocDAL mh: frmmain.monhoc.getArrayMH())
        {
              tong+=mh.getHeSo();
        }
        return tong;
    }
    private double getHeSo(String tenmh)
    {
        for(MonHocDAL mh: frmmain.monhoc.getArrayMH())
        {
                 if(mh.getTen().equals(tenmh))
                     return mh.getHeSo()*1.0;
        }
        return 0.0;
    }
    private double getDiemTB()
    {
        double diem=0.0;
        String value;
        for(int i=0;i<tablediem.getRowCount();i++)
        {
            value=tablediem.getValueAt(i, 5).toString().trim();
            if(value!="")
                diem+=Double.parseDouble(value)*getHeSo(tablediem.getValueAt(i, 0).toString().trim());
        }
        return diem/getTong();
    }
    private String DiemTB(String Mieng,String phut15,String phut45,String Thi)
    {
        if(Mieng!="" && phut15!="" && phut45!="" && Thi!="")
        {
            DecimalFormat df=new DecimalFormat("#.#");
            return df.format((TachDiem(Mieng)+TachDiem(phut15)+(TachDiem(phut45)*2)+(TachDiem(Thi)*3))/7);
        }
        else
            return "";
    }
    private double TachDiem(String diem){
        String[] str=diem.split(";");
        if(str.length==1)
            return Double.parseDouble(diem);
        else
        {
            float tong=0;
            for(int i=0;i<str.length;i++){
                tong+=Double.parseDouble(str[i].trim());
            }
            return tong/str.length;
        }
    }
    private void XuatKQHT()
    {
       try {
            byte[] bom = new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xBF }; 
            FileOutputStream fout = new FileOutputStream(txtnoiluufile.getText().trim());
            fout.write(bom);
            OutputStreamWriter bw = new OutputStreamWriter(fout, "UTF-8");
            PrintWriter printWriter = new PrintWriter(bw);
            printWriter.print("\n");
            printWriter.print(getHTML());
            bw.close();
            int result = JOptionPane.showConfirmDialog(null, "Xuất Kết Quả Học Tập Thành Công.\nBạn Có Muốn Mở File Vừa Xuất?", "Thông Báo", JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION)
            {
                Runtime runtime = Runtime.getRuntime();
                Process process =runtime.exec("cmd /c start "+txtnoiluufile.getText().trim());
            }
        } catch (Exception e) {
            if(e.getMessage().indexOf("(Access is denied)")!=-1)
                JOptionPane.showMessageDialog(null, "Không thể lưu dữ liệu vào Ổ Đĩa "+txtnoiluufile.getText().substring(0, 1)+". Vui lòng chọn ổ khác");
            else
                JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }
    private String getHTML()
    {
        StringBuilder sb=new StringBuilder();
        sb.append(getHeader());
        int MaNH=((NamHocDAL)(cbbnamhoc.getSelectedItem())).getMa();
        int MaHK=((HocKiDAL)(cbbhocki.getSelectedItem())).getMa();
        double tb;
        double diemtb;
        boolean b=false;
        for(int i=0;i<tablehocsinh.getRowCount();i++)
        {
            tb=0.0;
            sb.append(getTieuDe(i));
            sb.append("<tr>");
            sb.append("<th></th>");
            sb.append("<th>Miệng</th>");
            sb.append("<th>15 Phút</th>");
            sb.append("<th>45 Phút</th>");
            sb.append("<th>Thi</th>");
            sb.append("<th>Trung Bình Môn</th>");
            sb.append("</tr>");
            for(MonHocDAL mh :frmmain.monhoc.getArrayMH())
            {
                b=false;
                sb.append("<tr>");
                sb.append("<td class='border'>"+mh.getTen()+"</td>");
                for(KQDAL kq : arrkq)
                {
                    if(kq.getMaHS().equals(tablehocsinh.getValueAt(i, 0).toString()) && kq.getMaMH()==mh.getMa()
                            && kq.getMaNH()==MaNH && kq.getMaHK()==MaHK)
                    {
                       
                        sb.append("<td class='border'>"+kq.getM()+"</td>");
                        sb.append("<td class='border'>"+kq.getP1()+"</td>");
                        sb.append("<td class='border'>"+kq.getP2()+"</td>");
                        sb.append("<td class='border'>"+kq.getT()+"</td>");
                        sb.append("<td class='border'>"+kq.getTB()+"</td>");
                        if(kq.getTB()!="")
                            tb+=Double.parseDouble(kq.getTB())*mh.getHeSo();
                        b=true;
                    }
                }
                if(!b)
                {
                    sb.append("<td class='border'></td>");
                    sb.append("<td class='border'></td>");
                    sb.append("<td class='border'></td>");
                    sb.append("<td class='border'></td>");
                    sb.append("<td class='border'></td>");
                }
                sb.append("</tr>");
            }
            diemtb=tb/getTong();
            sb.append("<tr><td class='border'>Trung Bình</td><td colspan='4' class='border'></td><td class='border'>"+diemtb+"</td></tr>");
            sb.append("<tr><td class='border'>Học Lực</td><td colspan='4' class='border'></td><td class='border'>"+getHocLuc(diemtb)+"</td></tr>");
            sb.append("<tr><td class='border'>Hạnh Kiểm</td><td class='border' colspan='4'>"
                    + "</td><td class='border'>"+frmmain.kqht.getHanhKiem(tablehocsinh.getValueAt(i, 0).toString())+"</td></tr>");
        }
        sb.append(getFooter());
        return sb.toString();
    }
    private String getHeader()
    {
        if(cbbxuatra.getSelectedIndex()==1)
            return "<html><meta charset='utf-8' /><style>table tr td.border,table th{border:1px solid #000;padding:3px}</style>"
                    + "<div style='width:100%;text-align:center'>"
                    + "<div style='width'700px;margin:0px auto;text-align:left;padding:10px'>"
                    + "<table width='700' cellspacing='0' cellpadding='0'>";
            return "<html><meta charset='utf-8' />"
                    + "<table cellspacing='0' cellpadding='3' border='1'>";
    }
    private String getFooter()
    {
        if(cbbxuatra.getSelectedIndex()==1)
            return "</table></div></div></html>";
            return "</table></html>";
    }
    private String getTieuDe(int row)
    {
        return "<tr><td colspan='5'><div>-</div><td></tr><tr><td colspan='3'><b>Tên Học Sinh:</b> "+tablehocsinh.getValueAt(row, 1).toString()+"</td>"
                + "<td colspan='3'><b>Lớp:</b> "+cbblop.getSelectedItem().toString()+"</td>"
                + "</tr><tr><td colspan='3'><b>Năm Học:</b> "+cbbnamhoc.getSelectedItem().toString()+"</td>"
                + "<td colspan='3'><b>Học Kì:</b> "+cbbhocki.getSelectedItem().toString()+"</td></tr>";
    }
     private String removeDuoi(String Duoi)
    {
        Duoi=Duoi.replace(".xls", "");
        Duoi=Duoi.replace(".doc", "");
        return Duoi;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnchooser;
    private javax.swing.JButton btnxuat;
    private javax.swing.JComboBox cbbhocki;
    private javax.swing.JComboBox cbblop;
    private javax.swing.JComboBox cbbnamhoc;
    private javax.swing.JComboBox cbbxuatra;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lbdieukien;
    private javax.swing.JLabel lbnam;
    private javax.swing.JPanel pnlbottom;
    private javax.swing.JPanel pnltop;
    private javax.swing.JTable tablediem;
    private javax.swing.JTable tablehocsinh;
    private javax.swing.JTextField txtnoiluufile;
    // End of variables declaration//GEN-END:variables
}
