

package DataAccessLayers;

import PresentationLayers.FrmMain;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


public final class clsDatabase {
    private FrmMain parent;
    private Connection con=null;
    private String Server;
    private String Database;
    private String Username;
    private String Password;
    private String pordNumber;
    private String ConnectionString;
    
    //Dùng khi kết nối csdl thất bại
    public clsDatabase()
    {
        this.Server="DESKTOP-CPPLGMT\\SQLEXPRESS";
        this.Database="QLHS_PhoThong";
        this.Username="sa";
        this.Password="HMH123";
        this.pordNumber = "1433";
        this.ConnectionString="jdbc:odbc:Driver={SQL Server};Server="+Server+";Database="+Database+";UserName="+Username+";Password="+Password;
        /*this.Server="//WinXPDev";
        this.Database="QLHS_PhoThong";
        this.Username="thoa11cth1";
        this.Password="thoa11cth1";
        this.ConnectionString="jdbc:sqlserver:"+this.Server+";Database="+this.Database+";UserName="+this.Username+";Password="+this.Password;*/
        ConnectSQLServer();
    }
    //dùng khi bắt đầu chạy phần mềm
    public clsDatabase(FrmMain parent)
    {
        this.parent=parent;
        this.Server="DESKTOP-CPPLGMT\\SQLEXPRESS";
        this.Database="QLHS_PhoThong";
        this.Username="sa";
        this.Password="HMH123";
        this.pordNumber = "1433";
        this.ConnectionString="jdbc:odbc:Driver={SQL Server};Server="+Server+";Database="+Database+";UserName="+Username+";Password="+Password;
        /*this.Server="//WinXPDev";
        this.Database="QLHS_PhoThong";
        this.Username="thoa11cth1";
        this.Password="thoa11cth1";
        this.ConnectionString="jdbc:sqlserver:"+this.Server+";Database="+this.Database+";UserName="+this.Username+";Password="+this.Password;*/
        ConnectSQLServer();
    }
    //dùng để kết nối lại database có password vào username
    public clsDatabase(String Servername, String Databasename, String Username, String Password) {
        this.Database=Databasename;
        //this.ConnectionString="jdbc:sqlserver:"+Servername+";Database="+Databasename+";UserName="+Username+";Password="+Password;
        ConnectionString="jdbc:odbc:Driver={SQL Server};Server="+Servername+";Database="+Databasename+";UserName="+Username+";Password="+Password;
        ConnectSQLServer();
    }
    //dùng để kết nối lại database không có password vào username
    public clsDatabase(String Servername, String Databasename) {
        this.Database=Databasename;
        //this.ConnectionString="jdbc:sqlserver:"+Servername+";Database="+Databasename;
        ConnectionString="jdbc:odbc:Driver={SQL Server};Server="+Servername+";Database="+Databasename;
        ConnectSQLServer();
    }
   
    
    //Thêm Sửa Xóa Dữ Liệu
    public void ExecuteSPAction(String spName, String[] value)
    {
        
        try
        {
            spName = spName + "(";
            for(int i=0;i<value.length;i++)
            {
                if(i==0)
                    spName = spName + "?";
                else
                    spName = spName + ",?";
            }
            spName = spName + ")";
            CallableStatement cs=con.prepareCall("{call " + spName + "}");
            for(int i=0;i<value.length;i++)
            {
                cs.setString(i+1,value[i]);
            }
            cs.execute();
        }
        catch (SQLException e) 
        {  
        }               
    }
    
    
    //Lấy Dữ Liệu Có Điều Kiện
    public ResultSet ExecuteSPSelect(String spName, String[] value)
    {
        ResultSet rs = null;
        
        try
        {
            if(value.length>0)
            {
                spName = spName + "(";
                for(int i=0;i<value.length;i++)
                {
                    if(i==0)
                        spName = spName + "?";
                    else
                        spName = spName + ",?";
                }
                spName = spName + ")";
            }
            CallableStatement cs=con.prepareCall("{call " + spName + "}");
            for(int i=0;i<value.length;i++)
            {
                cs.setString(i+1,value[i]);
            }
            rs=cs.executeQuery();
        }
        catch (SQLException e) 
        {  
        }
        
        return rs;
    }
    //Lấy Dữ Liệu Không Có Điều Kiện
    public ResultSet ExecuteSPSelect(String spName)
    {
        ResultSet rs = null;
        
        try
        {
            CallableStatement cs=con.prepareCall("{call " + spName + "}");
            rs=cs.executeQuery();
        }
        catch (SQLException e) 
        {  
        }
        
        return rs;
    }
    
    //kết nối đến database
    public void ConnectSQLServer()
    {
        try
        {
            /*Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con = DriverManager.getConnection(this.ConnectionString);*/
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            con = DriverManager.getConnection(ConnectionString);
        }
        catch(ClassNotFoundException | SQLException ex)
        {
            parent.KetNoiLaiSQL();
        }
        
    }
    public String getBatabase()
    {
        return this.Database;
    }
    public Connection getConnect()
    {
        if(con==null)
            ConnectSQLServer();
        return this.con;
    }
}
