

package DataAccessLayers;

public class LopDAL {
    private int MaLop;
    private String TenLop;
    private NamHocDAL MaNamHoc;
    private GiaoVienDAL MaGiaoVien;
    private int SiSo;

    public LopDAL() {
        this.MaLop = 0;
        this.TenLop = "";
        this.MaNamHoc = null;
        this.MaGiaoVien = null;
        this.SiSo = 0;
    }

    public LopDAL(int MaLop, String TenLop, NamHocDAL MaNamHoc, GiaoVienDAL MaGiaoVien, int SiSo) {
        this.MaLop = MaLop;
        this.TenLop = TenLop;
        this.MaNamHoc = MaNamHoc;
        this.MaGiaoVien = MaGiaoVien;
        this.SiSo = SiSo;
    }

    public int getMaLop() {
        return MaLop;
    }

    public void setMaLop(int MaLop) {
        this.MaLop = MaLop;
    }

    public String getTenLop() {
        return TenLop;
    }

    public void setTenLop(String TenLop) {
        this.TenLop = TenLop;
    }

    public NamHocDAL getMaNamHoc() {
        return MaNamHoc;
    }

    public void setMaNamHoc(NamHocDAL MaNamHoc) {
        this.MaNamHoc = MaNamHoc;
    }

    public GiaoVienDAL getMaGiaoVien() {
        return MaGiaoVien;
    }

    public void setMaGiaoVien(GiaoVienDAL MaGiaoVien) {
        this.MaGiaoVien = MaGiaoVien;
    }

    public int getSiSo() {
        return SiSo;
    }

    public void setSiSo(int SiSo) {
        this.SiSo = SiSo;
    }
    
    @Override
    public String toString()
    {
        return this.TenLop;
    }
}
