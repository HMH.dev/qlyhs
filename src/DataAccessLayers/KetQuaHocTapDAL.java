

package DataAccessLayers;


public class KetQuaHocTapDAL {
    private HocSinhDAL HocSinh;
    private HocKiDAL HocKi;
    private NamHocDAL NamHoc;
    private String HocLuc;
    private String HanhKiem;

    public KetQuaHocTapDAL() {
    }

    public KetQuaHocTapDAL(HocSinhDAL HocSinh, HocKiDAL HocKi, NamHocDAL NamHoc, String HocLuc, String HanhKiem) {
        this.HocSinh = HocSinh;
        this.HocKi = HocKi;
        this.NamHoc = NamHoc;
        this.HocLuc = HocLuc;
        this.HanhKiem = HanhKiem;
    }

    public HocSinhDAL getHocSinh() {
        return HocSinh;
    }

    public void setHocSinh(HocSinhDAL HocSinh) {
        this.HocSinh = HocSinh;
    }

    public HocKiDAL getHocKi() {
        return HocKi;
    }

    public void setHocKi(HocKiDAL HocKi) {
        this.HocKi = HocKi;
    }

    public NamHocDAL getNamHoc() {
        return NamHoc;
    }

    public void setNamHoc(NamHocDAL NamHoc) {
        this.NamHoc = NamHoc;
    }

    public String getHocLuc() {
        return HocLuc;
    }

    public void setHocLuc(String HocLuc) {
        this.HocLuc = HocLuc;
    }

    public String getHanhKiem() {
        return HanhKiem;
    }

    public void setHanhKiem(String HanhKiem) {
        this.HanhKiem = HanhKiem;
    }
    
    
}
