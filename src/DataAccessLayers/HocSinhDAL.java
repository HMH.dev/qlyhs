

package DataAccessLayers;

public class HocSinhDAL extends NguoiDAL{
    private String MaHS;
    private String HoTenCha;
    private String HoTenMe;

    public HocSinhDAL() {
    }

    public HocSinhDAL(String MaHS, String HoTenCha, String HoTenMe) {
        this.MaHS = MaHS;
        this.HoTenCha = HoTenCha;
        this.HoTenMe = HoTenMe;
    }
    
    public String getMaHS() {
        return MaHS;
    }

    public void setMaHS(String MaHS) {
        this.MaHS = MaHS;
    }

    public String getHoTenCha() {
        return HoTenCha;
    }

    public void setHoTenCha(String HoTenCha) {
        this.HoTenCha = HoTenCha;
    }

    public String getHoTenMe() {
        return HoTenMe;
    }

    public void setHoTenMe(String HoTenMe) {
        this.HoTenMe = HoTenMe;
    }
    
    @Override
    public String toString() {
        return getHoTen();
    }
    
}
