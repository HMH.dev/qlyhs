
package DataAccessLayers;


public class DiemDAL {
    private HocSinh_LopDAL HocSinh;
    private MonHocDAL MonHoc;
    private HocKiDAL HocKi;
    private NamHocDAL NamHoc;
    private LoaiDiemDAL LoaiDiem;
    private String Diem;

    public DiemDAL() {
        
    }

    public DiemDAL(HocSinh_LopDAL HocSinh, MonHocDAL MonHoc, HocKiDAL HocKi, NamHocDAL NamHoc, LoaiDiemDAL LoaiDiem, String Diem) {
        this.HocSinh = HocSinh;
        this.MonHoc = MonHoc;
        this.HocKi = HocKi;
        this.NamHoc = NamHoc;
        this.LoaiDiem = LoaiDiem;
        this.Diem = Diem;
    }

    public HocSinh_LopDAL getHocSinh() {
        return HocSinh;
    }

    public void setHocSinh(HocSinh_LopDAL HocSinh) {
        this.HocSinh = HocSinh;
    }

    public MonHocDAL getMonHoc() {
        return MonHoc;
    }

    public void setMonHoc(MonHocDAL MonHoc) {
        this.MonHoc = MonHoc;
    }

    public HocKiDAL getHocKi() {
        return HocKi;
    }

    public void setHocKi(HocKiDAL HocKi) {
        this.HocKi = HocKi;
    }

    public NamHocDAL getNamHoc() {
        return NamHoc;
    }

    public void setNamHoc(NamHocDAL NamHoc) {
        this.NamHoc = NamHoc;
    }

    public LoaiDiemDAL getLoaiDiem() {
        return LoaiDiem;
    }

    public void setLoaiDiem(LoaiDiemDAL LoaiDiem) {
        this.LoaiDiem = LoaiDiem;
    }

    public String getDiem() {
        return Diem;
    }

    public void setDiem(String Diem) {
        this.Diem = Diem;
    }
    
    
}
