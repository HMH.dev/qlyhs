
package DataAccessLayers;


public class PhanCongDAL {
    private NamHocDAL MaNH;
    private HocKiDAL MaHK;
    private LopDAL MaLop;
    private GiaoVienDAL GiaoVien;

    public PhanCongDAL() {
    }

    public PhanCongDAL(NamHocDAL MaNH, HocKiDAL MaHK, LopDAL MaLop, GiaoVienDAL GiaoVien) {
        this.MaNH = MaNH;
        this.MaHK = MaHK;
        this.MaLop = MaLop;
        this.GiaoVien = GiaoVien;
    }

    public NamHocDAL getMaNH() {
        return MaNH;
    }

    public void setMaNH(NamHocDAL MaNH) {
        this.MaNH = MaNH;
    }

    public HocKiDAL getMaHK() {
        return MaHK;
    }

    public void setMaHK(HocKiDAL MaHK) {
        this.MaHK = MaHK;
    }

    public LopDAL getMaLop() {
        return MaLop;
    }

    public void setMaLop(LopDAL MaLop) {
        this.MaLop = MaLop;
    }

    public GiaoVienDAL getGiaoVien() {
        return GiaoVien;
    }

    public void setGiaoVien(GiaoVienDAL GiaoVien) {
        this.GiaoVien = GiaoVien;
    }
    
    
}
