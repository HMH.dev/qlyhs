

package DataAccessLayers;

public class HocSinh_LopDAL {
    private HocSinhDAL HocSinh;
    private LopDAL Lop;
    private HocKiDAL HocKi;
    private NamHocDAL NamHoc;

    public HocSinh_LopDAL() {
    }

    public HocSinh_LopDAL(HocSinhDAL HocSinh, LopDAL Lop, HocKiDAL HocKi, NamHocDAL NamHoc) {
        this.HocSinh = HocSinh;
        this.Lop = Lop;
        this.HocKi = HocKi;
        this.NamHoc = NamHoc;
    }

    public HocSinhDAL getHocSinh() {
        return HocSinh;
    }

    public void setHocSinh(HocSinhDAL HocSinh) {
        this.HocSinh = HocSinh;
    }

    public LopDAL getLop() {
        return Lop;
    }

    public void setLop(LopDAL Lop) {
        this.Lop = Lop;
    }

    public HocKiDAL getHocKi() {
        return HocKi;
    }

    public void setHocKi(HocKiDAL HocKi) {
        this.HocKi = HocKi;
    }

    public NamHocDAL getNamHoc() {
        return NamHoc;
    }

    public void setNamHoc(NamHocDAL NamHoc) {
        this.NamHoc = NamHoc;
    }
    
    @Override
    public String toString()
    {
        return this.HocSinh.getHoTen();
    }
    
}
