

package DataAccessLayers;


public class clsDAL {
    private int Ma;
    private String Ten;

    public clsDAL() {
    }

    public int getMa() {
        return Ma;
    }

    public void setMa(int Ma) {
        this.Ma = Ma;
    }

    public String getTen() {
        return Ten;
    }

    public void setTen(String Ten) {
        this.Ten = Ten;
    }
    
    @Override
    public String toString() {
        return Ten;
    }
}
