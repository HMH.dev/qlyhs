

package DataAccessLayers;


public class LoginDAL {
    private String Name;
    private String TenLoai;
    private boolean Them;
    private boolean Sua;
    private boolean Xoa;
    private boolean ThemThanhVienMoi;
    private boolean XoaThanhVien;
    private boolean KhoaThanhVien;

    public LoginDAL() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getTenLoai() {
        return TenLoai;
    }

    public void setTenLoai(String TenLoai) {
        this.TenLoai = TenLoai;
    }

    public boolean isThem() {
        return Them;
    }

    public void setThem(boolean Them) {
        this.Them = Them;
    }

    public boolean isSua() {
        return Sua;
    }

    public void setSua(boolean Sua) {
        this.Sua = Sua;
    }

    public boolean isXoa() {
        return Xoa;
    }

    public void setXoa(boolean Xoa) {
        this.Xoa = Xoa;
    }

    public boolean isThemThanhVienMoi() {
        return ThemThanhVienMoi;
    }

    public void setThemThanhVienMoi(boolean ThemThanhVienMoi) {
        this.ThemThanhVienMoi = ThemThanhVienMoi;
    }

    public boolean isXoaThanhVien() {
        return XoaThanhVien;
    }

    public void setXoaThanhVien(boolean XoaThanhVien) {
        this.XoaThanhVien = XoaThanhVien;
    }

    public boolean isKhoaThanhVien() {
        return KhoaThanhVien;
    }

    public void setKhoaThanhVien(boolean KhoaThanhVien) {
        this.KhoaThanhVien = KhoaThanhVien;
    }
    
}
