

package DataAccessLayers;


public class GiaoVienDAL extends NguoiDAL{
    private int MaGV;
    private MonHocDAL MaMonHoc;
    private String DienThoai;

    public GiaoVienDAL() {
        
    }

    public int getMaGV() {
        return MaGV;
    }

    public void setMaGV(int MaGV) {
        this.MaGV = MaGV;
    }

    public MonHocDAL getMaMonHoc() {
        return MaMonHoc;
    }

    public void setMaMonHoc(MonHocDAL MaMonHoc) {
        this.MaMonHoc = MaMonHoc;
    }

    public String getDienThoai() {
        return DienThoai;
    }

    public void setDienThoai(String DienThoai) {
        this.DienThoai = DienThoai;
    }

    @Override
    public String toString()
    {
        return getHoTen();
    }
    
}
