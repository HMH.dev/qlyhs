

package DataAccessLayers;

public class NguoiDAL {
    private String HoTen;
    private boolean GioiTinh;
    private String NgaySinh;
    private String NoiSinh;
    private DanTocDAL DanToc;
    private TonGiaoDAL TonGiao;

    public String getHoTen() {
        return HoTen;
    }

    public void setHoTen(String HoTen) {
        this.HoTen = HoTen;
    }

    public boolean isGioiTinh() {
        return GioiTinh;
    }

    public void setGioiTinh(boolean GioiTinh) {
        this.GioiTinh = GioiTinh;
    }

    public String getNgaySinh() {
        return NgaySinh;
    }

    public void setNgaySinh(String NgaySinh) {
        this.NgaySinh = NgaySinh;
    }

    public String getNoiSinh() {
        return NoiSinh;
    }

    public void setNoiSinh(String NoiSinh) {
        this.NoiSinh = NoiSinh;
    }

    public DanTocDAL getDanToc() {
        return DanToc;
    }

    public void setDanToc(DanTocDAL DanToc) {
        this.DanToc = DanToc;
    }

    public TonGiaoDAL getTonGiao() {
        return TonGiao;
    }

    public void setTonGiao(TonGiaoDAL TonGiao) {
        this.TonGiao = TonGiao;
    }
    
}
