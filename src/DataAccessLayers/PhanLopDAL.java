

package DataAccessLayers;


public class PhanLopDAL {
    private String MaHS;
    private String TenHS;
    
    public PhanLopDAL(){
        
    }
    
    public PhanLopDAL(String MaHS, String TenHS) {
        this.MaHS = MaHS;
        this.TenHS = TenHS;
    }

    public String getMaHS() {
        return MaHS;
    }

    public void setMaHS(String MaHS) {
        this.MaHS = MaHS;
    }

    public String getTenHS() {
        return TenHS;
    }

    public void setTenHS(String TenHS) {
        this.TenHS = TenHS;
    }
    
}
