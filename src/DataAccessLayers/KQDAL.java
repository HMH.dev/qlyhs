

package DataAccessLayers;


public class KQDAL {
    private String MaHS;
    private int MaMH;
    private int MaHK;
    private int MaNH;
    private String M;
    private String P1;
    private String P2;
    private String T;
    private String TB;

    public String getM() {
        return M;
    }

    public void setM(String M) {
        this.M = M;
    }

    public String getP1() {
        return P1;
    }

    public void setP1(String P1) {
        this.P1 = P1;
    }

    public String getP2() {
        return P2;
    }

    public void setP2(String P2) {
        this.P2 = P2;
    }

    public String getT() {
        return T;
    }

    public void setT(String T) {
        this.T = T;
    }

    public String getTB() {
        return TB;
    }

    public void setTB(String TB) {
        this.TB = TB;
    }

    public int getMaNH() {
        return MaNH;
    }

    public void setMaNH(int MaNH) {
        this.MaNH = MaNH;
    }

    public String getMaHS() {
        return MaHS;
    }

    public void setMaHS(String MaHS) {
        this.MaHS = MaHS;
    }

    public int getMaMH() {
        return MaMH;
    }

    public void setMaMH(int MaMH) {
        this.MaMH = MaMH;
    }

    public int getMaHK() {
        return MaHK;
    }

    public void setMaHK(int MaHK) {
        this.MaHK = MaHK;
    }
    
}
