

package DataAccessLayers;


public class MonHocDAL extends clsDAL{
    private int HeSo;

    public MonHocDAL() {
        this.HeSo = 0;
    }

    public MonHocDAL(int MaMonHoc, String TenMonHoc, int HeSo) {
        this.setMa(MaMonHoc);
        this.setTen(TenMonHoc);
        this.HeSo = HeSo;
    }

    public int getHeSo() {
        return HeSo;
    }

    public void setHeSo(int HeSo) {
        this.HeSo = HeSo;
    }
    
}
