
package DataAccessLayers;


public class LoaiDiemDAL {
    private int MaLoai;
    private String TenLoai;
    private int HeSo;

    public LoaiDiemDAL() {
        this.MaLoai = 0;
        this.TenLoai = "";
        this.HeSo = 0;
    }

    public LoaiDiemDAL(int MaLoai, String TenLoai, int HeSo) {
        this.MaLoai = MaLoai;
        this.TenLoai = TenLoai;
        this.HeSo = HeSo;
    }

    public int getMaLoai() {
        return MaLoai;
    }

    public void setMaLoai(int MaLoai) {
        this.MaLoai = MaLoai;
    }

    public String getTenLoai() {
        return TenLoai;
    }

    public void setTenLoai(String TenLoai) {
        this.TenLoai = TenLoai;
    }

    public int getHeSo() {
        return HeSo;
    }

    public void setHeSo(int HeSo) {
        this.HeSo = HeSo;
    }
    
    
}
